<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    "accepted"         => ":attribute debe ser aceptado.",
    "active_url"       => ":attribute no es una URL válida.",
    "after"            => ":attribute debe ser una fecha posterior a :date.",
    "alpha"            => ":attribute solo debe contener letras.",
    "alpha_dash"       => ":attribute solo debe contener letras, números y guiones.",
    "alpha_num"        => ":attribute solo debe contener letras y números.",
    "array"            => ":attribute debe ser un conjunto.",
    "before"           => ":attribute debe ser una fecha anterior a :date.",
    "between"          => [
        "numeric" => ":attribute tiene que estar entre :min - :max.",
        "file"    => ":attribute debe pesar entre :min - :max kilobytes.",
        "string"  => ":attribute tiene que tener entre :min - :max caracteres.",
        "array"   => ":attribute tiene que tener entre :min - :max ítems.",
    ],
    "boolean"          => "El campo :attribute debe tener un valor verdadero o falso.",
    "confirmed"        => "La confirmación de :attribute no coincide.",
    "date"             => ":attribute no es una fecha válida.",
    "date_format"      => ":attribute no corresponde al formato :format.",
    "different"        => ":attribute y :other deben ser diferentes.",
    "digits"           => ":attribute debe tener :digits dígitos.",
    "digits_between"   => ":attribute debe tener entre :min y :max dígitos.",
    "email"            => ":attribute no es un correo válido",
    "exists"           => ":attribute es inválido.",
    "filled"           => "El campo :attribute es obligatorio.",
    "image"            => ":attribute debe ser una imagen.",
    "in"               => ":attribute es inválido.",
    "integer"          => ":attribute debe ser un número entero.",
    "ip"               => ":attribute debe ser una dirección IP válida.",
    "max"              => [
        "numeric" => ":attribute no debe ser mayor a :max.",
        "file"    => ":attribute no debe ser mayor que :max kilobytes.",
        "string"  => ":attribute no debe ser mayor que :max caracteres.",
        "array"   => ":attribute no debe tener más de :max elementos.",
    ],
    "mimes"            => ":attribute debe ser un archivo con formato: :values.",
    "min"              => [
        "numeric" => "El tamaño de :attribute debe ser de al menos :min.",
        "file"    => "El tamaño de :attribute debe ser de al menos :min kilobytes.",
        "string"  => ":attribute debe contener al menos :min caracteres.",
        "array"   => ":attribute debe tener al menos :min elementos.",
    ],
    "not_in"           => ":attribute es inválido.",
    "numeric"          => ":attribute debe ser numérico.",
    "regex"            => "El formato de :attribute es inválido.",
    "required"         => "El campo :attribute es obligatorio.",
    "required_if"      => "El campo :attribute es obligatorio cuando :other es :value.",
    "required_with"    => "El campo :attribute es obligatorio cuando :values está presente.",
    "required_with_all" => "El campo :attribute es obligatorio cuando :values está presente.",
    "required_without" => "El campo :attribute es obligatorio cuando :values no está presente.",
    "required_without_all" => "El campo :attribute es obligatorio cuando ninguno de :values estén presentes.",
    "same"             => ":attribute y :other deben coincidir.",
    "size"             => [
        "numeric" => "El tamaño de :attribute debe ser :size.",
        "file"    => "El tamaño de :attribute debe ser :size kilobytes.",
        "string"  => ":attribute debe contener :size caracteres.",
        "array"   => ":attribute debe contener :size elementos.",
    ],
    "timezone"         => "El :attribute debe ser una zona válida.",
    "unique"           => ":attribute ya ha sido registrado.",
    "url"              => "El formato :attribute es inválido.",

    "image_size"       => "La :attribute debe ser :width de ancho y :height de alto.",
    'between'            => 'entre :size1 y :size2 pixeles',
    'lessthan'           => 'menor a :size pixeles',
    'lessthanorequal'    => 'menor o igual :size pixeles',
    'greaterthan'        => 'mayor a :size pixeles',
    'greaterthanorequal' => 'mayor o igual a :size pixeles',
    'equal'              => ':size pixeles',
    'anysize'            => 'cualquier tamaño',
    'image_aspect'       => 'The :attribute aspect ratio must be :aspect.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'nombre'                => 'Nombre',
        'nombres'               => 'Nombres',
        'apellido'              => 'Apellido',
        'apellidos'             => 'Apellidos',
        'cedula'                => 'Cédula',
        'username'              => 'Nombre de Usuario',
        'email_short'           => 'Correo',
        'email'                 => 'Correo Electrónico',
        'correo'                => 'Correo Electrónico',
        'password'              => 'Contraseña',
        'password_confirmation' => 'Confirme Contraseña',
        'tipo'                  => 'Tipo',
        'estado'                => 'Estado',
        'telefono_fijo'         => 'Teléfono Fijo',
        'telefono_celular'      => 'Celular',
        'departamento'          => 'Departamento',
        'ciudad'                => 'Ciudad',
        'barrio'                => 'Barrio',
        'direccion'             => 'Dirección',
        'codigo_postal'         => 'Código Postal',
        'url_imagen'            => 'Imagen',
        'descripcion'           => 'Descripción',
        'id_tipo'               => 'Tipo',
        'imagen'                => 'Imagen',
        'created_at'            => 'Fecha de Creado',
        'mime'                  => 'Mime',
        'estado'                => 'Estado',
        'titulo'                => 'Título',
        'lead'                  => 'Lead',
        'ubicacion'             => 'Ubicación',
        'organizacion'          => 'Organización',
        'sexo'                  => 'Sexo',
        'fecha_nacimiento'      => 'Fecha de Nacimiento',
        'terminos'              => 'Terminos y Condiciones',
        'nit'                   => 'NIT',
        'nombre_contacto'       => 'Nombre Contacto',
        'email_contacto'        => 'Correo Contacto',
        'telefono_contacto'     => 'Teléfono Contacto',
        'web'                   => 'Url Web',
        'fecha_ini_campana'     => 'Fecha Inicio de Campaña',
        'fecha_fin_campana'     => 'Fecha Fin de Campaña',
        'fecha_ini_beneficio'   => 'Fecha Inicio de Beneficio',
        'fecha_fin_beneficio'   => 'Fecha Fin de Beneficio',
        'imagen_md'             => 'Imagen lista de beneficios',
        'imagen_lg'             => 'Imagen detalle beneficio',
        'id_aliado'             => 'Aliado',
        'dispositivos'          => 'Dispositivos',
        'codigo'                => 'Código',
        'codigos'               => 'Códigos',
        'enlace'                => 'Enlace',
        'longitud'              => 'Longitud',
        'latitud'               => 'Latitud',
        'telefono'              => 'Teléfono',
        'horario'               => 'Horario',
        'archivo'               => 'Archivo',
        'id_ciudad'             => 'Ciudad',
        'empresa'               => 'Empresa',
        'cargo'                 => 'Cargo',
        'intervalo'             => 'Intervalo de Tiempo',
        'tiendas'               => 'Tiendas',
        'descripcion_corta'     => 'Descripción Corta',
        'paso_paso'             => 'Paso a Paso',
        'fecha_ini'             => 'Fecha Inicio',
        'fecha_fin'             => 'Fecha Fin Publicacion',
        'campana'               => 'Campaña',
        'id_campana'            => 'Campaña',
        'id_aliado'             => 'Aliado',
        'enunciado'             => 'Enunciado',
        'valor'                 => 'Valor',
        'fecha_hora'            => 'Fecha y Hora',
        'mensaje'               => 'Mensaje',
        'url_destino'           => 'Url Destino',
        'id_departamento'       => 'Departamento',
        'id_ciudad'             => 'Ciudad',
        'password_confirmed'    => 'Confirmar Contraseña',
        'fecha_inicio'    => 'Fecha Inicio Publicacion',
        'hora_inicio'    => 'Hora Inicio',
        'hora_fin'    => 'Hora Fin',
        'lugar'    => 'Lugar',
        'precio'    => 'Precio',
        'precio_descuento'    => 'Precio con Descuento',
        'id_categoria'    => 'Categoría',
        'localizacion' => 'Localización',
        'identificador' => 'Identificador',
        'id_estado' => 'Estado',
        'genero' => 'Género',
        'hora' => 'Hora',
        'fecha' => 'Fecha',
        'nombre_aliado' => 'Nombre',
        'direccion_aliado' => 'Dirección',
        'web_aliado' => 'Web/Url',
        'id_ciudad_aliado' => 'Ciudad',
        'descuento' => 'descuento',
        'fechadesc' => ' Fecha de Evento',
        'fecha_nacimiento' => ' Fecha de Nacimiento',
        'id_imagen' => 'imagen'
    ],

];
