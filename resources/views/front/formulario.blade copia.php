@extends('front.template.base')

@section('titulo')
Publica Tu Evento
@endsection

@section('css')
	{!! Html::style('front/css/vendor/dropzone.css') !!}
	{!! Html::style('front/css/vendor/cropper.css') !!}
@endsection

@section('js')
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSynIVRyKUopyFqtQIiJW34VOBrt4oa48&callback=initMap" async defer></script>
	{!! Html::script('front/js/vendor/dropzone.js') !!}
	{!! Html::script('front/js/vendor/cropper.min.js') !!}
	{!! Html::script('front/js/modulos/publica-evento.js'.'?'.str_random(4)) !!}
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">@yield('titulo')</h1>
					<p class="Encabezado-lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="Section Section-blanco homePublica">
		<div class="container">

			{!! Form::hidden('id_aliado', null) !!}
			{!! Form::hidden('id_contacto', null) !!}
			{!! Form::hidden('id_imagen', null) !!}

			<fieldset class="homePublica-form">
				<legend class="homePublica-form-title">Organización</legend>

				{!! Form::open(['route' => 'front_aliado_save', 'method' => 'post', 'id' => 'frm-aliado']) !!}
					<div class="row">
						<div class="col-md-6">

							<div class="form-group">
								{!! Form::label('nombre', trans('validation.attributes.nombre')) !!} <span class="text-danger">*</span>
								{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('nit', trans('validation.attributes.nit')) !!}
								{!! Form::text('nit', null, ['class' => 'form-control']) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('direccion', trans('validation.attributes.direccion')) !!} <span class="text-danger">*</span>
								{!! Form::text('direccion', null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

						</div>

						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('web', trans('validation.attributes.web')) !!}
								{!! Form::text('web', null, ['class' => 'form-control']) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!}
								{!! Form::select('id_ciudad', $ciudades, null, ['class' => 'form-control selectpicker', 'data-selected-text-format' => "count>3", 'data-live-search' => "true", 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>
							
						</div>

						<div class="col-md-12">
							<button class="btn btn-primary btn-md pull-right" id="btn-aliado" type="submit" >Guardar</button>
						</div>
					</div>
				{!! Form::close() !!}
			</fieldset>

			<fieldset class="homePublica-form">
				<legend class="homePublica-form-title">Información de Contacto</legend>
				<div class="row">
					{!! Form::open(['route' => ['front_aliado_contacto', 'id' => 1], 'method' => 'post', 'data-toggle' => "validator", 'id' => 'frm-contacto']) !!}

						<div class="col-md-4">

							<div class="form-group">
								{!! Form::label('nombre', trans('validation.attributes.nombre')) !!} <span class="text-danger">*</span>
								{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

						</div>

						<div class="col-md-4">
							
							<div class="form-group">
								{!! Form::label('email', trans('validation.attributes.email')) !!} <span class="text-danger">*</span>
								{!! Form::email('email', null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

						</div>
						<div class="col-md-4">
							
							<div class="form-group">
								{!! Form::label('telefono', trans('validation.attributes.telefono')) !!} <span class="text-danger">*</span>
								{!! Form::text('telefono', null, ['class' => 'form-control']) !!}
								<div class="help-block with-errors"></div>
							</div>
							
						</div>

						<div class="col-md-12">
							<button class="btn btn-primary btn-md pull-right" id="btn-contacto" disabled="disabled">Guardar</button>
						</div>
					{!! Form::close() !!}
				</div>
			</fieldset>

			<fieldset class="homePublica-form">
				<legend>Imagen del Evento</legend>

				{!! Form::open(['route' => 'front_aliado_evento_imagen', 'method' => 'post', 'id' => 'frm-imagenes', 'class' => 'dropzone']) !!}
					<div class="fallback">
						<input name="file" type="file" />
					</div>
				{!! Form::close() !!}

				<div class="homePublica-imagen"></div>

				<div class="row">
					<div class="col-md-12">
						<button class="btn btn-primary btn-md pull-right" id="btn-imagen" disabled="disabled">Guardar</button>
					</div>
				</div>
			</fieldset>

			<fieldset class="homePublica-form">
				<legend class="homePublica-form-title">Información del Evento</legend>
				<div class="row">
					{!! Form::open(['route' => ['front_aliado_evento_imagen', 'id' => 1], 'method' => 'post', 'data-toggle' => "validator", 'id' => 'frm-evento']) !!}
						<div class="col-md-12">
							{{-- Mensajes de error --}}
							<div class="alert alert-danger sr-only" id="msj_error"></div>
						</div>
						<div class="col-md-6">

							<div class="form-group">
								{!! Form::label('nombre', trans('validation.attributes.nombre')) !!} <span class="text-danger">*</span>
								{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!} <span class="text-danger">*</span>
								{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '4', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('terminos', trans('validation.attributes.terminos')) !!} <span class="text-danger">*</span>
								{!! Form::textarea('terminos', null, ['class' => 'form-control', 'rows' => '5', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('direccion', trans('validation.attributes.direccion')) !!} <span class="text-danger">*</span>
								{!! Form::text('direccion', null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('lugar', trans('validation.attributes.lugar')) !!}
								{!! Form::text('lugar', null, ['class' => 'form-control']) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('latitud', trans('validation.attributes.localizacion')) !!}
								<div id="mapa" style="height: 300px; width:100%;"></div>
								{!! Form::hidden('latitud', null, ['id' => 'latitud']) !!}
								{!! Form::hidden('longitud', null, ['id' => 'longitud']) !!}
							</div>

						</div>

						<div class="col-md-6">

							<div class="form-group">
								{!! Form::label('web', trans('validation.attributes.web')) !!}
								{!! Form::text('web', null, ['class' => 'form-control']) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('fecha_inicio', trans('validation.attributes.fecha_inicio')) !!} <span class="text-danger">*</span>
										{!! Form::text('fecha_inicio', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('fecha_fin', trans('validation.attributes.fecha_fin')) !!} <span class="text-danger">*</span>
										{!! Form::text('fecha_fin', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>

							<div class="form-group">
								{!! Form::label('precio', trans('validation.attributes.precio')) !!} <span class="text-danger">*</span>
								{!! Form::textarea('precio', null, ['class' => 'form-control', 'rows' => '3', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('id_categoria', trans('validation.attributes.id_categoria')) !!} <span class="text-danger">*</span>
								{!! Form::select('id_categoria', $categorias, null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!} <span class="text-danger">*</span>
								{!! Form::select('id_ciudad', $ciudades, null, ['class' => 'form-control selectpicker', 'data-selected-text-format' => "count>3", 'data-live-search' => "true"]) !!}
								<div class="help-block with-errors"></div>
							</div>
							
							{{-- Horario --}}
							<fieldset id="ctn-horario">
								<legend>Horarios</legend>

								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											{!! Form::text('fecha[]', null, ['class' => 'form-control datepicker fecha', 'placeholder' => trans('validation.attributes.fecha')]) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>

									<div class="col-md-5">
										<div class="form-group">
											{!! Form::text('hora[]', null, ['class' => 'form-control datepicker-time hora', 'placeholder' => trans('validation.attributes.hora')]) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>

									<div class="col-md-2">
										<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
									</div>
								</div>

							</fieldset>

						</div>

						<div class="col-md-12">
							<button class="btn btn-rosado btn-lg pull-right" id="btn-evento" disabled="disabled">Enviar Evento</button>
						</div>
					{!! Form::close() !!}
				</div>
			</fieldset>

			<div class="row sr-only" id="msj-success">
				<div class="col-md-12">
					<div class="alert alert-success">
						<p>Hemos recibido tu Evento, en unos momentos estará activo en nuestra web. Gracias.</p>
					</div>
				</div>
			</div>

			<div class="row sr-only" id="msj-error">
				<div class="col-md-12">
					<div class="alert alert-danger">
						<p>Ocurrió un error, por favor contactanos admin@sihayparahacer.com </p>
					</div>
				</div>
			</div>

		</div>
	</section>

	<script id="tmp-imagen" type="text/x-handlebars-template">
		<img class="img-responsive" id="image" src="@{{ url }}" data-id="@{{ id }}">
	</script>

	<script id="tmp-horario" type="text/x-handlebars-template">
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="fecha[]" class="form-control datepicker fecha" placeholder="Fecha">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="hora[]" class="form-control datepicker-time hora" placeholder="Hora">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-2">
				<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
			</div>
		</div>
	</script>

@endsection