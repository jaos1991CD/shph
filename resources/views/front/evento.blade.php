@extends('front.template.base')

@section('titulo')
{{ $evento->nombre }}
@endsection

@section('metatags')
	{{-- Etiquetas facebook --}}
	<meta property="og:title" content="{{ $evento->nombre }}" />
	<meta property="og:type" content="website" />
	<meta property="og:locale:alternate" content="es_ES" />
	<meta property="og:site_name" content="Si Hay Para Hacer" />
	<meta property="og:description" content="{{ $evento->nombre }}" />
	<meta property="og:image" content="{{url('front/img/eventos/'.$evento->imagen->nombre) }}" />
	<meta property="og:image:width" content="899" />
	<meta property="og:image:height" content="506" />
@endsection

@section('css')
	{!! Html::style('front/css/vendor/star-rating.css') !!}
@endsection

@section('js')
	@if($evento->latitud != '' && $evento->longitud != '')
		<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
		{!! Html::script('front/js/vendor/gmaps.js') !!}
	@endif
	{!! Html::script('front/js/vendor/star-rating.min.js') !!}

	<script type="text/javascript">
	$(document).on('ready', function() {
		$("#rating").rating({
			size: 'xs',
			step: 1,
			min: 0,
			max: 5,
			showCaption: false,
			showClear: false
		});
		// si el evento ya tiene una longitud y una latitud, simplemente se ubica en el mapa
		@if($evento->latitud != '' && $evento->longitud != '')
			var mapa = new GMaps({
				div: '#mapa',
				lat: {{ $evento->latitud }},
				lng: {{ $evento->longitud }},
				scrollwheel: false,
			});

			mapa.addMarker({
				lat: {{ $evento->latitud }},
				lng: {{ $evento->longitud }},
				title: '{{ $evento->ciudad->nombre }}',
				infoWindow: {
					content: '<p>{{ $evento->lugar }}</p><p>{{ $evento->direccion }}</p>'
				}
			});
		@endif

	});
	// Si el evento no tiene aun una longitud y latitud, se ubica segun la direccion y ciudad del evento. Se crea el evento para poder ubicar el marcador en el mapa
	@if($evento->latitud == '' && $evento->longitud == '')
		function initMap() {
			var map = new google.maps.Map(document.getElementById('mapa'), {
				zoom: 15,
				center: {lat: -34.397, lng: 150.644}
			});
			var geocoder = new google.maps.Geocoder();

			var address = '{{ $evento->direccion }}, {{ $evento->ciudad->nombre }}, {{ $evento->ciudad->departamento->nombre }}';

			geocoder.geocode({'address': address}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);
					var marker = new google.maps.Marker({
						map: map,
						position: results[0].geometry.location
					});
				} else {
					console.log('No se puede mostrar ubicación del evento');
				}
			});
		}
	@endif

	</script>

	@if($evento->latitud == '' && $evento->longitud == '')
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSynIVRyKUopyFqtQIiJW34VOBrt4oa48&callback=initMap" async defer></script>
	@endif

@endsection

@section('contenido')

	<section class="Evento Evento--blanco">
		<div class="container">

			@include('admin.template.partials.error')
			@include('admin.template.partials.errors')
			@include('admin.template.partials.success')

			<div class="row">
				<div class="col-md-12 col-xs-12 Evento-header">
					<h1 class="Evento-nombre">{{ $evento->nombre }}</h1>
					
					<ul class="Evento-localizacion">
						<li><i class="fa fa-calendar cl-text-{{ $evento->categoria->color }}"></i> {!! $evento->fechadesc!!}
						@if ( ! is_null($proximo_horario))
							<!-- <li><i class="fa fa-calendar cl-text-{{ $evento->categoria->color }}"></i> {{ ucwords($proximo_horario->fecha_format) }}</li> -->
							<li><i class="fa fa-clock-o cl-text-{{ $evento->categoria->color }}"></i> {{ $proximo_horario->hora_format }} </li>
						@endif
						<li><i class="fa fa-map-marker cl-text-{{ $evento->categoria->color }}"></i> {{ $evento->direccion }}</li>
						<li><i class="fa fa-building cl-text-{{ $evento->categoria->color }}"></i> {{ $evento->lugar }}, {{ $evento->ciudad->nombre }}</li>
					</ul>
				</div>

				<div class="col-md-8 col-xs-12">

					<figure class="Evento-imagen">
						<img src="{{url('front/img/eventos/'.$evento->imagen->nombre) }}">
					</figure>

					<hr>

					<div class="Evento-descripcion">
						<h3 class="Evento-descripcion-titulo">Descripción</h3>

						<p class="Evento-descripcion-texto">
							{!! $evento->descripcion !!}
						</p>
					</div>

					<div class="Evento-social">
						<a href="http://www.facebook.com/share.php?u={{ route('front_evento', ['id' => $evento->id]) }}&title={{ $evento->nombre }}" target="_blank" class="btn btn-facebook btn-sm"><i class="fa fa-facebook"></i> Compartir</a>
						<a href="http://twitter.com/intent/tweet?status={{ $evento->nombre }}+{{ route('front_evento', ['id' => $evento->id]) }}" class="btn btn-twitter btn-sm"><i class="fa fa-twitter"></i> Compartir</a>
					</div>

					@if (Auth::check())
						<div class="Evento-terminos">
							<h4 class="Evento-terminos-titulo">Términos y Condiciones</h4>
							<p class="Evento-terminos-texto">
								{{ $evento->terminos }}

								
							</p>
						</div>
					@endif

					<hr>

					<div class="Evento-comentarios">
						<div class="row">
							<div class="col-md-12 col-xs-12">
								@if(session()->has('msj_success_comentario'))
									<div class="alert alert-success">
										{{ session()->get('msj_success_comentario') }}
									</div>
								@endif
								<div class="Comentarios">
									<h4>Comentarios</h4>

									@if (\Auth::check())
										@if(Auth::user()->tipo != 'aliado')
											{!! Form::open(['route' => ['front_evento_comentario', 'id' => $evento->id], 'method' => 'post', 'class' => 'Comentarios-formulario']) !!}

												<ul class="comments">
													<li class="clearfix">
														<div class="post-comments">
															<input type="number" id="rating" name="calificacion">
															<div class="form-group">
																{!! Form::hidden('id', $evento->id) !!}

																{!! Form::textarea('comentario', null, ['class' => 'form-control', 'rows' => 5, 'placeholder' => 'Danos tu opinion acerca de este evento']) !!}
															</div>

															<button class="btn btn-azul pull-right">Enviar</button>
														</div>
													</li>
												</ul>

											{!! Form::close() !!}
										@endif
									@endif
									{{-- Comentarios --}}

									@if (count($comentarios) > 0)
										<ul class="comments">

											@foreach ($comentarios as $comentario)
												<li class="clearfix">
													<div class="post-comments">
														<p class="meta">{{ \Carbon\Carbon::parse($comentario->created_at)->toDateString() }} <a href="#">{{ $comentario->usuario->perfil->nombres }}</a> dice :</p>
														<p>
															{{ $comentario->mensaje }}
														</p>
														<p>
															@for ($i = 0; $i < $comentario->calificacion; $i++)
																<i class="fa fa-star"></i>
															@endfor
														</p>
													</div>
												</li>
											@endforeach

										</ul>
									@else
										<div class="alert alert-info">No hay comentarios de este evento</div>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-xs-12" id="Evento-sidebar">
					{{-- Mapa --}}
					 <div class="Evento-mapa" id="mapa"> </div> 

					{{-- Solo lo puede ver el registrado --}}
					@if (Auth::check())
						@if(Auth::user()->tipo != 'aliado' )
							@if(! is_null($suscripciones))
								{!! Form::open(['route' => ['front_solicitar_beneficio', 'id' => $evento->id], 'method' => 'post']) !!}
									
									@if ($beneficio)
										<button type="button" class="btn btn-{{ $evento->categoria->color }} btn-lg btn-block Evento-adquirir" disabled="disabled"><i class="fa fa-ticket"></i> Beneficio Adquirido</button>
									@else
										<button type="submit" onclick="return confirm('Confirmo que leí los términos y condiciones de este evento.')" class="btn btn-{{ $evento->categoria->color }} btn-lg btn-block Evento-adquirir"><i class="fa fa-ticket"></i> Adquirir Beneficio</button>
									@endif

								{!! Form::close() !!}
										{{-- Precio --}}
								<div class="Evento-aliado">
									<div class="Evento-aliado-header">
										<h4>Precio</h4>
									</div>
									<div class="Evento-aliado-body">
										<div class="Evento-aliado-contacto">
											{!! $evento->precio !!}
										</div>
									</div>
								</div>

								{{-- descuento --}}
								@if(! is_null($evento->descuento))
								<div class="Evento-aliado">
									<div class="Evento-aliado-header">
										<h4>Descuento</h4>
									</div>
									<div class="Evento-aliado-body">
										<div class="Evento-aliado-contacto">
											{!! $evento->descuento !!}
										</div>
									</div>
								</div>
								@endif
								{{-- fecha evento--}}
								<div class="Evento-aliado">
									<div class="Evento-aliado-header">
										<h4>Fecha del Evento</h4>
									</div>
									<div class="Evento-aliado-body">
										<div class="Evento-aliado-contacto">
											{{ $evento->fechadesc }}
										</div>
									</div>
								</div>
								{{-- Horarios --}}
								{{--@if (count($horarios) > 0)
									<div class="Evento-aliado">
										<div class="Evento-aliado-header">
											<h4>Calendario</h4>
										</div>
										<div class="Evento-aliado-body">
											<div class="Evento-aliado-contacto Evento-aliado-calendario">
												{!! $calendar !!}
											</div>
										</div>
									</div>
								@endif--}}
								{{-- Aliado --}}
								<div class="Evento-aliado">
									<div class="Evento-aliado-header">
										<h4>Organizador</h4>
										<p>{{ $evento->aliado->nombre }}</p>
									</div>
									<div class="Evento-aliado-body">
										<div class="Evento-aliado-contacto">
											<i class="fa fa-link cl-text-{{ $evento->categoria->color }}"></i> <a href="{{ $evento->aliado->web}}">Visitar Web</a>
										</div>
									</div>
								</div>	
							@else
								<p class="text-center">Membresía inactiva. Paga tu afiliación para recibir descuentos</p>
								<a href="{{ route('vista_pagos',['id' => $usuario->id]) }}"  class="btn btn-azul btn-lg btn-block">Suscribirse</a> 
							@endif
								
						@endif
					@else
						<p class="text-center">Para obtener los beneficio debes suscribirte con nosotros</p>
						<a href="{{ route('login') }}" target="_self" class="btn btn-azul btn-lg btn-block">Iniciar Sesión</a>
						<a href="{{ route('suscribirse') }}" class="btn btn-naranja btn-lg btn-block">Registrarme</a>
					@endif
                	
                	

					
				</div>
				
			</div>
		</div>
	</section>

@endsection