<div class="modalLogin modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLogin">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLogin">Iniciar Sesión</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="modalLogin-social">
							<p class="modalLogin-social-title">Puedes inicar sesión con:</p>
							<a href="{{ url('auth/social/facebook') }}" class="modalLogin-social-button btn btn-facebook btn-block btn-lg"><i class="fa fa-facebook"></i> Facebook</a>
						</div>

						<p class="modalLogin-social-title text-center">O con correo electrónico</p>
						

						{!! Form::open(['url' => 'auth/login', 'method' => 'post', 'role' => 'form']) !!}
						<div class="form-group">
							{!! Form::email('correo', null, ['class' => 'form-control input-lg', 'placeholder' => trans('validation.attributes.correo')]) !!}
						</div>

						<div class="form-group">
							{!! Form::password('password', ['class' => 'form-control input-lg', 'placeholder' => trans('validation.attributes.password')]) !!}
						</div>

						<div class="checkbox">
							<label>
								<input type="checkbox" name="rememberme"> Recordame
							</label>
						</div>

						<button class="btn btn-block btn-lg btn-primary" type="submit">Entrar</button>

						<div class="col-md-12 text-center">
							<a href="#" class="btn btn-link">Olvidé mi contraseña</a>
						</div>
						

						{!! Form::close() !!}

						<p class="modalLogin-social-title text-center">Aún no eres parte de nosotros?</p>
						<button type="button" class="btn btn-naranja btn-block btn-lg" data-dismiss="modal" data-toggle="modal" data-target="#modalRegistro">Registrarme</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>