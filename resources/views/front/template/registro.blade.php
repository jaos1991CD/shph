<div class="modalLogin modal fade" id="modalRegistro" tabindex="-1" role="dialog" aria-labelledby="myModalRegistro">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalRegistro">Registrate</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="modalLogin-social">
							<p class="modalLogin-social-title">Puedes registrarte con:</p>
							<a href="#" class="modalLogin-social-button btn btn-facebook btn-block btn-lg"><i class="fa fa-facebook"></i> Facebook</a>
						</div>

						<p class="modalLogin-social-title text-center">O crear una cuenta</p>

						{!! Form::open(['url' => 'auth/register', 'method' => 'post', 'role' => 'form', "data-toggle" => "validator", 'autocomplete' => 'off']) !!}

							<div class="form-group">
								{!! Form::text('nombres', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.nombres'), 'required' => true]) !!}
								<div class="help-block with-errors">
									@if ($errors->has('nombres')) {{ $errors->first('nombres') }} @endif
								</div>
							</div>

							<div class="form-group">
								{!! Form::text('apellidos', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.apellidos'), 'required' => true]) !!}
								<div class="help-block with-errors">
									@if ($errors->has('apellidos')) {{ $errors->first('apellidos') }} @endif
								</div>
							</div>

							<div class="form-group @if ($errors->has('email')) has-error @endif">
								{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.email'), 'required' => true]) !!}
								<div class="help-block with-errors">
									@if ($errors->has('email')) {{ $errors->first('email') }} @endif
								</div>
							</div>

							<div class="form-group">
								{!! Form::select('genero', $generos, null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								<label for="year">Fecha de nacimiento</label>
								<div class="row">
									<div class="col-md-4">
										<select name="year" id="year" class="form-control">
											@for ($i = 1950; $i < 2015; $i++)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>

									<div class="col-md-4">
										<select name="month" id="month" class="form-control">
											@for ($i = 1; $i <= 12; $i++)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>

									<div class="col-md-4">
										<select name="day" id="day" class="form-control">
											@for ($i = 1; $i <= 31; $i++)
												<option value="{{ $i }}">{{ $i }}</option>
											@endfor
										</select>
									</div>
								</div>
							</div>

							<div class="form-group">
								{!! Form::select('id_departamento', $departamentos, null, ['class' => 'form-control', 'id' => 'departamento', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::select('id_ciudad', ['' => 'Seleccione uno'], null, ['class' => 'form-control', 'id' => 'ciudad', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => trans('validation.attributes.password')]) !!}
								<div class="help-block with-errors"></div>
							</div>

							<div class="form-group">
								{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.password_confirmation'), 'data-match' => "#password", 'data-match-error' => "La contraseña no coincide"]) !!}
								<div class="help-block with-errors">
									@if ($errors->has('password')) {{ $errors->first('password') }} @endif
								</div>
							</div>

							<div class="checkbox">
								<label>
									<input type="checkbox" name="terminos"> Acepto los <a href="#">Términos y Condiciones</a>
								</label>
							</div>

							<button type="submit" class="btn btn-block btn-lg btn-primary">Registrarme</button>
						{!! Form::close() !!}

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="tmp-ciudades" type="text/x-handlebars-template">
	@{{#each ciudades}}
		<option value="@{{id}}">@{{nombre}}</option>
	@{{/each}}
</script>