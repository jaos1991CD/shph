
<div class="modal fade" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalSuccess">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-success" id="myModalSuccess">
					<i class="fa fa-check fa-2x"></i>
				</h4>
			</div>
			<div class="modal-body">
				<h3 class="text-success text-center">
					{{ Session::get('mensaje', 'Acción éxitosa') }}
				</h3>
			</div>
		</div>
	</div>
</div>