<nav class="navbar navbar-negro" role="navigation" id="menu">
	<div class="container">
		
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
	
		<div class="row">
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li {!! (Request::is('para-hacer')) ? 'class="active"' : '' !!}><a href="{{ route('front_eventos', ['slug' => 'para-hacer']) }}" class="bd-naranja">Todos </a></li>
					@foreach($categorias_menu as $i => $cat_menu)
						@if ($i < 7)
							<li {!! (Request::is('categoria/'.$cat_menu->slug)) ? 'class="active"' : '' !!}><a href="{{ route('front_eventos_categoria', ['slug' => $cat_menu->slug]) }}" class="bd-{{ $cat_menu->color }}">{{ $cat_menu->nombre }}</a></li>
						@endif
					@endforeach
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Más <span class="caret"></span></a>
						<ul class="dropdown-menu">
							@foreach($categorias_menu as $i => $cat_menu)
								@if ($i >= 6)
									<li><a href="{{ route('front_eventos_categoria', ['slug' => $cat_menu->slug]) }}">{{ $cat_menu->nombre }}</a></li>
								@endif
							@endforeach
						</ul>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
                    <li class="buscar"><a href="#search"><i class="fa fa-search"></i> Buscar</a></li>
                </ul>

			</div>
		</div>
	</div>
</nav>

<div id="search">
	{!! Form::open(['route' => ['front_eventos', 'slug' => 'buscar'], 'method' => 'get']) !!}
		<input type="search" name="q" placeholder="Qué quieres buscar" />
		<button type="submit" class="btn btn-lg btn-naranja"><i class="fa fa-search"></i> Buscar</button>
	{!! Form::close() !!}
</div>