
<footer class="Footer">
	<div class="container">
		<div class="row">
			<div class="Footer-item col-md-4 col-md-offset-2">
				<h4 class="Footer-item-title">Acerca</h4>
				<ul class="Footer-item-list">
					<li><a href="{{ route('front_nosotros') }}">Nosotros</a></li>
					<li><a href="{{ route('front_terminos') }}" target="_blank">Términos y Condiciones</a></li>
					{{-- <li><a href="#">Contacto</a></li> --}}
					<li><a href="{{ route('front_publica') }}">Pública tu Evento</a></li>
				</ul>
			</div>

			<div class="Footer-item col-md-4">
				<h4 class="Footer-item-title">Síguenos</h4>
				<a href="https://www.facebook.com/sihayparahacer1" target="_blank" class="Footer-social-btn btn btn-facebook btn-circle"><i class="fa fa-facebook fa-lg"></i></a>
				<a href="https://twitter.com/Sihayparahacer" target="_blank" class="Footer-social-btn btn btn-twitter btn-circle"><i class="fa fa-twitter fa-lg"></i></a>
				<a href="https://www.instagram.com/sihayparahacer/" target="_blank" class="Footer-social-btn btn btn-facebook btn-circle"><i class="fa fa-instagram fa-lg"></i></a>
			</div>
		</div>
	</div>
</footer>

<div class="Footer-CD">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<p class="Footer-CD-rights">Copyright © 2015 Crear Digital</p>
			</div>
			<div class="col-md-6">
				<p class="Footer-CD-links"><a href="#">Acerca de nosotros</a> - <a href="#">Privacidad</a></p>
			</div>
		</div>
	</div>
</div>


