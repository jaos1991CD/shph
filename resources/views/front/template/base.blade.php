<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, maximum-scale=3, minimum-scale=0.5">
	<meta name="viewport" content="width=device-width">
	<meta name="viewport" content="width=320">

	@yield('metatags', '')

	<meta name="description" content="@yield('descripcion', 'Agenda cultural del suroccidente colombiano, 24 años al aire presentando toda la información cultural y artística de nuestra región.')">
	<meta name="author" content="Crear Digital">
	<!-- <link rel="icon" href="../../favicon.ico"> -->
	<link rel="icon" href="{{ url('assets/favicon.ico') }}" >

	<title>@yield('titulo', 'Inicio') | Sí Hay Para Hacer</title>

	{{-- Bootstrap --}}
	{!! Html::style('front/css/vendor/bootstrap.css') !!}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
	{!! Html::style('admin/css/vendor/bootstrap-datetimepicker.css') !!}
	{!! Html::style('front/css/vendor/bootstrap-select.css') !!}
	{{-- Html::style('front/css/vendor/aslider.css') --}}
	
	@yield('css', '')
	
	{!! Html::style('front/css/main.css') !!}

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{   (i[r].q=i[r].q||[]).push(arguments)}
,i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-74031794-1', 'auto');
  ga('send', 'pageview');
</script>

</head>

<body>
	{{-- Header --}}
	<header class="Header">
		<div class="container">
			{{-- Header logo --}}
			<div class="row">
				<div class="col-xs-4 col-sm-3 col-md-3">
					{{-- Logo --}}
					<div class="Header-logo">
						<a href="{{ route('home') }}">
							<img src="{{ url('front/img/logo_SHPH.jpg') }}">
						</a>
					</div>
				</div>

				{{-- Filtro zonas --}}
				<div class="col-xs-8 col-sm-6 col-md-6">
					{!! Form::select('zona', $regiones, session()->get('zona'), ['class' => 'form-control Header-select', 'id' => 'zona']) !!}
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="Header-botones">
						@if (Auth::check())

							{{-- Usuario --}}
							@if (Auth::user()->tipo  == 'user')
								<div class="btn-group">
									<button type="button" class="Header-botones-item btn btn-transparent-rosado btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-user"></i> {{ Auth::user()->perfil->nombres }} <span class="caret"></span>
									</button>
									<ul class="dropdown-menu">
										<li><a href="{{ route('front_perfil') }}">Perfil</a></li>
										<li><a href="{{ route('logout') }}">Cerrar Sesión</a></li>
									</ul>
								</div>

							{{-- Aliado --}}
							@elseif(Auth::user()->tipo == 'aliado')
								<div class="btn-group">
									<button type="button" class="Header-botones-item btn btn-transparent-rosado btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-user"></i> {{ Auth::user()->aliado->contacto->nombre }} <span class="caret"></span>
									</button>
									<ul class="dropdown-menu">
										<li><a href="{{ route('aliado_dashboard') }}">Dashboard</a></li>
										<li><a href="{{ route('logout') }}">Cerrar Sesión</a></li>
									</ul>
								</div>
							
							{{-- Admin --}}
							@else
								<div class="btn-group">
									<button type="button" class="Header-botones-item btn btn-transparent-rosado btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-user"></i> {{ Auth::user()->nombres }} <span class="caret"></span>
									</button>
									<ul class="dropdown-menu">
										<li><a href="{{ route('dashboard') }}">Administrador</a></li>
										<li><a href="{{ route('logout') }}">Cerrar Sesión</a></li>
									</ul>
								</div>
							@endif
						@else
						
							
							{{-- <button type="button" class="Header-botones-item btn btn-transparent-rosado btn-lg" data-toggle="modal" data-target="#modalLogin"><i class="fa fa-user fa-lg"></i> Iniciar Sesión</button> --}}
							<a href="{{ route('login') }}" class="Header-botones-item btn btn-transparent-rosado btn-lg">Entrar</a>
							<a href="{{ route('suscribirse') }}" class="Header-botones-item btn btn-transparent-rosado btn-lg">Suscribirse</a>
						@endif
					</div>
				</div>
			</div>

			{{-- Header slide --}}
			
		</div>
	</header>

	@yield('header', '')

	{{-- Menu --}}
	@include('front.template.menu')
	
	@yield('contenido', '')
	
	{{-- Pie de pagina --}}
	@include('front.template.footer')
	
	{{-- Ventanas de login y registro --}}
	@include('front.template.success')

	{!! Html::script('front/js/vendor/jquery.min.js') !!}
	{!! Html::script('front/js/vendor/bootstrap.min.js') !!}
	{!! Html::script('front/js/vendor/handlebars-v4.0.2.js') !!}
	{!! Html::script('front/js/vendor/validator.min.js') !!}
	{!! Html::script('admin/js/vendor/bootstrap-filestyle.min.js') !!}
	{!! Html::script('front/js/vendor/bootstrap-select.min.js') !!}
	{!! Html::script('front/js/vendor/jquery.sticky.js') !!}
	{!! Html::script('front/js/vendor/jquery.blockUI.js') !!}
	
	{!! Html::script('front/js/modulos/format.js') !!}

	{!! Html::script('admin/js/vendor/moment.js') !!}
	{!! Html::script('admin/js/vendor/es.js') !!}
	{!! Html::script('admin/js/vendor/bootstrap-datetimepicker.min.js') !!}

	{{-- Html::script('front/js/vendor/aslider.min.js') --}}
	
	@yield('js', '')

	{!! Html::script('front/js/main.js') !!}
	
	@if (Session::has('success'))
		<script type="text/javascript">
			$(document).on('ready', function() {
				$("#modalSuccess").modal('show');
			});
		</script>
	@endif

	@yield('templates', '')

</body>
</html>
