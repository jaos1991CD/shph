@extends('front.template.base')

@section('titulo')
{{ $pagina->titulo }}
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">{{ $pagina->titulo }}</h1>
					@if ( ! is_null($pagina->lead))
						<p class="Encabezado-lead">{{ $pagina->lead }}</p>
					@endif
				</div>
			</div>
		</div>
	</section>

	<section class="Section Section-blanco">
		<div class="container">
			{!! $pagina->texto !!}

			
			
		</div>
	</section>

@endsection