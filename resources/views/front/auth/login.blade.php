@extends('front.template.base')

@section('titulo')
Iniciar Sesión
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">Haz parte del Club Si Hay Para Hacer!</h1>
					<p class="Encabezado-lead">Los mejores eventos de la ciudad con grandes beneficios.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="Section Section-blanco">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@include('admin.template.partials.errors')
					@include('admin.template.partials.success')
					@include('admin.template.partials.error')
				</div>

				<div class="col-md-6 col-md-offset-3">
					<h2 class="Section-title Section-title-naranja">Iniciar Sesión</h2>
				</div>

				<div class="col-md-6 col-md-offset-3">
					<div class="row">
						<div class="col-md-12">

							{!! Form::open(['route' => 'login', 'method' => 'post', 'role' => 'form']) !!}
								<div class="form-group">
									{!! Form::label('correo', trans('validation.attributes.correo')) !!}
									{!! Form::email('correo', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.correo')]) !!}
								</div>

								<div class="form-group">
									{!! Form::label('password', trans('validation.attributes.password')) !!}
									{!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.password')]) !!}
								</div>

								<div class="checkbox">
									<label>
										<input type="checkbox" name="rememberme"> Recordame
									</label>
								</div>

								<button class="btn btn-block btn-lg btn-azul" type="submit">Entrar</button>
								<br>
								<div class="modalLogin-social">
									<p class="modalLogin-social-title">Iniciar sesión con:</p>
									<a href="{{ url('auth/social/facebook') }}" class="modalLogin-social-button btn btn-facebook btn-block btn-lg"><i class="fa fa-facebook"></i> Facebook</a>
								</div>

								 <div class="col-md-12 text-center">
									<a href="{{ route('password.email') }}" class="btn btn-link">Olvidé mi contraseña</a>
								</div> 
							{!! Form::close() !!}
							
						</div>
					</div>

				</div>

				<div class="col-md-6">

				</div>
			</div>
		</div>
	</section>

@endsection