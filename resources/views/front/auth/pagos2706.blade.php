@extends('front.template.base')

@section('titulo')
Suscripción
@endsection

@section('js')
	{!! Html::script('front/js/modulos/suscripcion.js'.'?'.str_random(4)) !!}

<script type="text/javascript">
$(document).ready(function(){
    $('#id_suscripcion > option[value="3"]').attr('selected', 'selected');
});
</script>

@endsection

@section('contenido')
<section class="Section Section-blanco">
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				@include('admin.template.partials.errors')
				@include('admin.template.partials.success')
				@include('admin.template.partials.error')
			</div>
			<!-- Facebook Conversion Code for Campaña lanzamiento FB -->
			<script>(function() {
  				var _fbq = window._fbq || (window._fbq = []);
  					if (!_fbq.loaded) {
    					var fbds = document.createElement('script');
    					fbds.async = true;
    					fbds.src = 'https://connect.facebook.net/en_US/fbds.js';
    					var s = document.getElementsByTagName('script')[0];
    					s.parentNode.insertBefore(fbds, s);
    					_fbq.loaded = true;
  					}
				})();
					window._fbq = window._fbq || [];
					window._fbq.push(['track', '6052998150392', {'value':'1.00','currency':'COP'}]);
			</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6052998150392&amp;cd[value]=1.00&amp;cd[currency]=COP&amp;noscript=1" /></noscript>
			<div class="col-md-12 text-center">
				<h2 class="Section-title Section-title-naranja">Suscríbete Aquí</h2>
			</div>
			<div class="col-md-8 col-md-offset-2">
				{!! Form::open(['url' => "https://test.secureacceptance.allegraplatform.com/CI_Secure_Acceptance/Payment", 'method' => 'post', 'role' => 'form', "data-toggle" => "validator", 'autocomplete' => 'off', 'id'=>'frm-pago']) !!}
					{!! Form::hidden('id_usuario', $id, ['id' => 'id_usuario']) !!}
					{!! Form::hidden('url', route('pagos_dios_prueba'), ['id' => 'url']) !!}
					<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
					<input type="hidden" id="signed_field_names" name="signed_field_names" value="transaction_uuid,signed_field_names,signed_date_time,url,access_key,profile_id,reference_number,amount,currency,locale"/>
					<input type="hidden" id="signed_date_time" name="signed_date_time" value="2016-03-18T18:32:28Z"/>
					<input type="hidden" id="access_key" name="access_key" value="{{ $acceskey }}"/>
					<input type="hidden" id="profile_id" name="profile_id" value="{{ $profileid }}"/>
					<input type="hidden" id="secret key" name="secret key" value="{{ $secretkey }}"/>
					<input type="hidden" id="reference_number" name="reference_number" value="{{ $referpay }}"/>
					<input type="hidden" id="amount" name="amount" readonly value="0"  >
	
					<input type="hidden" id="currency" name="currency" value="COP"/>
					<input type="hidden" id="locale" name="locale" value="es-CO"/>
					<input type="hidden" id="signature" name="signature" value="FApodS4JbvhiioaT8x3qqUWvVP1XYfq+tUSwLCvKcRo="/>

					<div class="col-md-10 col-md-offset-1">

						<div class="alert alert-danger hide msj_error">
						</div>

						<div class="row">

							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('id_suscripcion', 'Elige un plan' ) !!}<span class="text-danger">*</span>
									<select name="id_suscripcion" id="id_suscripcion" class="form-control" data-url="{{ route('ajax_suscripcion_usuario') }}" required>
											<option value="0">Selecciones Una...</option>
										@foreach($suscripciones as $sus)
											<option value="{{ $sus->id }}" data-valor="{{ $sus->valor }}">{{ $sus->descripcion }}</option>
										@endforeach
									</select>
								</div>
							</div>

							<div class="col-md-6">								
								<div class="form-group">
									{!! Form::label('codigo', 'Código Promocional' ) !!}
									<div class="input-group">
										{!! Form::text('codigo', null, ['class' => 'form-control', 'id' => 'codigo', 'placeholder' => 'QWERTY']) !!}
										{!! Form::hidden('codigo_actual', 0, ['id' => 'codigo_actual', 'data-valor' => 0, 'data-id' => 0]) !!}
										<span class="input-group-btn">
											<button class="btn btn-default btn-success" id="aplicar" type="button" onclick="">Aplicar</button>
										</span>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<ul class="list-group">
									{{-- <li class="list-group-item list-group-item-info">Plan Mensual por $10.000</li>
									<li class="list-group-item">Plan Semestral por $15.000</li>
									<li class="list-group-item">Plan Anual por $20.000</li> --}}
								</ul>
							</div>

							<div class="col-md-6">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Detalle</th>
											<th>Valor</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td class="txt-plan">Plan Mensual</td>
											<td class="txt-valor">$ 0 (COP)</td>
										</tr>

										<tr class="txt-descuento sr-only">
											<td class="txt-descuento-nombre"></td>
											<td class="txt-descuento-valor"></td>
										</tr>

										<tr>
											<th class="txt-total-nombre">Total</th>
											<td class="txt-total-valor">$ 0 (COP)</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-md-offset-4 text-center">
								<button type="button" id="guardar" class="btn btn-block btn-lg btn-azul">Pagar</button>
							</div>
						</div>
					
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

</section>

<script id="tmp-ciudades" type="text/x-handlebars-template">
	@{{#each ciudades}}
		<option value="@{{id}}">@{{nombre}}</option>
	@{{/each}}
</script>



@endsection