
@extends('front.template.base')

@section('titulo')
Recuperar Contraseña 
@endsection

@section('contenido')

<section class="Section Section-blanco">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					
					
				</div>

				<div class="col-md-6 col-md-offset-3">
					<h2 class="Section-title Section-title-naranja">Cambiar Contraseña</h2>
				</div>

				<div class="col-md-6 col-md-offset-3">
					<div class="row">
						<div class="col-md-12">
								@include('admin.template.partials.success')
							{!! Form::open((['route' => 'password.email.post', 'method' => 'post', 'role' => 'form'])) !!}
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<!--name="_token" value="{{ csrf_token() }}">
									name="email" value="{{ old('email') }}">-->
							<div class="form-group">
								{!! Form::label('correo', trans('validation.attributes.correo')) !!}
								{!! Form::email('correo', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.correo')]) !!}

							</div>
							<!--<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label class="col-md-4 control-label">E-Mail Address</label>
								<div class="col-md-6">
									<input type="email" class="form-control" name="email" value="{{ old('email') }}" readonly>
								</div>
							</div>-->


								<button class="btn btn-block btn-lg btn-azul" type="submit">Recuperar</button>
								

								 
							{!! Form::close() !!}
							
						</div>
					</div>

				</div>

					</div>
			</div>
		</div>
	</section>



@endsection
