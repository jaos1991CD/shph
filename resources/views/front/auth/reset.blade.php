
@extends('front.template.base')

@section('contenido')
<section class="Section Section-blanco">
	<div class="container">
		<div class="row">
			<div class="col-md-12">


			</div>

			<div class="col-md-6 col-md-offset-3">
				<h2 class="Section-title Section-title-naranja">Nueva Contraseña</h2>
				<strong><p>La contraseña debe tener como minimo 6 caracteres</p></strong>
			</div>


			<div class="col-md-6 col-md-offset-3">
				<div class="row">
					<div class="col-md-12">

						@include('admin.template.partials.success')
						@include('admin.template.partials.error')
						@include('admin.template.partials.errors')

						{!! Form::open((['route' => 'password.reset.post', 'method' => 'post', 'role' => 'form'])) !!}
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{{ $token }}">


						<div class="form-group">
							{!! Form::label('correo', trans('validation.attributes.correo')) !!}
							{!! Form::email('correo', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.correo')]) !!}

						</div>
						<div class="form-group">
							{!! Form::label('password', trans('validation.attributes.password')) !!}
							{!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => trans('validation.attributes.password'), 'required' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('password_confirmation', trans('validation.attributes.password_confirmation')) !!}
							{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.password_confirmation'), 'required' => true]) !!}
							<div class="help-block with-errors">
								@if ($errors->has('password')) {{ $errors->first('password') }} @endif
							</div>
						</div>


						<button class="btn btn-block btn-lg btn-azul" type="submit">Nueva contraseña</button>



						{!! Form::close() !!}

					</div>
				</div>

			</div>

		</div>
	</div>

</section>


@endsection
