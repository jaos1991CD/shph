@extends('front.template.base')

@section('titulo')
Registrarse
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">Haz parte del Club Si Hay Para Hacer!</h1>
					<p class="Encabezado-lead">Los mejores eventos de la ciudad con grandes beneficios.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="Section Section-blanco">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@include('admin.template.partials.errors')
					@include('admin.template.partials.success')
					@include('admin.template.partials.error')
				</div>

				<br>
				{{--<div class="modalLogin-social">
				<div align="rigth" class="col-md-offset-2">
					<div class="col-md-5">
						<h2 class="Section-title Section-title-naranja">Registrate con:</h2>
						<a href="{{ url('auth/social/facebook') }}" class="modalLogin-social-button btn btn-facebook btn-block btn-lg"><i class="fa fa-facebook"></i> Facebook</a>
					</div>
				</div>
				<br><br><br><br><br><br>--}}
				<div class="col-md-offset-4">

					<div align="center" class="col-md-5">
						<h2 class="Section-title Section-title-naranja">Registrate con:</h2>
						<a href="{{ url('auth/social/facebook') }}" class="modalLogin-social-button btn btn-facebook btn-block btn-lg"><i class="fa fa-facebook"></i> Facebook</a>
					</div>
					<br><br><br><br><br><br><br><br>
					<h2 class="Section-title Section-title-naranja">   Ó Registrate</h2>
				</div>
                                              
				<div class="col-md-offset-2">
					<div class="row">
						<div class="col-md-4">
							<p>Campos Obligatorios<span class="text-danger">*</span></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							{!! Form::open(['route' => 'register', 'method' => 'post', 'role' => 'form', "data-toggle" => "validator", 'autocomplete' => 'off']) !!}

							<div class="form-group">
								{!! Form::label('nombres', trans('validation.attributes.nombres')) !!}<span class="text-danger">*</span>
								{!! Form::text('nombres', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.nombres'),  'required' => true]) !!}
								<div class="help-block with-errors">
									@if ($errors->has('nombres')) {{ $errors->first('nombres') }} @endif
								</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									{!! Form::label('apellidos', trans('validation.attributes.apellidos')) !!}<span class="text-danger">*</span>
									{!! Form::text('apellidos', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.apellidos'), 'required' => true]) !!}
									<div class="help-block with-errors">
										@if ($errors->has('apellidos')) {{ $errors->first('apellidos') }} @endif
									</div>
								</div>
							</div>
							
						</div>		
						<div class="row">
							<div class="col-md-4">
								<div class="form-group @if ($errors->has('email')) has-error @endif">
									{!! Form::label('email', trans('validation.attributes.email')) !!}<span class="text-danger">*</span>
									{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.email'), 'required' => true]) !!}
									<div class="help-block with-errors">
										@if ($errors->has('email')) {{ $errors->first('email') }} @endif
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									
									{!! Form::label('fecha_nacimiento', trans('validation.attributes.fecha_nacimiento')) !!}
									{!! Form::text('fecha_nacimiento', null, ['class' => 'form-control datepicker', 'placeholder' => trans('validation.attributes.fecha_nacimiento'),  'required' => true]) !!}
									<div class="help-block with-errors">
										@if ($errors->has('fecha_nacimiento')) {{ $errors->first('fecha_nacimiento') }} @endif
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									{!! Form::label('genero', trans('validation.attributes.genero')) !!}
									{!! Form::select('genero', $generos, null, ['class' => 'form-control', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									{!! Form::label('id_departamento', trans('validation.attributes.id_departamento')) !!}
									{!! Form::select('id_departamento', $departamentos, null, ['class' => 'form-control', 'id' => 'departamento', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-4">	
								<div class="form-group">
									{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!}
									{!! Form::select('id_ciudad', ['' => 'Seleccione uno'], null, ['class' => 'form-control', 'id' => 'ciudad', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									{!! Form::label('password', trans('validation.attributes.password')) !!}<span class="text-danger">*</span>
									{!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => trans('validation.attributes.password'), 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									{!! Form::label('password_confirmation', trans('validation.attributes.password_confirmation')) !!}<span class="text-danger">*</span>
									{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.password_confirmation'), 'required' => true]) !!}
									<div class="help-block with-errors">
										@if ($errors->has('password')) {{ $errors->first('password') }} @endif
									</div>
								</div>
							</div>
						</div>
						
							
						<div class="checkbox">
							<label>
								<input type="checkbox" name="terminos"> Acepto los <a href="{{ route('front_terminos') }}" target="_blank">Términos y Condiciones</a>
								 
							</label>

						</div>
						<div class="checkbox">
							<label>
					                      
                            <input type="checkbox" name="acepto" class=""  value="acepto"> Acepto Recibir Emails</a>
							</label> 
						</div>
						

											

						<div class="row">
							<div class="col-md-offset-2">
								<div class="col-md-5">
									<button type="submit" class="btn btn-block btn-lg btn-azul">Registrarme</button>

								</div>
							</div>
						</div>
								

						
						{!! Form::close() !!}
					</div>
				</div>
			</div>

				
			</div>
		</div>
	</section>

<script id="tmp-ciudades" type="text/x-handlebars-template">
	@{{#each ciudades}}
		<option value="@{{id}}">@{{nombre}}</option>
	@{{/each}}
</script>

	@endsection