@extends('front.template.base')

@section('titulo')
Registrarse Como Aliado
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">Haz parte del Club Si Hay Para Hacer!</h1>
					<p class="Encabezado-lead">Conviertete en Aliado de SiHayParaHacer para que puedas publicar tus propios eventos</p>
				</div>
			</div>
		</div>
	</section>

	<section class="Section Section-blanco">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@include('admin.template.partials.errors')
					@include('admin.template.partials.success')
					@include('admin.template.partials.error')
				</div>

				<div class="col-md-12">
					<h2 class="Section-title text-center">Registrarse</h2>
				</div>

				{!! Form::open(['route' => 'aliado.register.post', 'method' => 'post', "data-toggle" => "validator"]) !!}
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('nombre_aliado', trans('validation.attributes.nombre_aliado')) !!} <span class="text-danger">*</span>
							{!! Form::text('nombre_aliado', null, ['class' => 'form-control', 'required' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('direccion_aliado', trans('validation.attributes.direccion_aliado')) !!} <span class="text-danger">*</span>
							{!! Form::text('direccion_aliado', null, ['class' => 'form-control', 'required' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('id_ciudad_aliado', trans('validation.attributes.id_ciudad_aliado')) !!}
							{!! Form::select('id_ciudad_aliado', $ciudades, null, ['class' => 'form-control selectpicker', 'data-selected-text-format' => "count>3", 'data-live-search' => "true", 'required' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('web_aliado', trans('validation.attributes.web_aliado')) !!}
							{!! Form::text('web_aliado', null, ['class' => 'form-control']) !!}
							<div class="help-block with-errors"></div>
						</div>

					</div>

					<div class="col-md-6">
						{{-- Contacto --}}
						<div class="form-group">
							{!! Form::label('nombre_contacto', trans('validation.attributes.nombre_contacto')) !!} <span class="text-danger">*</span>
							{!! Form::text('nombre_contacto', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Juan Arias']) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('email_contacto', trans('validation.attributes.email_contacto')) !!} <span class="text-danger">*</span>
							{!! Form::email('email_contacto', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'juan.arias@hotmail.com']) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('telefono_contacto', trans('validation.attributes.telefono_contacto')) !!} <span class="text-danger">*</span>
							{!! Form::text('telefono_contacto', null, ['class' => 'form-control', 'placeholder' => '4454454', 'required' => true,]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('password', trans('validation.attributes.password')) !!}
							{!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => trans('validation.attributes.password'), 'required' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('password_confirmation', trans('validation.attributes.password_confirmation')) !!}
							{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.password_confirmation'), 'required' => true]) !!}
							<div class="help-block with-errors">
								@if ($errors->has('password')) {{ $errors->first('password') }} @endif
							</div>
						</div>

						<div class="checkbox">
							<label>
								<input type="checkbox" name="terminos"> Acepto los <a href="{{ route('front_terminos') }}" target="_blank" >Términos y Condiciones</a>
							</label>
						</div>

						<button type="submit" class="btn btn-block btn-lg btn-naranja">Registrarme</button>
					</div>
				{!! Form::close() !!}

				<!-- <div class="col-md-6">
					<fieldset>
						<legend>Medio de Pago</legend>
					</fieldset>
				</div> -->
			</div>
		</div>
	</section>

@endsection