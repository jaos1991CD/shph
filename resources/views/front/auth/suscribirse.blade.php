@extends('front.template.base')

@section('titulo')
Suscribirse
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">Haz parte del Club Si Hay Para Hacer!</h1>
					<p class="Encabezado-lead">Haz parte del Club SiHayParaHacer y disfruta de una plataforma de confianza que le permite a sus usuarios disfrutar lo mejor de la ciudad .</p>
				</div>
			</div>
		</div>
	</section>

	<section class="Section Section-blanco">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@include('admin.template.partials.errors')
					@include('admin.template.partials.success')
					@include('admin.template.partials.error')
				</div>

				<div class="col-md-12 ">
					<h2 class="Section-title text-center">Suscribirse</h2>
				</div>


				<div class="col-md-offset-3">
					<div class="row flat ">

						{{--<div class="col-lg-3 col-md-3 col-xs-2">
						<ul class="plan plan1 ">
							<li class="plan-name">
								Pruebas
							</li>
							<li class="plan-price">
								<strong>**</strong> / mes
							</li>
							<li>
								<strong>Beneficios</strong> Ilimitados
							</li>
							<li class="plan-action">
								<a href="{{ route('vista_pagos') }}" class="btn btn-danger btn-block btn-lg ">Pago</a>
							</li>
						</ul>
					</div>--}}

						<div class="col-lg-4 col-md-3 col-xs-12">
							<ul class="plan plan2 featured">
								<li class="plan-name">
									Usuario
								</li>
								<li class="plan-price">
									<strong>Suscríbete como usuario Sí hay Para Hacer y disfruta los beneficios que tenemos para tí. Selecciona el plan que más se ajuste a tus necesidades.</strong>
									
								</li>
								<li class="plan-action">
									<a href="{{ route('register') }}" class="btn btn-danger btn-block btn-lg">Registrarme</a>
								</li>
							</ul>
						</div>
						<div class="col-lg-4 col-md-3 col-xs-12">
							<ul class="plan plan5 featured">
								<li class="plan-name">
									 Aliado
								</li>
								<li class="plan-price">
									<strong>Suscríbete como Aliado sin costo y publica tu evento.</strong> 
										<br><br><br><br>
									
								</li>
								<li class="plan-action">
									<a href="{{ route('aliado.register', ['plan' => 'gratis']) }}" class="btn btn-danger btn-block btn-lg">Registrarme</a>
								</li>
							</ul>
						</div>

						{{--<div class="col-lg-3 col-md-3 col-xs-6">
							<ul class="plan plan3 disabled">
								<li class="plan-name">
									Semestral
								</li>
								<li class="plan-price">
									<strong>**</strong> / Seis meses
								</li>
								<li>
									<strong>Beneficios</strong> Ilimitados
								</li>
								<li class="plan-action">
									<a href="#" class="btn btn-danger btn-block btn-lg disabled">Registrarme</a>
								</li>
							</ul>
						</div>--}}

						{{--<div class="col-lg-3 col-md-3 col-xs-6">
							<ul class="plan plan4 disabled">
								<li class="plan-name">
									Anual
								</li>
								<li class="plan-price">
									<strong>**</strong> / Anual
								</li>
								<li>
									<strong>Beneficios</strong> Ilimitados
								</li>
								<li class="plan-action">
									<a href="#" class="btn btn-danger btn-block btn-lg disabled">Registrarme</a>
								</li>
							</ul>
						</div>--}}
					</div>
				</div>

				{{--<div class="col-md-12 text-center">
					<a href="{{ route('vista_pagos') }}" class="btn btn-azul btn-lg">¡Quiero Ser Aliado!</a>
				</div>--}}
			</div>
		</div>
	</section>

@endsection