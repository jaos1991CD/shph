@extends('front.template.base')

@section('titulo')
Eventos
@endsection

@section('js')
	{!! Html::script('front/js/modulos/eventos.js') !!}
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-xs-12">
					@if (isset($categoria))
						<h1 class="Encabezado-titulo">{{ $categoria->nombre }}</h1>
					@else
						@if ($slug == 'buscar')
							<h1 class="Encabezado-titulo">Busqueda de eventos</h1>
						@else
							<h1 class="Encabezado-titulo">Sí hay para hacer</h1>
						@endif
					@endif
				</div>

				<!-- <div class="col-md-3">
					<div class="Encabezado-botones text-right">
						<a href="#" class="btn btn-facebook"><i class="fa fa-facebook fa-lg"></i></a>
						<a href="#" class="btn btn-twitter"><i class="fa fa-twitter fa-lg"></i></a>
					</div>
				</div> -->
			</div>
		</div>
	</section>

	<section class="Section Section-blanco">
		<div class="container">
			@if (count($eventos) > 0)
				<div class="row">
					<div class="col-md-12 col-xs-12">
						<h3 class="Section-title Section-title-naranja">Lista de Eventos</h3>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 col-xs-12">
						<section class="containerEventos" id="containerEventos">
							{{-- Eventos --}}
							@foreach ($eventos as $i => $evento)
								<div class="eventoCard-contenedor">
									<div class="eventoCard">
										<figure class="eventoCard-imagen">
											<a href="{{ route('front_evento', ['slug' => $evento->slug, 'ciudad' => $evento->ciudad->slug]) }}">
												<div class="eventoCard-imagen-cover"></div>
												<img src="{{ url('front/img/eventos/'.$evento->imagen->nombre) }}">
												<h4 class="eventoCard-titulo">{{ $evento->nombre }}</h4>
											</a>
										</figure>
										<div class="eventoCard-caption">
											<span class="eventoCard-ubicacion">
												<i class="fa fa-map-marker"></i>
												{{ $evento->lugar }}
											</span>
											<span class="eventoCard-fecha">
												<i class="fa fa-calendar"></i>
												{{ $evento->fechadesc }}  
											</span>
											<!-- <span class="eventoCard-fecha">
												<i class="fa fa-calendar"></i>
												<?php $horario = $evento->horarios()->whereRaw('concat(fecha, " ", hora) >= '.Carbon\Carbon::now()->toDateString())->orderBy('fecha')->first(); ?>
												{{ $horario->fecha_format }} {{ $horario->hora_format }} 
											</span> -->
											<div class="eventoCard-acciones">
												<a href="{{ route('front_evento', ['slug' => $evento->slug, 'ciudad' => $evento->ciudad->slug]) }}" class="btn btn-{{ (isset($categoria)) ? $categoria->color : 'azul' }} pull-right">Más Detalles</a>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						</section>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 col-xs-12 text-center">
						{!! str_replace('/?', '?', $eventos->render()) !!}
					</div>
				</div>
				
			@else
				<div class="row">
					<div class="col-md-12 col-xs-12 ">
						<div class="alert alert-info">
							<p>No tenemos eventos para esta Categoría.</p>
						</div>
					</div>
				</div>
				
			@endif
		</div>
		
	</section>
		
@endsection