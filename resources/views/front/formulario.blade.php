@extends('front.template.base')

@section('titulo')
Publica Tu Evento
@endsection

@section('css')
	{!! Html::style('front/css/vendor/dropzone.css') !!}
	{!! Html::style('front/css/vendor/cropper.css') !!}
	{!! Html::style('front/css/vendor/bootstrap-wizard.css') !!}
@endsection

@section('js')
	{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSynIVRyKUopyFqtQIiJW34VOBrt4oa48&callback=initMap" async defer></script> --}}
	{!! Html::script('front/js/vendor/dropzone.js') !!}
	{!! Html::script('front/js/vendor/cropper.min.js') !!}
	{!! Html::script('front/js/modulos/publica-evento.js'.'?'.str_random(4)) !!}
@endsection

@section('contenido')

	{!! Form::hidden('_token', csrf_token())!!}

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">@yield('titulo')</h1>
					<p class="Encabezado-lead">Estas organizando algún evento? Públicalo aquí</p>
				</div>
			</div>
		</div>
	</section>

	@if(Auth::check())
		@if(Auth::user()->tipo == 'aliado')
			<section class="Section Section-blanco homePublica">
				<div class="container">
					<div class="row">
						<div class="wizard">
							<div class="wizard-inner">
								<div class="connecting-line"></div>

								{{-- Header step to step --}}
								<ul class="nav nav-tabs" role="tablist">

									<!-- <li role="presentation">
										<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Paso 1. Información del Aliado">
											<span class="round-tab">
												<i class="fa fa-user"></i>
											</span>
										</a>
									</li> -->

									<li role="presentation" class="active">
										<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Paso 1. Imagen del Evento">
											<span class="round-tab">
												<i class="fa fa-image"></i>
											</span>
										</a>
									</li>
									<li role="presentation" class="disabled">
										<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Piso 2. Información del Evento">
											<span class="round-tab">
												<i class="fa fa-calendar"></i>
											</span>
										</a>
									</li>

									

									<li role="presentation" class="disabled">
										<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Completado">
											<span class="round-tab">
												<i class="glyphicon glyphicon-ok"></i>
											</span>
										</a>
									</li>
								</ul>

							</div>
										
								<div class="tab-content">
									
									{{-- Imagen Evento --}}
									<div class="tab-pane active" role="tabpanel" id="step1">
										{{-- Error imagen --}}
										<div class="hide" id="error-imagen">
											<div class="col-md-12">
												<div class="alert alert-danger"></div>
											</div>
										</div>

										<div class="col-md-12">
											{!! Form::open(['route' => 'front_evento_imagen', 'method' => 'post', 'id' => 'frm-imagenes', 'class' => 'dropzone']) !!}
												<div class="fallback">
													<input name="file" type="file" />
												</div>
											{!! Form::close() !!}

											<div class="homePublica-imagen"></div>
										</div>


										<ul class="list-inline pull-right botonera">
											<li><button type="button" class="btn btn-default prev-step btn-lg">Anterior <i class="fa fa-arrow-circle-o-left"></i></button></li>
											<li><button type="button" class="btn btn-primary btn-lg" id="btn-cortar-imagen">Siguente <i class="fa fa-arrow-circle-o-right"></i></button></li>
										</ul>
									</div>

									{{-- Informacion del evento --}}
									<div class="tab-pane" role="tabpanel" id="step2">
										{{-- Error evento --}}
										<div class="hide" id="error-evento">
											<div class="col-md-12">
												<div class="alert alert-danger"></div>
											</div>
										</div>
										{!! Form::open(['route' => ['front_evento_create'], 'method' => 'post', 'data-toggle' => "validator", 'id' => 'frm-evento']) !!}
											<div class="col-md-6">

												{!! Form::hidden('id_aliado', Auth::user()->aliado->id) !!}
												{!! Form::hidden('id_imagen', null) !!}

												<div class="form-group">
													{!! Form::label('nombre', trans('validation.attributes.nombre')) !!} <span class="text-danger">*</span>
													{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true]) !!}
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													{!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!} <span class="text-danger">*</span>
													{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '4', 'required' => true]) !!}
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													{!! Form::label('terminos', trans('validation.attributes.terminos')) !!} <span class="text-danger">*</span>
													{!! Form::textarea('terminos', null, ['class' => 'form-control', 'rows' => '5', 'required' => true]) !!}
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													{!! Form::label('direccion', trans('validation.attributes.direccion')) !!} <span class="text-danger">*</span>
													{!! Form::text('direccion', null, ['class' => 'form-control', 'required' => true]) !!}
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													{!! Form::label('lugar', trans('validation.attributes.lugar')) !!}
													{!! Form::text('lugar', null, ['class' => 'form-control']) !!}
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													{!! Form::label('web', trans('validation.attributes.web')) !!}
													{!! Form::text('web', null, ['class' => 'form-control']) !!}
													<div class="help-block with-errors"></div>
												</div>

												{{-- <div class="form-group">
													{!! Form::label('latitud', trans('validation.attributes.localizacion')) !!}
													<div id="mapa" style="height: 300px; width:100%;"></div>
													{!! Form::hidden('latitud', null, ['id' => 'latitud']) !!}
													{!! Form::hidden('longitud', null, ['id' => 'longitud']) !!}
												</div> --}}
											</div>

											<div class="col-md-6">

												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															{!! Form::label('fecha_inicio', trans('validation.attributes.fecha_inicio')) !!} <span class="text-danger">*</span>
															{!! Form::text('fecha_inicio', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
															<div class="help-block with-errors"></div>
														</div>
													</div>

													<div class="col-md-6">
														<div class="form-group">
															{!! Form::label('fecha_fin', trans('validation.attributes.fecha_fin')) !!} <span class="text-danger">*</span>
															{!! Form::text('fecha_fin', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
															<div class="help-block with-errors"></div>
														</div>
													</div>
												</div>

												<div class="form-group">
													{!! Form::label('precio', trans('validation.attributes.precio')) !!} <span class="text-danger">*</span>
													{!! Form::textarea('precio', null, ['class' => 'form-control', 'rows' => '3', 'required' => true]) !!}
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													{!! Form::label('descuento', trans('validation.attributes.descuento')) !!} <span class="text-danger">*</span>
													{!! Form::textarea('descuento', null, ['class' => 'form-control', 'rows' => '3']) !!}
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													{!! Form::label('fechadesc', trans('validation.attributes.fechadesc')) !!} <span class="text-danger">*</span>
													{!! Form::textarea('fechadesc', null, ['class' => 'form-control', 'rows' => '3', 'required' => true]) !!}
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													{!! Form::label('id_categoria', trans('validation.attributes.id_categoria')) !!} <span class="text-danger">*</span>
													{!! Form::select('id_categoria', $categorias, null, ['class' => 'form-control', 'required' => true]) !!}
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!} <span class="text-danger">*</span>
													{!! Form::select('id_ciudad', $ciudades, null, ['class' => 'form-control selectpicker', 'data-selected-text-format' => "count>3", 'data-live-search' => "true"]) !!}
													<div class="help-block with-errors"></div>
												</div>
												
												{{-- Horario --}}
												<fieldset id="ctn-horario">
													<legend>Horarios</legend>

													<div class="row">
														<div class="col-md-5">
															<div class="form-group">
																{!! Form::text('fecha[]', null, ['class' => 'form-control datepicker fecha', 'placeholder' => trans('validation.attributes.fecha')]) !!}
																<div class="help-block with-errors"></div>
															</div>
														</div>

														<div class="col-md-5">
															<div class="form-group">
																{!! Form::text('hora[]', null, ['class' => 'form-control datepicker-time hora', 'placeholder' => trans('validation.attributes.hora')]) !!}
																<div class="help-block with-errors"></div>
															</div>
														</div>

														<div class="col-md-2">
															<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
														</div>
													</div>

												</fieldset>
											</div>

											<div class="row">
												<div class="col-md-12">
													<ul class="list-inline pull-right botonera">
														<li><button type="button" class="btn btn-default prev-step btn-lg">Anterior <i class="fa fa-arrow-circle-o-left"></i></button></li>
														<li><button type="submit" class="btn btn-primary btn-lg" id="btn-finalizar">Finalizar <i class="fa fa-check"></i></button></li>
													</ul>
												</div>
											</div>
										{!! Form::close() !!}
									</div>
									
									
									{{-- Finish --}}
									<div class="tab-pane" role="tabpanel" id="complete">
										<div class="row sr-only" id="msj-success">
											<div class="col-md-6">
												
												<div class="alert alert-success">
													<p>Hemos recibido tu Evento, en unos momentos estará activo en nuestra web. Puedes ver el estado en tu <a href="{{ route('aliado_dashboard') }}">dashboard</a></p>
													<p>Verfica si los datos del evento son correctos para continuar  </p>
												</div>
												
												
												
												<div class="form-group">
													{!! Form::label('nombre', trans('validation.attributes.nombre')) !!} <span class="text-danger">*</span>
													{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true,'id'=>'i_nombre']) !!}
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													{!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!} <span class="text-danger">*</span>
													{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '4', 'required' => true, 'id'=>'i_descripcion']) !!}
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													{!! Form::label('terminos', trans('validation.attributes.terminos')) !!} <span class="text-danger">*</span>
													{!! Form::textarea('terminos', null, ['class' => 'form-control', 'rows' => '5', 'required' => true, 'id'=>'i_terminos']) !!}
													<div class="help-block with-errors"></div>
												</div>

												
											<button type="button" class="btn btn-primary btn-lg" id="btn-actualizar">Guardar <i class="fa fa-check"></i></button>
										
										</div>

										<div class="row sr-only" id="msj-error">
											<div class="col-md-12">
												<div class="alert alert-danger">
													<p>Ocurrió un error, por favor contactanos admin@sihayparahacer.com </p>
												</div>
											</div>
										</div>
									</div>

									<div class="clearfix"></div>

								</div>
						</div>
					</div>
				</div>
			</section>
		@else
			<section class="Section Section-blanco homePublica">
				<div class="container">
					<div class="alert alert-info">
						<p> no tienes permiso como aliado para publicar, regístrate como aliado <a href="{{ route('aliado.register') }}" >¡Quiero Ser Aliado!</a></p>
					</div>
				</div>
			</section>
		@endif
	@endif


	<script id="tmp-imagen" type="text/x-handlebars-template">
		<img class="img-responsive" id="image" src="@{{ url }}" data-id="@{{ id }}">
	</script>

	<script id="tmp-horario" type="text/x-handlebars-template">
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="fecha[]" class="form-control datepicker fecha" placeholder="Fecha">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="hora[]" class="form-control datepicker-time hora" placeholder="Hora">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-2">
				<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
			</div>
		</div>
	</script>

@endsection