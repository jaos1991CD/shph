@extends('front.template.base')

@section('titulo')
{{ $usuario->perfil->nombre_completo }}
@endsection

@section('css')
	{!! Html::style('front/css/vendor/dropzone.css') !!}
	{!! Html::style('front/css/vendor/cropper.css') !!}
@endsection

@section('js')
	{!! Html::script('front/js/vendor/dropzone.js') !!}
	{!! Html::script('front/js/vendor/cropper.min.js') !!}
	{!! Html::script('front/js/modulos/perfil.js') !!}
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-xs-12">
					<h1 class="Encabezado-titulo">Hola, {{ $usuario->perfil->nombre_completo }}</h1>
					<p class="Encabezado-lead">Aquí puedes ver toda la información de tu cuenta.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="Section Section-blanco">
		<div class="container">

			<div class="row">
				<div class="col-md-2 col-md-offset-5 col-xs-12">
					<div class="thumbnail">
						@if (is_null($usuario->perfil->imagen))
							<img src="{{ url('front/img/usuarios/avatar.jpg') }}" alt="" class="img-responsive" id="imagen-perfil" />
						@else
							@if (file_exists(public_path('front/img/usuarios/'.$usuario->perfil->imagen)))
								<img src="{{ url('front/img/usuarios/'.$usuario->perfil->imagen) }}" alt="" class="img-responsive" id="imagen-perfil" />
							@else
								<img src="{{ url('front/img/usuarios/avatar.jpg') }}" alt="" class="img-responsive" id="imagen-perfil" />
							@endif
						@endif

						<div class="caption">
							<a class="btn btn-default btn-xs btn-block" data-toggle="modal" href='#modalPerfil'>Cambiar</a>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-xs-12">
					@include('admin.template.partials.error')
					@include('admin.template.partials.errors')
					@include('admin.template.partials.success')
				</div>

				{{-- Perfil --}}

				{!! Form::open(['route' => 'front_perfil_post', 'method' => 'post', 'role' => 'form', "data-toggle" => "validator"]) !!}
					<h3>Perfil</h3>

					<div class="col-md-6 col-xs-12">
						<div class="form-group">
							{!! Form::label('nombres', trans('validation.attributes.nombres')) !!}
							{!! Form::text('nombres', $usuario->perfil->nombres, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.nombres'), 'required' => true]) !!}
							<div class="help-block with-errors">
								@if ($errors->has('nombres')) {{ $errors->first('nombres') }} @endif
							</div>
						</div>

						<div class="form-group">
							{!! Form::label('apellidos', trans('validation.attributes.apellidos')) !!}
							{!! Form::text('apellidos', $usuario->perfil->apellidos, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.apellidos'), 'required' => true]) !!}
							<div class="help-block with-errors">
								@if ($errors->has('apellidos')) {{ $errors->first('apellidos') }} @endif
							</div>
						</div>

						<div class="form-group @if ($errors->has('email')) has-error @endif">
							{!! Form::label('email', trans('validation.attributes.email')) !!}
							{!! Form::email('email', $usuario->correo, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.email'), 'required' => true]) !!}
							<div class="help-block with-errors">
								@if ($errors->has('email')) {{ $errors->first('email') }} @endif
							</div>
						</div>

						<div class="form-group">
							{!! Form::label('genero', trans('validation.attributes.genero')) !!}
							{!! Form::select('genero', $generos, $usuario->perfil->genero, ['class' => 'form-control', 'required' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>
						
					</div>

					<div class="col-md-6 col-xs-12">
						<div class="form-group">
							{!! Form::label('fecha_nacimiento', trans('validation.attributes.fecha_nacimiento')) !!}
							{!! Form::text('fecha_nacimiento', null, ['class' => 'form-control datepicker', 'placeholder' => trans('validation.attributes.fecha_nacimiento'),  'required' => true]) !!}
							<div class="help-block with-errors">
								@if ($errors->has('fecha_nacimiento')) {{ $errors->first('fecha_nacimiento') }} @endif
							</div>
						</div>

						<div class="row">
							<div class="col-md-6 col-xs-6">
								<div class="form-group">
									{!! Form::label('id_departamento', trans('validation.attributes.id_departamento')) !!}
									{!! Form::select('id_departamento', $departamentos, $usuario->perfil->ciudad->departamento->id, ['class' => 'form-control', 'id' => 'departamento', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>
							</div>

							<div class="col-md-6 col-xs-6">
								<div class="form-group">
									{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!}
									{!! Form::select('id_ciudad', ['' => 'Seleccione uno'], $usuario->perfil->ciudad->id, ['class' => 'form-control', 'id' => 'ciudad', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password">Cambiar Contraseña</label>
							{!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => trans('validation.attributes.password')]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							<label for="password">Confirmar Contraseña</label>
							{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.password_confirmation'), 'data-match' => "#password", 'data-match-error' => "La contraseña no coincide"]) !!}
							<div class="help-block with-errors">
								@if ($errors->has('password')) {{ $errors->first('password') }} @endif
							</div>
						</div>
					</div>

					<div class="col-md-12 col-xs-12 text-right">
						<button type="submit" class="btn btn-lg btn-naranja">Guardar</button>
					</div>
				{!! Form::close() !!}

				{{-- Imagen --}}
				<div class="col-md-12 col-xs-12">
					<h3>Historial de Suscripción</h3>
					
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>#</th>
									<th>Fecha de inicio </th>
									<th>Fecha de vencimiento </th>
									<th>Días Restante</th>
									<th>Estado</th>
									<th># Transaccion </th>
								</tr>
							</thead>

							<tbody>

								@foreach ($usuario->suscripcion as $i => $suscripcion)
								<tr @if($suscripcion->pivot->estado == 'activo') class="success" @endif>
									<td>{!!$i+1!!}</td>
									<td>{!!$suscripcion->pivot->fecha_inicio!!}</td>
									<td>{!!$suscripcion->pivot->fecha_vencimiento!!}</td>
									<?php
									$f_ini = Carbon\Carbon::now()->subDay()->endOfDay();
									$f_ven = Carbon\Carbon::parse($suscripcion->pivot->fecha_vencimiento);
									?>
									<td>{{ $f_ini->diffInDays($f_ven) }}</td>	
									<td>{!!$suscripcion->pivot->estado!!}</td>
									<td>{!! $suscripcion->pivot->idTransaccion!!}</td>				
								</tr>
								@endforeach
								
							</tbody>
						</table>

						<div class="row">
							<div class="col-md-12 col-xs-12">
								@if ( ! $usuario->suscripcion()->where('estado', 'activo')->exists())
								<a href="{{ route('vista_pagos', ['id' => $usuario->id]) }}" class="btn btn-primary btn-lg">Nueva Suscripción</a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="modalPerfil">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Cambiar Imagen de Perfil</h4>
				</div>
				<div class="modal-body">
					<div class="Perfil-imagen-upload">
						{!! Form::open(['route' => 'front_perfil_imagen_upload', 'method' => 'post', 'id' => 'frm-imagenes', 'class' => 'dropzone']) !!}
							<div class="fallback">
								<input name="file" type="file" />
							</div>
						{!! Form::close() !!}
					</div>

					<div class="Perfil-imagen-crop"> </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary Perfil-imagen-guardar">Terminar</button>
				</div>
			</div>
		</div>
	</div>

	<script id="tmp-imagen" type="text/x-handlebars-template">
		<img class="img-responsive" id="image" src="@{{ url }}">
	</script>

	<script id="tmp-ciudades" type="text/x-handlebars-template">
		@{{#each ciudades}}
			<option value="@{{id}}">@{{nombre}}</option>
		@{{/each}}
	</script>

@endsection