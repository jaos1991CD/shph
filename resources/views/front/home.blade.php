@extends('front.template.base')

@section('header')

	<div class="homeSlide">
		<div class="container">
		
			<div class="row">
				<div class="col-md-12">
					<h2 class="homeSlide-titulo">NUESTROS RECOMENDADOS</h2>
				</div>

				{{-- Slide --}}
				<div class="homeCarousel col-md-8 col-xs-12">
					<div class="aslider" data-slide="aslider" data-speed="500" data-wait="8000" data-preview="true">
						@if(count($slides)>0)
						@foreach ($slides as $i => $slide)

							<div id="s1" class="slide">

								<img src="{{ url('front/img/slide/'.$slide->imagen) }}">

								<div class="caption">
									<h3>{{ $slide->titulo }}</h3>
									<p>{{ $slide->descripcion }}</p>
								</div>
								<a href="{{ $slide->enlace }}" target="_blank">Abrir enlace</a>
							</div>

						@endforeach
						@endif

					</div>
				</div>

				<!-- video y banner -->
				<div class="col-md-4">
					<div class="row">
						<div class="homeSlide-video col-md-12 col-xs-12">
							<iframe style="width:100%;height:190px;" width="100%" src="https://www.youtube.com/embed/{{ $video->identificador }}" frameborder="0" allowfullscreen></iframe>
						</div>

						<div class="col-md-12">
							<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/Sihayparahacer" data-widget-id="697127663933194241">Tweets por el @Sihayparahacer.</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
@endsection

@section('contenido')

	<!-- Programacion -->
	<section class="Section Section-blanco homeProgramacion">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<h2 class="Section-title Section-title-azul">Programación</h2>
				</div>
			</div>

			<div class="row">
				<div class="col-md-3 col-xs-6 col-xs-12">
					<div class="homeProgramacion-item">
						<div class="homeProgramacion-item-back"></div>
						<a href="{{ route('front_eventos', ['slug' => 'hoy']) }}" class="homeProgramacion-item-a">
							<div class="homeProgramacion-item-img">
								<img src="{{ url('front/img/evento1-04.png') }}" class="img-responsive">
							</div>

							<div class="homeProgramacion-item-text">
								<h3>Hoy</h3>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-xs-6 col-xs-12">
					<div class="homeProgramacion-item">
						<div class="homeProgramacion-item-back"></div>
						<a href="{{ route('front_eventos', ['slug' => 'esta-semana']) }}" class="homeProgramacion-item-a">
							<div class="homeProgramacion-item-img">
								<img src="{{ url('front/img/evento2-02.png') }}" class="img-responsive">
							</div>

							<div class="homeProgramacion-item-text">
								<h3>Esta Semana</h3>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-xs-6 col-xs-12">
					<div class="homeProgramacion-item">
						<div class="homeProgramacion-item-back"></div>
						<a href="{{ route('front_eventos', ['slug' => 'fin-de-semana']) }}" class="homeProgramacion-item-a">
							<div class="homeProgramacion-item-img">
								<img src="{{ url('front/img/evento3-02.png') }}" class="img-responsive">
							</div>

							<div class="homeProgramacion-item-text ">
								<h3>Este Fin de Semana</h3>
							</div>
						</a>
					</div>
				</div>

				<div class="col-md-3 col-xs-6 col-xs-12">
					<div class="homeProgramacion-item">
						<div class="homeProgramacion-item-back"></div>
						<a href="{{ route('front_eventos', ['slug' => 'este-mes']) }}" class="homeProgramacion-item-a">
							<div class="homeProgramacion-item-img">
								<img src="{{ url('front/img/evento4-02.png') }}" class="img-responsive">
							</div>

							<div class="homeProgramacion-item-text">
								<h3>Este Mes</h3>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="Section Publica text-center">
		<div class="container">
			<a href="{{ route('front_publica') }}" class="btn btn-blanco btn-lg">Publica tu Evento</a>
		</div>
	</section>

@endsection