@extends('front.template.base')

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
		</div>

		{!! Form::hidden('imagen', url('front/img/eventos/'.$evento->imagen->nombre), ['id' => 'imagen']) !!}

		<div class="eventoInfo">

			<div class="col-md-6">
				<fieldset>
					<legend>Evento Actualizar</legend>
					{!! Form::model($evento, ['route' => ['front_aliado_actualizar', 'id' => $evento->id], 'method' => 'patch', 'role' => 'form', 'data-toggle' => "validator", 'files' => true]) !!}
					<div class="form-group">
						{!! Form::label('nombre', trans('validation.attributes.nombre')) !!}
						{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true]) !!}
						<div class="help-block with-errors"></div>
					</div>	
					<div class="form-group">
						{!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!}
						{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '4', 'required' => true]) !!}
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group">
						{!! Form::label('terminos', trans('validation.attributes.terminos')) !!}
						{!! Form::textarea('terminos', null, ['class' => 'form-control', 'rows' => '5', 'required' => true]) !!}
						<div class="help-block with-errors"></div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('direccion', trans('validation.attributes.direccion')) !!}
								{!! Form::text('direccion', null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('lugar', trans('validation.attributes.lugar')) !!}
								{!! Form::text('lugar', null, ['class' => 'form-control']) !!}
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<div class="form-group">
						{!! Form::label('web', trans('validation.attributes.web')) !!}
						{!! Form::text('web', null, ['class' => 'form-control']) !!}
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group">
						{!! Form::label('precio', trans('validation.attributes.precio')) !!}
						{!! Form::textarea('precio', null, ['class' => 'form-control','class' => 'ckeditor', 'rows' => '3', 'required' => true]) !!}
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						{!! Form::label('descuento', trans('validation.attributes.descuento')) !!}
						{!! Form::textarea('descuento', null, ['class' => 'form-control', 'class' => 'ckeditor', 'rows' => '3', 'required' => true]) !!}
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						{!! Form::label('fechadesc', trans('validation.attributes.fechadesc')) !!}
						{!! Form::text('fechadesc', null, ['class' => 'form-control', 'rows' => '3', 'required' => true]) !!}
						<div class="help-block with-errors"></div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('fecha_inicio', trans('validation.attributes.fecha_inicio')) !!}
								{!! Form::text('fecha_inicio', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('fecha_fin', trans('validation.attributes.fecha_fin')) !!}
								{!! Form::text('fecha_fin', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('id_aliado', trans('validation.attributes.id_aliado')) !!}
								{!! Form::select('id_aliado', $aliados, null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('id_categoria', trans('validation.attributes.id_categoria')) !!}
								{!! Form::select('id_categoria', $categorias, null, ['class' => 'form-control', 'required' => true]) !!}
								<div class="help-block with-errors"></div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!}
								{!! Form::select('id_ciudad', $ciudades, null, ['class' => 'form-control selectpicker', 'data-selected-text-format' => "count>3", 'data-live-search' => "true"]) !!}
								<div class="help-block with-errors"></div>
							</div>
						</div>		
					</div>
					<fieldset id="ctn-horario">
									<legend>Horarios</legend>

									@foreach ($evento->horarios as $i => $horario)
										<div class="row">
											<div class="col-md-5">
												<div class="form-group">
													{!! Form::text('fecha[]', $horario->fecha, ['class' => 'form-control datepicker', 'placeholder' => trans('validation.attributes.fecha')]) !!}
													<div class="help-block with-errors"></div>
												</div>
											</div>

											<div class="col-md-5">
												<div class="form-group">
													{!! Form::text('hora[]', $horario->hora, ['class' => 'form-control datepicker-time', 'placeholder' => trans('validation.attributes.hora')]) !!}
													<div class="help-block with-errors"></div>
												</div>
											</div>

											<div class="col-md-2">
												@if (count($evento->horarios) == $i+1)
													<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
												@endif
											</div>
										</div>
									@endforeach
								</fieldset>
								<button class="btn btn-primary btn-lg pull-right" type="submit"><i class="fa fa-save"></i> Editar Evento</button>
								
							{!! Form::close() !!}
					
				</fieldset>
			</div>
		</div>
	</div>

	<script id="tmp-imagen" type="text/x-handlebars-template">
		<img class="img-responsive" style="width:100%;" id="image" src="@{{ url }}" data-id="@{{ id }}">
	</script>

	<script id="tmp-horario" type="text/x-handlebars-template">
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="fecha[]" class="form-control datepicker" placeholder="Fecha">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="hora[]" class="form-control datepicker-time" placeholder="Hora">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-2">
				<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
			</div>
		</div>
	</script>
@endsection