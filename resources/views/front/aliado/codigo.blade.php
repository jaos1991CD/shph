@extends('front.template.base')

@section('titulo')
Codigos Suscripcion
@endsection


@section('contenido')

<div class="panel panel-default">
	<div class="panel-body">
		<div class="Contenido-botonera">
			<a href="{{ route('codigo_vista') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo</a>
		</div>
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.success')
			@include('admin.template.partials.error')
		</div>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nombre</th>
					<th>valor porcentaje</th>
					<th>estado</th>
					<th class="text-center">Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($codigos as $codigo)
				<tr>
					<td>{{ $codigo->id }}</td>
					<td>{{ $codigo->nombre }}</td>
					<td>{{ $codigo->valor }}</td>
					<td>{{ $codigo->estado }}</td>
					<td class="text-center">			
						<a href="{{route('codigo_editar', $codigo->id)}}" class= "btn btn-primary btn-xs">editar</a>							
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

</div>
{!! $codigos->render() !!}

@endsection