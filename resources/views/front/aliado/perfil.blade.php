@extends('front.template.base')

@section('titulo')
Perfil: {{ $aliado->nombre }}
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">Perfil: {{ $aliado->nombre }}</h1>
				</div>
			</div>
		</div>
	</section>

	<section class="Section Section-blanco">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@include('admin.template.partials.errors')
					@include('admin.template.partials.success')
					@include('admin.template.partials.error')
				</div>

				<div class="col-md-12">
					<h2 class="Section-title text-center">Aliado</h2>
				</div>

				{!! Form::open(['route' => ['aliado.perfil.patch', 'id' => $aliado->id], 'method' => 'patch', "data-toggle" => "validator"]) !!}
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('nombre_aliado', trans('validation.attributes.nombre_aliado')) !!} <span class="text-danger">*</span>
							{!! Form::text('nombre_aliado', $aliado->nombre, ['class' => 'form-control', 'required' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('direccion_aliado', trans('validation.attributes.direccion_aliado')) !!} <span class="text-danger">*</span>
							{!! Form::text('direccion_aliado', $aliado->direccion, ['class' => 'form-control', 'required' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('id_ciudad_aliado', trans('validation.attributes.id_ciudad_aliado')) !!}
							{!! Form::select('id_ciudad_aliado', $ciudades, $aliado->id_ciudad, ['class' => 'form-control selectpicker', 'data-selected-text-format' => "count>3", 'data-live-search' => "true", 'required' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('web_aliado', trans('validation.attributes.web_aliado')) !!}
							{!! Form::text('web_aliado', $aliado->web, ['class' => 'form-control']) !!}
							<div class="help-block with-errors"></div>
						</div>

					</div>

					<div class="col-md-6">
						{{-- Contacto --}}
						<div class="form-group">
							{!! Form::label('nombre_contacto', trans('validation.attributes.nombre_contacto')) !!} <span class="text-danger">*</span>
							{!! Form::text('nombre_contacto', $aliado->contacto->nombre, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Juan Arias']) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('telefono_contacto', trans('validation.attributes.telefono_contacto')) !!} <span class="text-danger">*</span>
							{!! Form::text('telefono_contacto', $aliado->contacto->telefono, ['class' => 'form-control', 'placeholder' => '4454454', 'required' => true,]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('password', trans('validation.attributes.password')) !!}
							{!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => trans('validation.attributes.password')]) !!}
							<div class="help-block with-errors"></div>
						</div>

						<div class="form-group">
							{!! Form::label('password_confirmation', trans('validation.attributes.password_confirmation')) !!}
							{!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('validation.attributes.password_confirmation')]) !!}
							<div class="help-block with-errors">
								@if ($errors->has('password')) {{ $errors->first('password') }} @endif
							</div>
						</div>

						<button type="submit" class="btn btn-block btn-lg btn-naranja">Registrarme</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</section>

@endsection