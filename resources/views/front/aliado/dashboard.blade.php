@extends('front.template.base')

@section('titulo')
Eventos
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">Dashboard</h1>
				</div>
			</div>
		</div>
	</section>
	<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.success')
			@include('admin.template.partials.error')
		</div>

	<section class="Section Section-blanco">
		<div class="container">

			<div class="row">
				<div class="col-md-3">
					<ul class="nav nav-pills nav-stacked">
						<li role="presentation" class="active"><a href="#"><i class="fa fa-calendar"></i> Eventos</a></li>
						<li role="presentation"><a href="{{ route('front_publica') }}"><i class="fa fa-plus"></i> Evento</a></li>
						<li role="presentation"><a href="{{ route('aliado_perfil') }}"><i class="fa fa-user"></i> Perfil</a></li>
						{{--  <li role="presentation"><a href="{{ route('codigo_suscripcion') }}"><i class="fa fa-user"></i> Codigos Descuento</a></li>--}}
					</ul>						
				</div>
				<div class="col-md-9">
					@if (count($eventos) > 0)
						<div class="row">
							{{-- Eventos --}}
							

							<table class="table table-condensed table-hover table-bordered table-striped">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Aliado</th>
										<th>En espera</th>
										<th>Acciones</th>
										
									</tr>
								</thead>
								<tbody>
									{!! Form::hidden('token', csrf_token(), ['id' => 'token']) !!}
									@foreach ($eventos as $i => $evento)
									<tr>
										<td>{{$evento->nombre}}</td>
										<td>{{$evento->aliado->nombre}}</td>
										<td>
											@if($evento->estado->id == 2)
											<span>Por Aprobar</span>
											@else
											<span>Activo</span>
											@endif 
										</td>
										
										<td class="text-center">
											<a href="{{ route('front_edit', ['evento' => $evento->id]) }}" class="btn btn-primary btn-xs">Editar</a>
											
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							
						
						</div>
					@else
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-info">
									<p>No tenemos eventos para esta Categoría.</p>
								</div>
							</div>
						</div>
						
					@endif
				</div>
			</div>
		</div>
	</section>

@endsection