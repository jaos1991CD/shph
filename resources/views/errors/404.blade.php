@extends('front.template.base')

@section('titulo')
Eventos
@endsection

@section('contenido')

	<section class="Encabezado">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1 class="Encabezado-titulo">Sí hay para hacer</h1>
					<p class="Encabezado-lead"></p>
				</div>

				<div class="col-md-3">
					<div class="Encabezado-botones text-right">
						<a href="#" class="btn btn-facebook"><i class="fa fa-facebook fa-lg"></i></a>
						<a href="#" class="btn btn-twitter"><i class="fa fa-twitter fa-lg"></i></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="Section Section-blanco">
		<div class="container">
			<h1>Error 404</h1>
		</div>
	</section>

@endsection