<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>@yield('titulo', 'Inicio') | {{ env('NAME', 'Cms') }}</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	
	{!! Html::style('admin/css/bootstrap-admin.css') !!}
	{!! Html::style('admin/css/main.css') !!}

	@yield('css', '')
</head>
<body>

	{{-- Navegacion --}}
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{ url('/') }}">{{ env('NAME', 'Cms') }}</a>
		</div>
		{{-- Menu Top --}}
		@include('admin.template.header')
	</nav>

	<div class="container-fluid">

		<div class="Login row">
			<div class="Login-content col-md-4 col-md-offset-4">

				@include('admin.template.partials.errors')
				
				{!! Form::open(['url' => 'auth/login', 'method' => 'post', 'role' => 'form']) !!}
					<legend>Iniciar Sesión</legend>
				
					<div class="form-group">
						{!! Form::email('correo', null, ['class' => 'form-control input-lg', 'placeholder' => trans('validation.attributes.email'), 'required' => true, 'autofocus' => true]) !!}
					</div>
					<div class="form-group">
						{!! Form::password('password', ['class' => 'form-control input-lg', 'placeholder' => "Password", 'required' => true]) !!}
					</div>

					<div class="checkbox">
						<label>
							<input type="checkbox" name="rememberme"> Recordarme
						</label>
					</div>

					<button type="submit" class="btn btn-lg btn-primary btn-block">Entrar</button>

					{{-- <a href="#" class="btn btn-link btn-block">Recordar mi contraseña</a> --}}
				
					
				
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	{!! Html::script('admin/js/main.js') !!}
</body>
</html>