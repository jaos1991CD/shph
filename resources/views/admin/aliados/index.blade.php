@extends('admin.template.base')

@section('titulo')
Aliados
@endsection

@section('breadcrumbs')
{!! Breadcrumbs::render('aliados') !!}
@endsection

@section('contenido')

	<div class="panel panel-default">
		<div class="panel-body">
			<div class="Contenido-botonera">
				<a href="{{ route('administrador.aliados.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo</a>
				{{--<a href="{{ route('excel_aliado') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Descargar reporte</a>--}}
			</div>

			<div class="Contenido-botonera">
				{!! Form::model(Request::only('fecha_ini', 'fecha_fin'), ['route' => 'administrador.aliados.index', 'method' => 'get', 'class' => 'form-inline pull-right']) !!}
					
					{!! Form::text('fecha_ini', null, ['class' => 'form-control datepicker', 'placeholder' => 'Fecha Registro Inicio']) !!}
					{!! Form::text('fecha_fin', null, ['class' => 'form-control datepicker', 'placeholder' => 'Fecha Registro Fin']) !!}
					
					<button class="btn btn-primary" type="submit" name="buscar" value="1"><i class="fa fa-search"></i> Buscar</button>
					<button class="btn btn-primary" type="submit" name="exportar" value="2"><i class="fa fa-download"></i> Exportar</button>
				{!! Form::close() !!}
			</div>

			@include('admin.template.partials.success')
		</div>
		@if (count($aliados) > 0)
			<div class="table-responsive">
				<table class="table table-condensed table-hover table-bordered table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>Cant. Eventos</th>
							<th>Estado</th>
							<th class="text-center">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($aliados as $aliado)
							<tr>
								<td>{{ $aliado->id }}</td>
								<td>{{ $aliado->nombre }}</td>
								<td>{{ count($aliado->eventos) }}</td>
								<td>{{ $aliado->estado }}</td>
								<td class="text-center">
									{!! Form::open(['route' => ['administrador.aliados.destroy', 'id' => $aliado->id], 'method' => 'delete']) !!}
										<a href="{{ route('administrador.aliados.edit', ['aliado' => $aliado->id]) }}" class="btn btn-primary btn-xs">Editar</a>
										<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Se borrará toda la información referente a este aliado\nSeguro que desea eliminar este registro?');">Eliminar</button>
									{!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="alert alert-info">
				<p>Lo sentimos, no hay registro de Notificaciones para mostrar.</p>
			</div>
		@endif
	</div>
	{!! $aliados->render() !!}

@endsection