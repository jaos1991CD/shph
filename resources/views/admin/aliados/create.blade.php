@extends('admin.template.base')

@section('titulo')
Crear Aliado
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('aliados_create') !!}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
		</div>

		{!! Form::open(['route' => 'administrador.aliados.store', 'method' => 'post', 'role' => 'form', 'data-toggle' => "validator"]) !!}
			<div class="col-md-6">

				<div class="form-group">
					{!! Form::label('nombre', trans('validation.attributes.nombre')) !!}
					{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('nit', trans('validation.attributes.nit')) !!}
					{!! Form::text('nit', null, ['class' => 'form-control']) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('web', trans('validation.attributes.web')) !!}
					{!! Form::text('web', null, ['class' => 'form-control']) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('estado', trans('validation.attributes.estado')) !!}
					{!! Form::select('estado', config('lists.estados'), null, ['class' => 'form-control']) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!}
					{!! Form::select('id_ciudad', $ciudades, null, ['class' => 'form-control selectpicker', 'data-selected-text-format' => "count>3", 'data-live-search' => "true"]) !!}
					<div class="help-block with-errors"></div>
				</div>

			</div>

			<div class="col-md-6">

				<div class="form-group">
					{!! Form::label('nombre_contacto', trans('validation.attributes.nombre_contacto')) !!}
					{!! Form::text('nombre_contacto', null, ['class' => 'form-control', 'required' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('email_contacto', trans('validation.attributes.email_contacto')) !!}
					{!! Form::email('email_contacto', null, ['class' => 'form-control', 'required' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('telefono_contacto', trans('validation.attributes.telefono_contacto')) !!}
					{!! Form::text('telefono_contacto', null, ['class' => 'form-control']) !!}
					<div class="help-block with-errors"></div>
				</div>

			</div>

			<div class="col-md-12">
				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Guardar</button>
			</div>
		{!! Form::close() !!}

	</div>
@endsection