@extends('admin.template.base')

@section('titulo')
Gestionar Imágenes Slide
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('slide') !!}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
			@include('admin.template.partials.success')
		</div>

		{!! Form::open(['route' => 'admin_contenido_slide_post', 'method' => 'post', 'role' => 'form', 'data-toggle' => "validator", 'files' => true]) !!}
			<div class="col-md-6">

				<div class="form-group">
					{!! Form::label('imagen', trans('validation.attributes.imagen')) !!}
					{!! Form::file('imagen', ['required' => true]) !!}
					<div class="help-block with-errors"></div>
					<div class="help-block">Medidas 800px x 500px</div>
				</div>

				<div class="form-group">
					{!! Form::label('titulo', trans('validation.attributes.titulo')) !!}
					{!! Form::text('titulo', null, ['class' => 'form-control', 'id' => 'titulo', 'required' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('enlace', trans('validation.attributes.enlace')) !!}
					{!! Form::text('enlace', null, ['class' => 'form-control', 'required' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!}
					{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '3']) !!}
					<div class="help-block with-errors"></div>
				</div>

				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Guardar</button>
			</div>
		{!! Form::close() !!}

		{{-- Lista de slides --}}

	</div>

	<div class="row" style="margin-top: 16px;">
		@foreach ($slides as $slide)
			<div class="col-md-4">
				<div class="thumbnail">
					<img src="{{ url('/front/img/slide/'.$slide->imagen) }}" alt="{{ $slide->imagen }}">
					<div class="caption">
						<h4>{{ $slide->titulo }}</h4>
						<a href="{{ $slide->enlace }}" target="_blank">{{ $slide->enlace }}</a>
						<p>
							{!! Form::open(['route' => ['admin_contenido_slide_delete', 'id' => $slide->id], 'method' => 'delete', 'role' => 'form', 'data-toggle' => "validator", 'files' => true]) !!}
							<a href="{{ route('admin_contenido_slide_edit', ['id' =>$slide->id]) }}" class="btn btn-primary btn-xs">Editar</a>
								<button type="submit" class="btn btn-xs btn-danger">Eliminar</button>
							{!! Form::close() !!}
						</p>
					</div>
				</div>
			</div>
		@endforeach
	</div>
@endsection