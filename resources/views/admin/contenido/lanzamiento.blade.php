<!DOCTYPE html>
<html lang="es">
  
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Descuentos para eventos">
    <meta name="keywords" content="descuento, promocion, cali, eventos, teatro, cultura, discoteca, cine, rumba, vacaiones, talleres"> 

    <title>Si hay para hacer | Afíliate hoy</title>
    <meta name="viewport" content="width=device-width, maximum-scale=1, user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="shortcut icon" href="assets/img/icon_logo.ico" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>

    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/baguetteBox.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/demo.css">
    <link rel="stylesheet" href="assets/css/fonts.css">
  </head>
  <body class="no-scroll">
    <div class="site-wrapper">
      <!-- Main wrapper-->
      <div class="top-bar">
        <div class="container">
          <nav  class="nav">
            <div  class="brand-logo pull-left">
              <!-- Site logo--><a href="#home" class="smooth-scrolling"><img src="assets/img/logo-si-hay-para-hacer.jpg" alt="Logo"></a>
            </div>
            <ul class="list-inline pull-right" >
              <!-- Navigation-->
              <li><a href="#works" class="smooth-scrolling">Como hacerlo</a></li>
              <li><a href="#detailed" class="smooth-scrolling">Regístrate</a></li>
              <li><a href="#testimonios" class="smooth-scrolling">Testimonios</a></li>
            </ul>
            <button class="fa fa-bars open-mobile-menu"></button>
          </nav>
        </div>
      </div>
      <header  id="home" class="header">
        <!-- Header begin-->
        <div class="header-section">
          <div class="bg">
            <div class="container">
              <div class="row">
                <div class="col-sm-6 text-afiliate">
                  <h1><strong>AFILIATE HOY</strong><br>POR SOLO $20.000</h1>
                  <p>Únete a la comunidad <strong>Si Hay Para Hacer</strong> y disfruta un mundo de <strong>beneficios en promociones</strong> para eventos <strong class="blod-rose">durante un año.</strong> </p>
                 <a href="http://www.sihayparahacer.com/register" target="_blank" >  <button  class="btn">¡AFÍLIATE AHORA!</button></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <section id="works" class="section">
        <!-- Section begin-->
        <div class="container">
          <h2 class="section-title">Como HACERLO</h2>
          <div class="row">
            <div class="col-sm-3 step">
              <div class="step-icon fa fa-pencil"></div>
              <div class="text">
                <h4>Suscríbete</h4>
                <p>Ingresa tus datos en la sección de registro</p>
              </div>
            </div>
            <div class="col-sm-3 step">
              <div class="step-icon icon icon-calendar-check-o"></div>
              <div class="text">
                <h4>Escoge tu afiliación</h4>
                <p>Selecciona tu tipo de afiliación y paga.</p>
              </div>
            </div>
            <div class="col-sm-3 step">
              <div class="step-icon fa fa-envelope"></div>
              <div class="text">
                <h4>Confirma tu e-mail</h4>
                <p>Revisa tu correo y confirma tu registro.</p>
              </div>
            </div>
            <div class="col-sm-3 step">
              <div class="step-icon icon icon-sale-04"></div>
              <div class="text">
                <h4>Descuentos</h4>
                <p>Disfruta de los eventos de la ciudad con exelentes descuentos.</p>
              </div>
            </div>
          </div>
        </div>
        <!-- Section end-->
      </section>
      <section id="detailed" class="section gray">
        <!-- Section begin-->
        <div class="container">
          <h2 class="section-title">REGÍSTRATE</h2>
          <div> <p>Afíliate hoy, aprovecha el descuento y disfruta de promociones por un año para eventos en:</p></div>
          <div class="image-slider">
            <!--Teatro-->
            <div class="row section-table">
              <div class="col-sm-7 features"><img src="assets/img/teatro.jpg" alt="featured">
              </div>
              <div class="col-sm-5">
                <h2><strong>Teatro</strong></h2>
                <p>Descuentos y beneficios en las mejores obras de teatro en Cali.</p>
                <a href="http://www.sihayparahacer.com/register" target="_blank" >  <button  class="btn">Regístrate</button></a>
              </div>
            </div>
            <!--Cine-->
            <div class="row section-table">
              <div class="col-sm-7 features"><img src="assets/img/cine.jpg" alt="featured">
              </div>
              <div class="col-sm-5">
                <h2><strong>Cine</strong></h2>
                <p>Funciones especiales exclusivas para los socios y boletas con descuento.</p>
                <a href="http://www.sihayparahacer.com/register" target="_blank" >  <button  class="btn">Regístrate</button></a>
              </div>
            </div>
            <!--Rumba-->
            <div class="row section-table">
              <div class="col-sm-7 features"><img src="assets/img/rumba.jpg" alt="featured">
              </div>
              <div class="col-sm-5">
                <h2><strong>Rumba</strong></h2>
                <p>Combo de entradas y algo de tomar en los mejores sitios de la rumba caleña.</p>
                <a href="http://www.sihayparahacer.com/register" target="_blank" >  <button  class="btn">Regístrate</button></a>
              </div>
            </div>
            <!--Vacaciones-->
            <div class="row section-table">
              <div class="col-sm-7 features"><img src="assets/img/vacaciones.png" alt="featured">
              </div>
              <div class="col-sm-5">
                <h2><strong>Vacaciones</strong></h2>
                <p>Programa tus vacaciones con Si Hay para Hacer y consigue descuentos en los planes todo incluido que tenemos para ti.</p>
                <a href="http://www.sihayparahacer.com/register" target="_blank" >  <button  class="btn">Regístrate</button></a>
              </div>
            </div>
            <!--Talleres-->
            <div class="row section-table">              
              <div class="col-sm-7 features"><img src="assets/img/taller.jpg" alt="featured">
              </div>
              <div class="col-sm-5">
                <h2><strong>Talleres</strong></h2>
                <p>¿Fotografía, danza, teatro, manualidades? Tenemos todos los talleres con descuento para que desarrolles tu creatividad. </p>
                <a href="http://www.sihayparahacer.com/register" target="_blank" >  <button  class="btn">Regístrate</button></a>
              </div>
            </div>
          </div>
        </div>
        <!-- Section end-->
      </section>

    <!-- <div id="map"></div>-->
      <section id="testimonios" data-stellar-background-ratio="0.7" class="section bg testimonials-section">
        <!-- Section begin-->
       <div class="container">
          <h2 class="section-title">Testimonios</h2>
          <div class="row">
            <div class="testimonials-slider">
              <div class="slide media">
                <div class="media-left"><img src="assets/img/coment.png" alt="customer 1">
                </div>
                <div class="media-body">
                  <h4>JORGE PINZÓN</h4>
                  <p>En Cali hay muchas actividades para hacer durante la semana. Me parece muy bueno que te mantengas informado y además que haya descuento para asistir. Super recomendado! .</p>
                  
                </div>
              </div>
              <div class="slide media">
                <div class="media-left"><img src="assets/img/coment.png" alt="customer 1">
                </div>
                <div class="media-body">
                  <h4>DIANA ZAMBRANO</h4>
                  <p>Fui a una obra de teatro con el 25% de descuento en las boletas. Me encantó!.</p>
                  
                </div>
              </div>

            </div>
          </div>
        </div>
      </section>


     
     <section id="map">  </section>
         
      <footer class="footer">
        <!-- Footer begin-->
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
              <ul class="list-inline text-center">
              
                <li><a href="https://www.facebook.com/sihayparahacer1/?fref=ts" target="_blank" class="social"><i class="fa fa-facebook"></i></a></li>
               
              </ul>
              <p class="copy">Copyright © 2016 Si hay para hacer</p>
            </div>
          </div>
        </div>
        <!-- Footer end-->
      </footer>
    </div>
    <div class="loader">
      <!-- Loader-->
      <div class="spinner"></div>
    </div>
    <!-- Scripts-->
    <script src="assets/js/vendors/jquery.min.js"></script>
    <script src="assets/js/vendors/bootstrap.min.js"></script>
    <script src="assets/js/vendors/jquery.stellar.min.js"></script>
    <script src="assets/js/vendors/jquery.ajaxchimp.min.js"></script>
    <script src="assets/js/vendors/scrollReveal.min.js"></script>
    <script src="assets/js/vendors/baguetteBox.min.js"></script>
    <script src="assets/js/vendors/slick.min.js"></script>
    <script src="assets/js/vendors/jquery.flexslider-min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=false"></script>
    <script src="assets/js/main.js"></script>
  </body>
</html>