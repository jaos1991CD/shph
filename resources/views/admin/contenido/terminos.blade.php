@extends('admin.template.base')

@section('titulo')
Editar Sección Términos y Condiciones
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('terminos') !!}
@endsection

@section('js')
	{!! Html::script('admin/js/vendor/summernote.min.js') !!}
	{!! Html::script('admin/js/vendor/lang/summernote-es-ES.js') !!}

	<script type="text/javascript">
		$(document).ready(function() {
			$('#summernote').summernote({
				toolbar: [
				['style', ['bold', 'italic', 'underline', 'clear']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']],
				]
			});
		});


	</script>
@endsection

@section('css')
	{!! Html::style('admin/css/vendor/summernote.css') !!}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
			@include('admin.template.partials.success')
		</div>

		{!! Form::model($pagina, ['route' => 'admin_contenido_terminos_post', 'method' => 'post', 'role' => 'form', 'data-toggle' => "validator"]) !!}
			<div class="col-md-6">

				<div class="form-group">
					{!! Form::label('titulo', trans('validation.attributes.titulo')) !!}
					{!! Form::text('titulo', null, ['class' => 'form-control', 'id' => 'titulo', 'required' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				{!! Form::textarea('texto', null, ['id' => 'summernote']) !!}


				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Guardar</button>
			</div>
		{!! Form::close() !!}

	</div>
@endsection