@extends('admin.template.base')

@section('titulo')
Gestionar Video
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('video') !!}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
			@include('admin.template.partials.success')
		</div>

		{!! Form::model($video, ['route' => 'admin_contenido_video_post', 'method' => 'post', 'role' => 'form', 'data-toggle' => "validator"]) !!}
			<div class="col-md-6">

				<div class="form-group">
					{!! Form::label('identificador', trans('validation.attributes.identificador')) !!}
					{!! Form::text('identificador', null, ['class' => 'form-control', 'id' => 'identificador', 'required' => true]) !!}
					{!! Form::hidden('id', $video->id) !!}
					<div class="help-block with-errors"></div>
				</div>
				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Guardar</button>
			</div>

			<div class="col-md-6">
				<iframe style="width:100%;height:250px;" width="100%" src="https://www.youtube.com/embed/{{ $video->identificador }}" frameborder="0" allowfullscreen></iframe>
			</div>
		{!! Form::close() !!}

	</div>
@endsection