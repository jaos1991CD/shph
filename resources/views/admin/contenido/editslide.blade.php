@extends('admin.template.base')

@section('titulo')
Gestionar Imágenes Slide
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('slide') !!}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
			@include('admin.template.partials.success')
		</div>

		{!! Form::model($slide, ['route' => ['admin_contenido_slide_update','id' => $slide->id], 'method' => 'patch', 'role' => 'form', 'data-toggle' => "validator", 'files' => true]) !!}
		
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('imagen', trans('validation.attributes.imagen')) !!}
					{!! Form::file('imagen', ['required' => true]) !!}
					<div class="help-block with-errors"></div>
					<div class="help-block">Medidas 800px x 500px</div>
				</div>

				<div class="form-group">
					{!! Form::label('titulo', trans('validation.attributes.titulo')) !!}
					{!! Form::text('titulo', null, ['class' => 'form-control', 'id' => 'titulo', 'required' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('enlace', trans('validation.attributes.enlace')) !!}
					{!! Form::text('enlace', null, ['class' => 'form-control', 'required' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!}
					{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '3']) !!}
					<div class="help-block with-errors"></div>
				</div>

				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Guardar</button>
			</div>
		{!! Form::close() !!}

		{{-- Lista de slides --}}

	</div>

	
@endsection