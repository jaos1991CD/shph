@extends('admin.template.base')

@section('titulo')
Codigos Suscripcion
@endsection


@section('contenido')

<div class="panel panel-default">
	<div class="panel-body">
		<div class="Contenido-botonera">
			<a href="{{ route('administrador.codigosuscripcion.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo</a>
		</div>
		<div class="Contenido-botonera">
			{!! Form::open(['route' => ['administrador.codigosuscripcion.store'], 'method' => 'POST',  'class' => 'form-inline pull-right', 'enctype'=>'multipart/form-data']) !!}
				
				<input type="file" name="importar" class="filestyle" data-buttonText="Abrir Archivo" required/>
				<button class="btn btn-primary" type="submit" ><i class="fa fa-upload"></i> importar</button>
					
				{!! Form::close() !!}
		</div>
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.success')
			@include('admin.template.partials.error')
		</div>
	</div>
	
	<div class="table-responsive">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nombre</th>
					<th>valor porcentaje</th>
					<th>estado</th>
					<th class="text-center">Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($codigos as $codigo)
				<tr>
					<td>{{ $codigo->id }}</td>
					<td>{{ $codigo->nombre }}</td>
					<td>{{ $codigo->valor }}</td>
					<td>{{ $codigo->estado }}</td>
					<td class="text-center">
						{!! Form::open(['route' => ['administrador.codigosuscripcion.destroy', 'id' => $codigo->id], 'method' => 'delete']) !!}
						{{--  <a href="{{route('administrador.codigosuscripcion.edit', $codigo->id)}}" class= "btn btn-primary btn-xs">editar</a>--}}
						<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Se borrará toda la información referente a este Codigo\nSeguro que desea eliminar este registro?');">Eliminar</button>
						{!! Form::close() !!}							
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

</div>
{!! $codigos->render() !!}

@endsection

