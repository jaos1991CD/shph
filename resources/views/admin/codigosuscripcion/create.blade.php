@extends('admin.template.base')

@section('titulo')
Crear codigo
@endsection

@section('contenido')
	<section class="Section Section-blanco">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@include('admin.template.partials.errors')
					@include('admin.template.partials.success')
					@include('admin.template.partials.error')
				</div>
				<div class="col-md-offset-4">
					<h2 class="Section-title Section-title-naranja">Crear Codigo</h2>
				</div>                                             
				<div class="col-md-offset-1">
					<div class="row">
						<div class="col-md-3">
							{!! Form::open(['route' => 'administrador.codigosuscripcion.store', 'method' => 'post', 'role' => 'form', "data-toggle" => "validator", 'autocomplete' => 'off']) !!}
							<div class="form-group">
								{!! Form::label('nombre', 'Nombre ') !!}<span class="text-danger">*</span>
								{!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Codigf152',  'required' => true]) !!}
								
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{!! Form::label('valor', 'porcentaje') !!}<span class="text-danger">*</span>
								{!! Form::text('valor', null, ['class' => 'form-control', 'placeholder' => '25', 'required' => true]) !!}
								
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{!! Form::label('id_aliado', trans('validation.attributes.id_aliado')) !!}
								{!! Form::select('id_aliado', $aliados, null, ['class' => 'form-control', 'required' => true]) !!}
							</div>
						</div>
					</div>	

					<div class="row">
						<div class="col-md-offset-2">
							<div class="col-md-5">
								<button type="submit" class="btn btn-block btn-lg btn-azul">Crear</button>

							</div>
						</div>
					</div>		
					{!! Form::close() !!}
				</div>
			</div>
		</div>


	</div>
</div>
</section>



	@endsection