@extends('admin.template.base')

@section('titulo')
Comentarios
@endsection

@section('breadcrumbs')
{!! Breadcrumbs::render('comentarios') !!}
@endsection

@section('contenido')

	<div class="panel panel-default">
		<div class="panel-body">
			<div class="Contenido-botonera">
				<a href="{{ route('administrador.comentarios.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo</a>
			</div>

			@include('admin.template.partials.success')
		</div>
		@if (count($comentarios) > 0)
			<div class="table-responsive">
				<table class="table table-condensed table-hover table-bordered table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre Usuario</th>
							<th>Comentario</th>
							<th>Evento</th>
							<th>Estado</th>
							<th>Calificación</th>
							<th class="text-center">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($comentarios as $comentario)
							<tr @if($comentario->estado == 'inactivo') class="danger" @endif>
								<td>{{ $comentario->id }}</td>
								<td>{{ $comentario->usuario->perfil->nombre_completo }}</td>
								<td>"<i>{{ $comentario->mensaje }}</i>"</td>
								<td>{{ $comentario->evento->nombre }}</td>
								<td>{{ $comentario->estado }}</td>
								<td>{{ $comentario->calificacion }}</td>
								<td class="text-center">
									{!! Form::open(['route' => ['administrador.comentarios.update', 'id' => $comentario->id], 'method' => 'patch']) !!}
										@if ($comentario->estado != 'activo')
											<button type="submit" name="aprobar" value="1" class="btn btn-success btn-xs">Aprobar</button>
										@endif
										<button type="submit" name="eliminar" value="1" class="btn btn-danger btn-xs">Eliminar</button>
									{!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="alert alert-info">
				<p>Lo sentimos, no hay registro de Notificaciones para mostrar.</p>
			</div>
		@endif
	</div>
	{!! $comentarios->render() !!}

@endsection