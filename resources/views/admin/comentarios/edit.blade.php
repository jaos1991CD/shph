@extends('admin.template.base')

@section('titulo')
Ver Comentario
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('comentarios_edit') !!}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
		</div>

		{!! Form::model($comentario, ['route' => ['administrador.comentarios.update', 'id' => $comentario->id], 'method' => 'patch', 'role' => 'form', 'data-toggle' => "validator"]) !!}
			<div class="col-md-6">
				
			</div>
		{!! Form::close() !!}

	</div>
@endsection