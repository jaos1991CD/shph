@extends('admin.template.base')

@section('titulo')
Nuevo Usuario
@endsection



@section('contenido')
	<div class="row">

		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
		</div>
			
			{!! Form::model($suscripcion, [ 'route' => ['administrador.suscripcion.update', $suscripcion->id ], 'method' => 'PUT', 'role' => 'form', 'data-toggle' => "validator"])!!}
		
			<div class="col-md-6">

			
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('nombre', 'Nombre') !!}
							{!! Form::text('nombre', null, ['class' => 'form-control']) !!}
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('descripcion', 'Descripcion') !!}
							{!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
							<div class="help-block with-errors"></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('valor', 'Valor') !!}
							{!! Form::text('valor', null, ['class' => 'form-control']) !!}
							<div class="help-block with-errors"></div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							
							{!! Form::label('tipo', 'Tipo') !!}
							{!! Form::text('tipo', null, ['class' => 'form-control']) !!}
							<div class="help-block with-errors"></div>
						</div>
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('meses', 'Meses') !!}
					{!! Form::text('meses', null, ['class' => 'form-control']) !!}
					<div class="help-block with-errors"></div>
				</div>

				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Guardar</button>

			</div>

			
			
		{!! Form::close() !!}

	</div>
@endsection