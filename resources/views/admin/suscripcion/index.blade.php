@extends('admin.template.base')

@section('titulo')
Suscripcion
@endsection



@section('contenido')

	<div class="panel panel-default">
		<div class="panel-body">
			<div class="Contenido-botonera">
			 <a href="{{ route('administrador.suscripcion.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo</a> 
			</div>
		
			@include('admin.template.partials.success')
		</div>
		@if (count($suscripciones) > 0)
			<div class="table-responsive">
				<table class="table table-condensed table-hover table-bordered table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>Descripcion</th>
							<th>Valor</th>
							<th>Tipo</th>
							<th>Meses</th>
							<th class="text-center">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($suscripciones as $suscripcion)
							<tr>
								<td>{{ $suscripcion->id }}</td>
								<td>{{ $suscripcion->nombre }}</td>
								<td>{{ $suscripcion->descripcion }}</td>
								<td>{{ $suscripcion->valor }}</td>
								<td>{{ $suscripcion->tipo }}</td>
								<td>{{ $suscripcion->meses }}</td>
								<td class="text-center">
									{!! Form::open(['route' => ['administrador.suscripcion.destroy', 'id' => $suscripcion->id], 'method' => 'delete']) !!}
					
										<a href="{{ route('administrador.suscripcion.edit', $suscripcion->id) }}" class="btn btn-primary btn-xs">Editar</a>
										<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Se borrará toda la información referente a esta Suscripcion Desea continuar?');">Eliminar</button>
									{!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		
			
		@endif
	</div>

	 {!! str_replace('/?', '?', $suscripciones->render()) !!}

@endsection