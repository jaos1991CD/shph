@if(Session::has('msj_error'))
	<div class="alert alert-danger">
		<i class="fa fa-check"></i> {!! Session::get('msj_error') !!}
	</div>
@endif