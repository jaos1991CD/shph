@if(Session::has('msj_success'))
	<div class="alert alert-success">
		<i class="fa fa-check"></i> {!! Session::get('msj_success') !!}
	</div>
@endif