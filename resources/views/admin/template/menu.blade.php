<div class="collapse navbar-collapse navbar-ex1-collapse">
	<ul class="nav navbar-nav side-nav">

		
		{{-- Menu de super admin --}}
		@if (\Auth::user()->tipo == 'admin')
			<li {!! (Request::is('administrador/')) ? 'class="active"' : '' !!}>
				<a href="{{ route('dashboard') }}"><i class="fa fa-fw fa-home"></i> Inicio</a>
			</li>

			<li>
				<a href="{{ route('administrador.aliados.index') }}"><i class="fa fa-fw fa-users"></i> Aliados</a>
			</li>

			<li>
				<a href="{{ route('administrador.categorias.index') }}"><i class="fa fa-fw fa-users"></i> Categorías</a>
			</li>

			<li>
				<a href="{{ route('administrador.eventos.index') }}"><i class="fa fa-fw fa-calendar"></i> Eventos</a>
			</li>
			
			<li>
				<a href="{{ route('administrador.suscripcion.index') }}"><i class="fa fa-fw fa-users"></i> Suscripcion</a>
			</li>
			<li>
				<a href="{{ route('administrador.codigosuscripcion.index') }}"><i class="fa fa-fw fa-ticket"></i> Codigos Descuento</a>
			</li>

			<li>
				<a href="{{ route('administrador.comentarios.index') }}"><i class="fa fa-commenting-o"></i> Comentarios</a>
			</li>

			{{-- <li>
				<a href="#"><i class="fa fa-database"></i> Combos</a>
			</li> --}}

			<li>
				<a href="{{ route('administrador.usuarios.index') }}"><i class="fa fa-fw fa-users"></i> Usuarios</a>
			</li>

			<li>
				<a href="#" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-file-text-o"></i> Contenido <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="demo" class="collapse">
					<li><a href="{{ route('admin_contenido_video') }}">Video</a></li>
					<li><a href="{{ route('admin_contenido_nosotros') }}">Nosotros</a></li>
					<li><a href="{{ route('admin_contenido_terminos') }}">Términos y Condiciones</a></li>
					<li><a href="{{ route('admin_contenido_slide') }}">Slides</a></li>
				</ul>
			</li>
		@endif
		
	</ul>
</div>