<ul class="nav navbar-right top-nav">
	@if (Auth::check())
		
		<li class="dropdown hidden-xs">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->perfil->nombre_completo }} <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li>
					<a href=""><i class="fa fa-fw fa-user"></i> Perfil</a>
					<a href="{{ route('logout') }}"><i class="fa fa-fw fa-power-off"></i> Cerrar Sesión</a>
				</li>
			</ul>
		</li>
	@else
		<li>
			<a href="#">Invitado</a>
		</li>
	@endif
</ul>