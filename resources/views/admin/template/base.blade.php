<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('titulo', 'Inicio') | {{ env('NAME', 'Cms') }}</title>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="icon" href="{{ url('assets/favicon.ico') }}" >
	

	
	{!! Html::style('front/css/vendor/bootstrap-select.css') !!}
	{!! Html::style('admin/css/bootstrap-admin.css') !!}
	{!! Html::style('admin/css/vendor/bootstrap-datetimepicker.css') !!}
	{!! Html::style('admin/css/vendor/snackbar.css') !!}
   

	@yield('css', '')

	{!! Html::style('admin/css/main.css') !!}
</head>
<body>
	<div id="wrapper">
		{{-- Navegacion --}}
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">{{ env('NAME', 'Cms') }}</a>
			</div>

			{{-- Menu Top --}}
			@include('admin.template.header')

			{{-- Menu lateral --}}
			@include('admin.template.menu')
		</nav>

		<div id="page-wrapper">

			<div class="container-fluid">

				<div class="Header row">
					<div class="col-lg-12">
						<h1 class="page-header Header-titulo">
							@yield('titulo', 'Inicio')
						</h1>

						@yield('breadcrumbs', Breadcrumbs::render('inicio'))
						
					</div>
				</div>

				<div class="row Contenido">
					<div class="col-md-12">

						@yield('contenido', 'No hay contenido para mostrar')

					</div>
				</div>

			</div>

		</div>

		<footer class="Footer">
			<p>Crear Digital 2015</p>
		</footer>

		@yield('templates', '')

	</div>

	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
	{!! Html::script('front/js/vendor/bootstrap-select.min.js') !!}
	{!! Html::script('front/js/vendor/validator.min.js') !!}
	{!! Html::script('admin/js/vendor/bootstrap-filestyle.min.js') !!}
	{!! Html::script('admin/js/vendor/snackbar.min.js') !!}
	{!! Html::script('admin/js/vendor/handlebars-v4.0.2.js') !!}

	{!! Html::script('admin/js/vendor/moment.js') !!}
	{!! Html::script('admin/js/vendor/es.js') !!}
	{!! Html::script('admin/js/vendor/bootstrap-datetimepicker.min.js') !!}

	@yield('js', '')

	{!! Html::script('admin/js/main.js') !!}
</body>
</html>