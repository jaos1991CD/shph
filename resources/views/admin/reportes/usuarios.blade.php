<table>
	<tr>
		<td><h3>Listado de Usuarios </h3></td>
	</tr>
	<tr>
		<td>Nombre</td>
		<td>Correo</td>
		<td>Tipo</td>
		<td>Fecha de nacimiento</td>
		<td>ID</td>
		<td>Genero</td>
		<td>Ciudad</td>
		<td>Acepta</td>
		<td>Cantidad de Beneficios Solicitados </td>
		
		
	</tr>
	@foreach($usuarios as $usuario)
	<tr>	
		<td>{{$usuario->perfil->nombre_completo}}</td>
		<td>{{$usuario->correo}}</td>
		<td>{{$usuario->tipo}}</td>
		<td>{{$usuario->perfil->fecha_nacimiento}}</td>
		<td>{{$usuario->id}}</td>
		<td>{{$usuario->perfil->genero}}</td>
		<td>{{$usuario->perfil->ciudad->nombre}}</td>
		<td>{{ ($usuario->acepto == '1') ? 'SI' : 'NO'}}</td>

		<td>{{ $usuario->beneficios->where('estado','solicitado')->count() }}</td>		
	</tr>
	@endforeach


	
</table>
