@extends('admin.template.base')

@section('titulo')
Aliados
@endsection

@section('breadcrumbs')
{!! Breadcrumbs::render('categorias') !!}
@endsection

@section('contenido')

	<div class="panel panel-default">
		<div class="panel-body">
			<div class="Contenido-botonera">
				<a href="{{ route('administrador.categorias.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo</a>
			</div>

			@include('admin.template.partials.success')
		</div>
		@if (count($categorias) > 0)
			<div class="table-responsive">
				<table class="table table-condensed table-hover table-bordered table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>Color</th>
							<th class="text-center">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categorias as $categoria)
							<tr>
								<td>{{ $categoria->id }}</td>
								<td>{{ $categoria->nombre }}</td>
								<td>
									<span class="label bg-{{ $categoria->color }}">{{ $categoria->color }}</span>
								</td>
								<td class="text-center">
									<a href="{{ route('administrador.categorias.edit', ['categoria' => $categoria->id]) }}" class="btn btn-primary btn-xs">Editar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="alert alert-info">
				<p>Lo sentimos, no hay registro de Notificaciones para mostrar.</p>
			</div>
		@endif
	</div>
	{!! $categorias->render() !!}

@endsection