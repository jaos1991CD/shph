@extends('admin.template.base')

@section('titulo')
Crear Categoría
@endsection

@section('js')
	<script type="text/javascript">
		$(document).on('ready', function(){
			$(".CategoriaColor").on('click', function(event) {
				event.preventDefault();

				$(".CategoriaColor").removeClass('CategoriaColor-active');
				$(this).addClass('CategoriaColor-active');

				$("#color").val($(this).data('color'));
			});
		});
	</script>
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('categorias_create') !!}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
		</div>

		{!! Form::open(['route' => 'administrador.categorias.store', 'method' => 'post', 'role' => 'form', 'data-toggle' => "validator"]) !!}
			<div class="col-md-6">

				{!! Form::hidden('color', null, ['id' => 'color']) !!}

				<div class="form-group">
					{!! Form::label('nombre', trans('validation.attributes.nombre')) !!}
					{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!}
					{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'required' => true, 'rows' => 5]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="row">
					<div class="col-xs-1">
						<span class="CategoriaColor bg-rojo" data-color="rojo"></span>
					</div>
					<div class="col-xs-1">
						<span class="CategoriaColor bg-azul" data-color="azul"></span>
					</div>
					<div class="col-xs-1">
						<span class="CategoriaColor bg-amarillo" data-color="amarillo"></span>
					</div>
					<div class="col-xs-1">
						<span class="CategoriaColor bg-rosado" data-color="rosado"></span>
					</div>
					<div class="col-xs-1">
						<span class="CategoriaColor bg-verde" data-color="verde"></span>
					</div>
					<div class="col-xs-1">
						<span class="CategoriaColor bg-aguamarina" data-color="aguamarina"></span>
					</div>
					<div class="col-xs-1">
						<span class="CategoriaColor bg-azul_oscuro" data-color="azul_oscuro"></span>
					</div>
					<div class="col-xs-1">
						<span class="CategoriaColor bg-morado" data-color="morado"></span>
					</div>
					<div class="col-xs-1">
						<span class="CategoriaColor bg-naranja" data-color="naranja"></span>
					</div>
				</div>

				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Guardar</button>
			</div>
		{!! Form::close() !!}

	</div>
@endsection