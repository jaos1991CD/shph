@extends('admin.template.base')

@section('titulo')
Usuarios
@endsection

@section('breadcrumbs')
{!! Breadcrumbs::render('usuarios') !!}
@endsection

@section('contenido')

	<div class="panel panel-default">
		<div class="panel-body">
			<div class="Contenido-botonera">
				<!-- <a href="{{ route('administrador.usuarios.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo</a> -->
				{{--<a href="{{ route('excel_usuario') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Descargar Reporte</a>--}}
			</div>

			<div class="Contenido-botonera">
				{!! Form::model(Request::only('fecha_ini', 'fecha_fin','correo'), ['route' => 'administrador.usuarios.index', 'method' => 'get', 'class' => 'form-inline pull-right']) !!}
					
					{!! Form::text('fecha_ini', null, ['class' => 'form-control datepicker', 'placeholder' => 'Fecha Registro Inicio']) !!}
					{!! Form::text('fecha_fin', null, ['class' => 'form-control datepicker', 'placeholder' => 'Fecha Registro Fin']) !!}
					{!! Form::text('correo', null, ['class' => 'form-control', 'placeholder' => 'Correo']) !!}
					<button class="btn btn-primary" type="submit" name="buscar" value="1"><i class="fa fa-search"></i> Buscar</button>
					<button class="btn btn-primary" type="submit" name="exportar" value="2"><i class="fa fa-download"></i> Exportar</button>
				{!! Form::close() !!}
				
			</div>

			@include('admin.template.partials.success')
		</div>
		@if (count($usuarios) > 0)
			<div class="table-responsive">
				<table class="table table-condensed table-hover table-bordered table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>Correo</th>
							<th>Estado</th>
							<th class="text-center">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($usuarios as $usuario)
							<tr>
								<td>{{ $usuario->id }}</td>
								<td>{{ $usuario->perfil->nombre_completo }}</td>
								<td>{{ $usuario->correo }}</td>
								<td>{{ $usuario->estado }}</td>
								<td class="text-center">
									{!! Form::open(['route' => ['administrador.usuarios.destroy', 'id' => $usuario->id], 'method' => 'delete']) !!}
										<a href="{{ route('administrador.usuarios.edit', ['usuario' => $usuario->id]) }}" class="btn btn-primary btn-xs">Editar</a>
										<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Se borrará toda la información referente a este usuario\n- Sesiones\n- Beneficios\nDesea continuar?');">Eliminar</button>
									{!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="alert alert-info">
				<p>Lo sentimos, no hay registro de Notificaciones para mostrar.</p>
			</div>
		@endif
	</div>
<!-- 	{!! str_replace('/?', '?', $usuarios->render()) !!}	
	 -->{!! $usuarios->render() !!}

@endsection