@extends('admin.template.base')

@section('titulo')
Nuevo Usuario
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('usuarios_create') !!}
@endsection

@section('contenido')
	<div class="row">

		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
		</div>

		{!! Form::model($usuario, ['route' => ['administrador.usuarios.update', 'id' => $usuario->id], 'method' => 'patch', 'role' => 'form', 'data-toggle' => "validator"]) !!}
			<div class="col-md-6">

				<div class="form-group">
					{!! Form::label('correo', trans('validation.attributes.correo')) !!}
					{!! Form::text('correo', null, ['class' => 'form-control', 'disabled' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('nombres', trans('validation.attributes.nombres')) !!}
							{!! Form::text('nombres', $usuario->perfil->nombres, ['class' => 'form-control', 'disabled' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('apellidos', trans('validation.attributes.apellidos')) !!}
							{!! Form::text('apellidos', $usuario->perfil->apellidos, ['class' => 'form-control', 'disabled' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('genero', trans('validation.attributes.genero')) !!}
							{!! Form::text('genero', config('lists.generos.'.$usuario->perfil->genero), ['class' => 'form-control', 'disabled' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							
							{!! Form::label('nombre', trans('validation.attributes.fecha_nacimiento')) !!}
							{!! Form::text('fecha_nacimiento', $usuario->perfil->fecha_nacimiento, ['class' => 'form-control', 'disabled' => true]) !!}
							<div class="help-block with-errors"></div>
						</div>
					</div>
				</div>

				<div class="form-group">
					{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!}
					{!! Form::text('id_ciudad', $usuario->perfil->ciudad->nombre, ['class' => 'form-control', 'disabled' => true]) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('estado', trans('validation.attributes.estado')) !!}
					{!! Form::select('estado', config('lists.estados'), null, ['class' => 'form-control']) !!}
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group">
					{!! Form::label('tipo', trans('validation.attributes.tipo')) !!}
					{!! Form::select('tipo', config('lists.tipos_usuarios'), $usuario->tipo, ['class' => 'form-control']) !!}
					<div class="help-block with-errors"></div>
				</div>

			</div>

			<div class="col-md-6">

				<fieldset>
					<legend>Cambiar Contraseña</legend>

					<div class="form-group">
						{!! Form::label('password', trans('validation.attributes.password')) !!}
						{!! Form::password('password', ['class' => 'form-control']) !!}
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group">
						{!! Form::label('password_confirmation', trans('validation.attributes.password_confirmation')) !!}
						{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
						<div class="help-block with-errors"></div>
					</div>
				</fieldset>
			</div>

			<div class="col-md-12">
				<button class="btn btn-primary pull-right" type="submit"><i class="fa fa-save"></i> Guardar</button>
			</div>
		{!! Form::close() !!}

	</div>
@endsection