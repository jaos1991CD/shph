@extends('admin.template.base')

@section('titulo')
Crear Evento
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('eventos_create') !!}
@endsection

@section('css')
	{!! Html::style('front/css/vendor/dropzone.css') !!}
	{!! Html::style('front/css/vendor/cropper.css') !!}
@endsection

@section('js')
	{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSynIVRyKUopyFqtQIiJW34VOBrt4oa48&callback=initMap" async defer></script> --}}
	{!! Html::script('front/js/vendor/dropzone.js') !!}
	{!! Html::script('front/js/vendor/cropper.min.js') !!}
	{!! Html::script('admin/js/crear_evento.js') !!}
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
		</div>

		<div class="eventoInfo">

			<div class="col-md-12">
				<fieldset>
					<legend>Información del Evento</legend>

						<div class="row">
							<div class="col-md-6">
								{!! Form::open(['route' => 'administrador.eventos.store', 'method' => 'post', 'role' => 'form', 'data-toggle' => "validator", 'files' => true]) !!}
									{!! Form::hidden('id_imagen', null, ['id' => 'id_imagen', 'required' => 'required']) !!}

									<div class="form-group">
										{!! Form::label('nombre', trans('validation.attributes.nombre')) !!}
										{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div>

									{{-- <div class="form-group">
										{!! Form::label('tipo', trans('validation.attributes.tipo')) !!}
										{!! Form::select('tipo', config('lists.tipos_evento'), null, ['class' => 'form-control', 'id' => 'tipo', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div> --}}

									{{-- <div class="form-group codigoGeneral sr-only">
										{!! Form::label('codigo', trans('validation.attributes.codigo')) !!}
										{!! Form::text('codigo', null, ['class' => 'form-control']) !!}
										<div class="help-block with-errors"></div>
									</div> --}}

									<div class="form-group enlace sr-only">
										{!! Form::label('enlace', trans('validation.attributes.enlace')) !!}
										{!! Form::text('enlace', null, ['class' => 'form-control']) !!}
										<div class="help-block with-errors"></div>
									</div>

									<div class="form-group">
										{!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!}
										{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '4','class' => 'ckeditor', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div>

									<div class="form-group">
										{!! Form::label('terminos', trans('validation.attributes.terminos')) !!}
										{!! Form::textarea('terminos', null, ['class' => 'form-control', 'rows' => '5', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												{!! Form::label('direccion', trans('validation.attributes.direccion')) !!}
												{!! Form::text('direccion', null, ['class' => 'form-control', 'required' => true]) !!}
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												{!! Form::label('lugar', trans('validation.attributes.lugar')) !!}
												{!! Form::text('lugar', null, ['class' => 'form-control']) !!}
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</div>

									<div class="form-group">
										{!! Form::label('web', trans('validation.attributes.web')) !!}
										{!! Form::text('web', null, ['class' => 'form-control']) !!}
										<div class="help-block with-errors"></div>
									</div>

									<div class="form-group">
										{!! Form::label('precio', trans('validation.attributes.precio')) !!}
										{!! Form::textarea('precio', null, ['class' => 'form-control', 'class' => 'ckeditor' , 'rows' => '3', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div>
									
									<div class="form-group">
										{!! Form::label('descuento', trans('validation.attributes.descuento')) !!}
										{!! Form::textarea('descuento', null, ['class' => 'form-control', 'rows' => '3', 'class' => 'ckeditor' ]) !!}
										<div class="help-block with-errors"></div>
									</div> 

									<div class="form-group">
										{!! Form::label('fechadesc', trans('validation.attributes.fechadesc')) !!}
										{!! Form::text('fechadesc', null, ['class' => 'form-control', 'rows' => '3', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												{!! Form::label('fecha_inicio', trans('validation.attributes.fecha_inicio')) !!}
												{!! Form::text('fecha_inicio', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
												<div class="help-block with-errors"></div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												{!! Form::label('fecha_fin', trans('validation.attributes.fecha_fin')) !!}
												{!! Form::text('fecha_fin', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												{!! Form::label('id_aliado', trans('validation.attributes.id_aliado')) !!}
												{!! Form::select('id_aliado', $aliados, null, ['class' => 'form-control', 'required' => true]) !!}
												<div class="help-block with-errors"></div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												{!! Form::label('id_categoria', trans('validation.attributes.id_categoria')) !!}
												{!! Form::select('id_categoria', $categorias, null, ['class' => 'form-control', 'required' => true]) !!}
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!}
												{!! Form::select('id_ciudad', $ciudades, null, ['class' => 'form-control selectpicker', 'data-selected-text-format' => "count>3", 'data-live-search' => "true"]) !!}
												<div class="help-block with-errors"></div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												{!! Form::label('id_estado', trans('validation.attributes.id_estado')) !!}
												{!! Form::select('id_estado', $estados, null, ['class' => 'form-control']) !!}
												<div class="help-block with-errors"></div>
											</div>
										</div>
									</div>

									<fieldset id="ctn-horario">
										<legend>Horarios</legend>

										<div class="row">
											<div class="col-md-5">
												<div class="form-group">
													{!! Form::text('fecha[]', null, ['class' => 'form-control datepicker', 'placeholder' => trans('validation.attributes.fecha')]) !!}
													<div class="help-block with-errors"></div>
												</div>
											</div>

											<div class="col-md-5">
												<div class="form-group">
													{!! Form::text('hora[]', null, ['class' => 'form-control datepicker-time', 'placeholder' => trans('validation.attributes.hora')]) !!}
													<div class="help-block with-errors"></div>
												</div>
											</div>

											<div class="col-md-2">
												<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
											</div>
										</div>
									</fieldset>

									<button class="btn btn-primary pull-right btn-lg" type="submit"><i class="fa fa-save"></i> Crear Evento</button>
									{{-- <div class="form-group">
										{!! Form::label('latitud', trans('validation.attributes.localizacion')) !!}
										<div id="mapa" style="height: 300px; width:100%;"></div>
										{!! Form::hidden('latitud', null, ['id' => 'latitud']) !!}
										{!! Form::hidden('longitud', null, ['id' => 'longitud']) !!}
									</div> --}}
								{!! Form::close() !!}
							</div>


							<div class="col-md-6">

								<div class="eventoImagen">
									<div class="col-md-12" style="margin-bottom: 32px;">
										{!! Form::label('id_imagen', trans('validation.attributes.imagen')) !!}

										{!! Form::open(['route' => 'front_evento_imagen', 'method' => 'post', 'id' => 'frm-imagenes', 'class' => 'dropzone','required' => true]) !!}
										<div class="fallback">
											<input name="file" type="file" />
										</div>
										{!! Form::close() !!}

										<div class="homePublica-imagen"></div>


										<div class="row">
											<div class="col-md-12">
												<button class="btn btn-primary btn-md pull-right sr-only" id="btn-imagen"><i class="fa fa-crop"></i> Cortar</button>
											</div>
										</div>

									</div>
								</div>
						</div>
				</fieldset>
			</div>
		</div>
	</div>

	<script id="tmp-imagen" type="text/x-handlebars-template">
		<img class="img-responsive" style="width:100%;" id="image" src="@{{ url }}" data-id="@{{ id }}">
	</script>

	<script id="tmp-horario" type="text/x-handlebars-template">
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="fecha[]" class="form-control datepicker" placeholder="Fecha">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="hora[]" class="form-control datepicker-time" placeholder="Hora">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-2">
				<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
			</div>
		</div>
	</script>

@endsection