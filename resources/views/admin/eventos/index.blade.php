@extends('admin.template.base')

@section('titulo')
Eventos
@endsection

@section('js')
	{!! Html::script('front/js/vendor/jquery.blockUI.js') !!}

	<script type="text/javascript">

		$(document).on('ready', function() {
			$(".estado").on('change', function(event) {
				event.preventDefault();
				$.blockUI();
				
				var token = $("#token").val();
				var url = $(this).data('url');

				datos = {
					"_token": token,
					"id_estado": $(this).val()
				}

				cambiarEstado(datos, url, function (data) {
					$.unblockUI();
					
					if(data.status = 'success'){
						alert('Se ha cambiado el estado de este evento.');
					}else{
						alert(data.message);
					}
				})
			});
		});

		function cambiarEstado (datos, url, callback) {
			$.post(url, datos, function(data, textStatus, xhr) {
				callback(data);
			});
		}
	</script>
@endsection

@section('breadcrumbs')
{!! Breadcrumbs::render('eventos') !!}
@endsection

@section('contenido')

	<div class="panel panel-default">

		<div class="panel-body">
			<div class="Contenido-botonera">
				<a href="{{ route('administrador.eventos.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Nuevo</a>
			</div>

			<div class="Contenido-botonera">
				{!! Form::model(Request::only('estado','nombre'), ['route' => 'administrador.eventos.index', 'method' => 'get', 'class' => 'form-inline pull-right']) !!}
					
					{!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Nombre del evento']) !!}					
					{!! Form::select('estado', $estados, null, ['class' => 'form-control', 'data-live-search' => "true"]) !!}
					<button class="btn btn-primary" type="submit" name="Buscar"><i class="fa fa-search"></i> Buscar</button>
					<button class="btn btn-primary" type="submit" name="exportar" value="2"><i class="fa fa-download"></i> Exportar</button>
				{!! Form::close() !!}
			</div>
		

			@include('admin.template.partials.success')
		</div>

		@if (count($eventos) > 0)
			<div class="table-responsive">
				<table class="table table-condensed table-hover table-bordered table-striped">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Aliado</th>
							<th>Categoría</th>
							<th>Beneficios </th>
							<th>Estado</th>
							<th class="text-center">Acciones</th>
						</tr>
					</thead>
					<tbody>
						{!! Form::hidden('token', csrf_token(), ['id' => 'token']) !!}
						@foreach($eventos as $evento)
							<tr>
								<td>{{ $evento->nombre }}</td>
								<td>{{ $evento->aliado->nombre }}</td>
								<td>{{ $evento->categoria->nombre }}</td>
								<td>{{count( $evento->beneficios)}}</td>
								<td>
									{!! Form::select('estado', $estados, $evento->estado->id, ['class' => 'form-control estado', 'id' => 'estado', 'data-url' => route('admin_cambiar_estado', ['id' => $evento->id])]) !!}
								</td>
								<td class="text-center">
									{!! Form::open(['route' => ['administrador.eventos.destroy', 'id' => $evento->id], 'method' => 'delete']) !!}
										<a href="{{ route('administrador.eventos.edit', ['evento' => $evento->id]) }}" class="btn btn-primary btn-xs">Editar</a>
										<button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('Se borrará toda la información referente a este Evento\nSeguro que desea eliminar este registro?');">Eliminar</button>
									{!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="alert alert-info">
				<p>Lo sentimos, no hay registro de Notificaciones para mostrar.</p>
			</div>
		@endif
	</div>
	 
   {!! str_replace('/?', '?', $eventos->appends(Input::except('page'))->render()) !!}
@endsection