@extends('admin.template.base')

@section('titulo')
Editar Evento
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('eventos_edit') !!}
@endsection

@section('css')
	{!! Html::style('front/css/vendor/dropzone.css') !!}
	{!! Html::style('front/css/vendor/cropper.css') !!}
@endsection

@section('js')
	{!! Html::script('front/js/vendor/dropzone.js') !!}
	{!! Html::script('front/js/vendor/cropper.min.js') !!}
	{!! Html::script('admin/js/editar_evento.js') !!}

	<script type="text/javascript">
		@if(is_null($evento->latitud) && is_null($evento->longitud))
			function initMap() {
				var map = new google.maps.Map(document.getElementById('mapa'), {
					zoom: 15,
					center: {lat: -34.397, lng: 150.644},
					scrollwheel: false
				});
				var geocoder = new google.maps.Geocoder();
				var address = '{{ $evento->direccion }}, {{ $evento->ciudad->nombre }}, {{ $evento->ciudad->departamento->nombre }}';
				var markers = [];

				geocoder.geocode({'address': address}, function(results, status) {
					if (status === google.maps.GeocoderStatus.OK) {
						map.setCenter(results[0].geometry.location);
						var marker = new google.maps.Marker({
							map: map,
							position: results[0].geometry.location
						});
						markers.push(marker);

						//$("#latitud").val(marker.getPosition().lat());
						//$("#longitud").val(marker.getPosition().lng());

					} else {
						console.log('No se puede mostrar ubicación del evento');
					}
				});

				map.addListener('dragend', function() {
					for (var i = 0; i < markers.length; i++) {
						markers[i].setMap(null);
					}
					var marker = new google.maps.Marker({
						map: map,
						position: map.getCenter()
					});
					markers.push(marker);

					$("#latitud").val(marker.getPosition().lat());
					$("#longitud").val(marker.getPosition().lng());
				});

			}
		@else
			function initMap() {
				var map = new google.maps.Map(document.getElementById('mapa'), {
					zoom: 15,
					center: {
						lat: {{ $evento->latitud }},
						lng: {{ $evento->longitud }} 
					},
					scrollwheel: false
				});

				var markers = [];
				
				var marker = new google.maps.Marker({
					map: map,
					position: map.getCenter()
				});
				markers.push(marker);


				map.addListener('dragend', function() {
					for (var i = 0; i < markers.length; i++) {
						markers[i].setMap(null);
					}
					var marker = new google.maps.Marker({
						map: map,
						position: map.getCenter()
					});
					markers.push(marker);

					$("#latitud").val(marker.getPosition().lat());
					$("#longitud").val(marker.getPosition().lng());
				});

			}
		@endif
	</script>


	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSynIVRyKUopyFqtQIiJW34VOBrt4oa48&callback=initMap" async defer></script>
@endsection

@section('contenido')
	<div class="row">
		<div class="col-md-12">
			@include('admin.template.partials.errors')
			@include('admin.template.partials.error')
		</div>

		{!! Form::hidden('imagen', url('front/img/eventos/'.$evento->imagen->nombre), ['id' => 'imagen']) !!}

		<div class="eventoInfo">

			<div class="col-md-12">
				<fieldset>
					<legend>Información del Evento</legend>

					<div class="row">
							
						<div class="col-md-6">
							{!! Form::model($evento, ['route' => ['administrador.eventos.update', 'id' => $evento->id], 'method' => 'patch', 'role' => 'form', 'data-toggle' => "validator", 'files' => true]) !!}
								
								{!! Form::hidden('id_imagen', null, ['id' => 'id_imagen']) !!}
								<div class="form-group">
									{!! Form::label('nombre', trans('validation.attributes.nombre')) !!}
									{!! Form::text('nombre', null, ['class' => 'form-control', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>

								{{-- <div class="form-group">
									{!! Form::label('tipo', trans('validation.attributes.tipo')) !!}
									{!! Form::select('tipo', config('lists.tipos_evento'), null, ['class' => 'form-control', 'id' => 'tipo', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>

								<div class="form-group codigoGeneral sr-only">
									{!! Form::label('codigo', trans('validation.attributes.codigo')) !!}
									{!! Form::text('codigo', null, ['class' => 'form-control']) !!}
									<div class="help-block with-errors"></div>
								</div>

								<div class="form-group enlace sr-only">
									{!! Form::label('enlace', trans('validation.attributes.enlace')) !!}
									{!! Form::text('enlace', null, ['class' => 'form-control']) !!}
									<div class="help-block with-errors"></div>
								</div> --}}

								<div class="form-group">
									{!! Form::label('descripcion', trans('validation.attributes.descripcion')) !!}
									{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '4', 'class' => 'ckeditor', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>

								<div class="form-group">
									{!! Form::label('terminos', trans('validation.attributes.terminos')) !!}
									{!! Form::textarea('terminos', null, ['class' => 'form-control', 'rows' => '5', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('direccion', trans('validation.attributes.direccion')) !!}
											{!! Form::text('direccion', null, ['class' => 'form-control', 'required' => true]) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('lugar', trans('validation.attributes.lugar')) !!}
											{!! Form::text('lugar', null, ['class' => 'form-control']) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>

								<div class="form-group">
									{!! Form::label('latitud', trans('validation.attributes.localizacion')) !!}
									<div id="mapa" style="height: 300px; width:100%;"></div>
									{!! Form::hidden('latitud', null, ['id' => 'latitud']) !!}
									{!! Form::hidden('longitud', null, ['id' => 'longitud']) !!}
								</div>

								<div class="form-group">
									{!! Form::label('web', trans('validation.attributes.web')) !!}
									{!! Form::text('web', null, ['class' => 'form-control']) !!}
									<div class="help-block with-errors"></div>
								</div>

								<div class="form-group">
									{!! Form::label('precio', trans('validation.attributes.precio')) !!}
									{!! Form::textarea('precio', null, ['class' => 'form-control','class' => 'ckeditor', 'rows' => '3', 'required' => true]) !!}
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group">
										{!! Form::label('descuento', trans('validation.attributes.descuento')) !!}
										{!! Form::textarea('descuento', null, ['class' => 'form-control', 'class' => 'ckeditor', 'rows' => '3', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div>
									<div class="form-group">
										{!! Form::label('fechadesc', trans('validation.attributes.fechadesc')) !!}
										{!! Form::text('fechadesc', null, ['class' => 'form-control', 'rows' => '3', 'required' => true]) !!}
										<div class="help-block with-errors"></div>
									</div>

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('fecha_inicio', trans('validation.attributes.fecha_inicio')) !!}
											{!! Form::text('fecha_inicio', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('fecha_fin', trans('validation.attributes.fecha_fin')) !!}
											{!! Form::text('fecha_fin', null, ['class' => 'form-control datepicker', 'required' => true]) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('id_aliado', trans('validation.attributes.id_aliado')) !!}
											{!! Form::select('id_aliado', $aliados, null, ['class' => 'form-control', 'required' => true]) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('id_categoria', trans('validation.attributes.id_categoria')) !!}
											{!! Form::select('id_categoria', $categorias, null, ['class' => 'form-control', 'required' => true]) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('id_ciudad', trans('validation.attributes.id_ciudad')) !!}
											{!! Form::select('id_ciudad', $ciudades, null, ['class' => 'form-control selectpicker', 'data-selected-text-format' => "count>3", 'data-live-search' => "true"]) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											{!! Form::label('id_estado', trans('validation.attributes.id_estado')) !!}
											{!! Form::select('id_estado', $estados, null, ['class' => 'form-control']) !!}
											<div class="help-block with-errors"></div>
										</div>
									</div>
								</div>

								<fieldset id="ctn-horario">
									<legend>Horarios</legend>

									@foreach ($evento->horarios as $i => $horario)
										<div class="row">
											<div class="col-md-5">
												<div class="form-group">
													{!! Form::text('fecha[]', $horario->fecha, ['class' => 'form-control datepicker', 'placeholder' => trans('validation.attributes.fecha')]) !!}
													<div class="help-block with-errors"></div>
												</div>
											</div>

											<div class="col-md-5">
												<div class="form-group">
													{!! Form::text('hora[]', $horario->hora, ['class' => 'form-control datepicker-time', 'placeholder' => trans('validation.attributes.hora')]) !!}
													<div class="help-block with-errors"></div>
												</div>
											</div>

											<div class="col-md-2">
												@if (count($evento->horarios) == $i+1)
													<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
												@endif
											</div>
										</div>
									@endforeach
								</fieldset>

								<button class="btn btn-primary btn-lg pull-right" type="submit"><i class="fa fa-save"></i> Editar Evento</button>
								
							{!! Form::close() !!}
						</div>

						<div class="col-md-6">

							<div class="eventoImagen">
								<div class="col-md-12"style="margin-bottom: 32px;">

									{!! Form::open(['route' => 'front_evento_imagen', 'method' => 'post', 'id' => 'frm-imagenes', 'class' => 'dropzone']) !!}
										<div class="fallback">
											<input name="file" type="file" />
										</div>
									{!! Form::close() !!}

									<div class="homePublica-imagen" style="with:100%;"></div>

									<div class="row">
										<div class="col-md-12">
											<button class="btn btn-primary btn-md pull-right sr-only" id="btn-imagen">Guardar</button>
										</div>
									</div>

								</div>
							</div>

							{{-- <div class="form-group codigoUnico sr-only">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="true" name="generar_codigos" id="generar_codigos">
										Generar Códigos
									</label>

									<label>
										<input type="checkbox" value="true" name="cargar_codigos" id="cargar_codigos">
										Cargar Códigos
									</label>
								</div>
							</div>

							<div class="row generarCodigos sr-only">
								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('codigos_numero', 'Número de Códigos') !!}
										{!! Form::text('codigos_numero', null, ['class' => 'form-control']) !!}
										<div class="help-block with-errors"></div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										{!! Form::label('codigos_cantidad', 'Cantidad de Digitos') !!}
										{!! Form::number('codigos_cantidad', 5, ['class' => 'form-control', 'min' => 5]) !!}
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>

							<div class="cargarCodigos sr-only">
								<div class="form-group">
									{!! Form::label('codigos', trans('validation.attributes.codigos')) !!}
									{!! Form::file('codigos', ['class' => '.file']) !!}
									<p class="help-block">Puede descargar la plantilla <a href="#">Aquí</a></p>
									<div class="help-block with-errors"></div>
								</div>
							</div> --}}
						</div>
						
					</div>

				</fieldset>
			</div>
		</div>
	</div>

	<script id="tmp-imagen" type="text/x-handlebars-template">
		<img class="img-responsive" style="width:100%;" id="image" src="@{{ url }}" data-id="@{{ id }}">
	</script>

	<script id="tmp-horario" type="text/x-handlebars-template">
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="fecha[]" class="form-control datepicker" placeholder="Fecha">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-5">
				<div class="form-group">
					<input type="text" name="hora[]" class="form-control datepicker-time" placeholder="Hora">
					<div class="help-block with-errors"></div>
				</div>
			</div>

			<div class="col-md-2">
				<button type="button" class="btn btn-success btn-block btn-add-horario"><i class="fa fa-plus"></i></button>
			</div>
		</div>
	</script>
@endsection