@extends('email.template.base')

@section('contenido')
	<p style="text-align: center">
		Se ha publicado un nuevo evento y esta a la espera de su activación
	</p>

	<table>
		<tr>
			<td>Aliado</td>
			<td>{{ $evento->aliado->nombre }}</td>
		</tr>

		<tr>
			<td>Nombre del Evento</td>
			<td>{{ $evento->nombre }}</td>
		</tr>

		<tr>
			<td>Para activar el evento por favor siga este enlace</td>
			<td>{{ route('administrador.eventos.edit', ['evento' => $evento->id]) }}</td>
		</tr>
	</table>
@endsection