@extends('email.template.base')

@section('contenido')
	<p style="text-align: center">
		Hola {{ $nombre }}, Bienvenido al Club Si Hay Para Hacer, una plataforma de confianza diseñada para que sus usuarios
		disfruten los mejores eventos y encuentros de su ciudad.
	</p>

	<p style="text-align: center">
		Por favor confirma tu correo electrónico en el siguiente enlace: <a href="{{ $url }}">Confirmar</a>
	</p>
@endsection