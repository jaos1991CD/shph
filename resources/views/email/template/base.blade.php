<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin-top: 0px !important; padding-top: 0px !important">
	<head>
		<style type="text/css">
			html, body{ margin-top: 0px !important; padding-top: 0px !important; }
			body{ background-color:#FFFFFF; margin-top: 0px !important; padding-top: 0px !important; font-family:sans-serif; }
			table{ margin-top: 0px !important; padding-top: 0px !important; }
		</style>
		<style type="text/css">
			a img{ color:#000001 !important; }
			.wysiwyg-text-align-right{ text-align: right; }
			.wysiwyg-text-align-center { text-align: center; }
			.wysiwyg-text-align-left{ text-align: left; }
			.wysiwyg-text-align-justify{ text-align: justify; }
			body{ text-shadow:none; padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#000000!important; font-style:normal; font-family:Arial; font-size:14px; line-height:24px; }
			h1, #email-347855 h1{ text-shadow:none; padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#000000!important; font-weight:400; font-style:normal; font-family:Arial; font-size:36px; line-height:44px; }
			h2, #email-347855 h2{ text-shadow:none; padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#000000!important; font-weight:400; font-style:normal; font-family:Arial; font-size:24px; line-height:32px; }
			h3, #email-347855 h3{ text-shadow:none; padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#000000!important; font-weight:400; font-style:normal; font-family:Arial; font-size:15px; line-height:21px; }
			p, #email-347855 p{ text-shadow:none; padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#000000!important; font-style:normal; font-family:Arial; font-size:14px; line-height:24px; }
			a, #email-347855 a{ text-shadow:none; padding-top:0; padding-right:0; padding-bottom:0; padding-left:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0; color:#1122CC!important; text-decoration:underline; }
			.h1_color_span_wrapper{ color: #000000; }
			.h2_color_span_wrapper{ color: #000000; }
			.h3_color_span_wrapper{ color: #000000; }
			.p_color_span_wrapper{ color: #000000; }
			.a_color_span_wrapper{ color: #1122CC; }
			.mi-all{ display: block; }
			.mi-desktop{ display: block; }
			.mi-mobile{
			display: none;
			font-size: 0; 
			max-height: 0; 
			line-height: 0; 
			padding: 0;
			float: left;
			overflow: hidden;
			mso-hide: all; /* hide elements in Outlook 2007-2013 */
			}
		</style>
		<style type="text/css" >
			div, p, a, li, td { -webkit-text-size-adjust:none; }
			@media only screen and (max-device-width: 480px), screen and (max-width: 480px), screen and (orientation: landscape) and (max-width: 630px) {
			/* very important! all except 'all' and this current type get a display:none; */
			.mi-desktop{ display: none !important; }
			/* then show the mobile one */
			.mi-mobile{ 
			display: block !important;
			font-size: 12px !important;
			max-height: none !important;
			line-height: 1.5 !important;
			float: none !important;
			overflow: visible !important;
			}
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body id="email-347855" style="background: #FFFFFF; color: #000000 !important; font-family: Arial; font-size: 14px; font-style: normal; line-height: 24px; margin: 0px 0 0 0px; padding: 0px 0 0; text-shadow: none" bgcolor="#FFFFFF">
		<style type="text/css">
			body {
			margin-top: 0px !important; padding-top: 0px !important;
			}
			body {
			background-color: #FFFFFF; margin-top: 0px !important; padding-top: 0px !important; font-family: sans-serif;
			}
			body {
			text-shadow: none; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0; margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; color: #000000 !important; font-style: normal; font-family: Arial; font-size: 14px; line-height: 24px;
			}
		</style>
		<div class="mi-desktop" style="display: block">
			<table width="100%" cellspacing="0" cellpadding="0" align="center" style="background: #FFFFFF; border: 0px none; border-collapse: collapse; border-spacing: 0px; margin: 0px; padding: 0px" bgcolor="#FFFFFF">
				<tbody>
					<tr align="center" style="border: 0px none; border-collapse: collapse; border-spacing: 0px">
						<td valign="top" align="center" style="border: 0px none; border-collapse: collapse; border-spacing: 0px">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="border: 0px none; border-collapse: collapse; border-spacing: 0px; margin: 0px; padding: 0px">
								<tbody>
									<tr align="left" style="border: 0px none; border-collapse: collapse; border-spacing: 0px">
										<td width="100%">
											<table width="100%" cellspacing="0" cellpadding="0" border="0" style="border: 0px none; border-collapse: collapse; border-spacing: 0px; margin-top: 0px !important; padding-top: 0px !important">
												<tbody>
													<tr align="left" style="border: 0px none; border-collapse: collapse; border-spacing: 0px">
														<td width="100%">
															<table width="100%" cellspacing="0" cellpadding="0" border="0" style="border: 0px none; border-collapse: collapse; border-spacing: 0px; margin-top: 0px !important; padding-top: 0px !important">
																<tbody>
																	<tr style="border: 0; border-collapse: collapse; border-spacing: 0px; height: 50px">
																		<td width="100%" valign="top" height="50" align="left" style="background: #FFFFFF; font-size: 1px; line-height: 1px" bgcolor="#FFFFFF"> </td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<table align="center" cellpadding="0" cellspacing="0" width="100%" style="background: #FFFFFF; border: 0; border-collapse: collapse; border-spacing: 0; margin: 0px 0 0; padding: 0px 0 0" bgcolor="#FFFFFF">
			<tr align="center" style="border: 0; border-collapse: collapse; border-spacing: 0">
				<td align="center" valign="top" style="border: 0; border-collapse: collapse; border-spacing: 0">
					<div class="mi-all" style="display: block">
						<table width="612" class="mi-all" align="center" cellspacing="0" cellpadding="0" border="0" style="border: 0; border-collapse: collapse; border-spacing: 0; display: block; margin: 0px 0 0; min-width: 612px; padding: 0px 0 0">
							<tbody>
								<tr align="left" style="border: 0; border-collapse: collapse; border-spacing: 0">
									<td>
										<table cellspacing="0" cellpadding="0" border="0" style="border: 0; border-collapse: collapse; border-spacing: 0; margin-top: 0px !important; min-width: 612px; padding-top: 0px !important">
											<tbody>
												<tr style="border: 0; border-collapse: collapse; border-spacing: 0">
													<td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly"><img src="http://sihayparahacer.com/front/img/email/image-7d483c49d45a772ed08f221633153ee090f4f1fa.jpg" style="border: 0; display: block; height: 194px; line-height: 0px; max-height: 194px; max-width: 220px; min-height: 194px; min-width: 220px; width: 220px" /></td>
													<td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly">
														<a href="{{ url() }}">
															<img src="http://sihayparahacer.com/front/img/email/image-713837439465f8a7c7559da441540775827460b1.jpg" style="border: 0; display: block; height: 194px; line-height: 0px; max-height: 194px; max-width: 173px; min-height: 194px; min-width: 173px; width: 173px" />
														</a>
													</td>
													<td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly"><img src="http://sihayparahacer.com/front/img/email/image-ea664e4b51c807834c878dcb9421d48641846bd0.jpg" style="border: 0; display: block; height: 194px; line-height: 0px; max-height: 194px; max-width: 219px; min-height: 194px; min-width: 219px; width: 219px" /></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr align="left" style="border: 0; border-collapse: collapse; border-spacing: 0">
									<td>
										<table cellspacing="0" cellpadding="0" border="0" style="border: 0; border-collapse: collapse; border-spacing: 0; margin-top: 0px !important; min-width: 612px; padding-top: 0px !important">
											<tbody>
												<tr style="border: 0; border-collapse: collapse; border-spacing: 0">
													<td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly"><img src="http://sihayparahacer.com/front/img/email/image-002eb7439f79e5908e379e44f1a351420630112f.jpg" style="border: 0; display: block; height: 51px; line-height: 0px; max-height: 51px; max-width: 612px; min-height: 51px; min-width: 612px; width: 612px" /></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr align="left" style="border: 0; border-collapse: collapse; border-spacing: 0">
									<td>
										<table cellspacing="0" cellpadding="0" border="0" style="border: 0; border-collapse: collapse; border-spacing: 0; margin-top: 0px !important; min-width: 612px; padding-top: 0px !important">
											<tbody>
												<tr style="border: 0; border-collapse: collapse; border-spacing: 0">
													<td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly"><img src="http://sihayparahacer.com/front/img/email/image-47c383b2eef9d5ca3e46080111cfc3a71d2a071c.jpg" style="border: 0; display: block; height: 181px; line-height: 0px; max-height: 181px; max-width: 70px; min-height: 181px; min-width: 70px; width: 70px" /></td>
													<td align="left" valign="top" style=" mso-line-height-rule: exactly">

														@yield('contenido')

													</td>
													<td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly"><img src="http://sihayparahacer.com/front/img/email/image-d870c993c250862f9e0a69df553f35e50e460d75.jpg" style="border: 0; display: block; height: 181px; line-height: 0px; max-height: 181px; max-width: 70px; min-height: 181px; min-width: 70px; width: 70px" /></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr align="left" style="border: 0; border-collapse: collapse; border-spacing: 0">
									<td>
										<table cellspacing="0" cellpadding="0" border="0" style="border: 0; border-collapse: collapse; border-spacing: 0; margin-top: 0px !important; min-width: 612px; padding-top: 0px !important">
											<tbody>
												<tr style="border: 0; border-collapse: collapse; border-spacing: 0">
													<td align="left" valign="top" style="line-height: 0px; mso-line-height-rule: exactly"><img src="http://sihayparahacer.com/front/img/email/image-aff745e977bcd2670e1a874a8be40e854fd6f2ef.jpg" style="border: 0; display: block; height: 75px; line-height: 0px; max-height: 75px; max-width: 612px; min-height: 75px; min-width: 612px; width: 612px" /></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
		</table>
		<div style="display: none; font: 15px courier; white-space: nowrap">
		</div>
	</body>
</html>
