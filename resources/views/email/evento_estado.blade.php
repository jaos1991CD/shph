@extends('email.template.base')

@section('contenido')
	<p style="text-align: center">
		El siguiente evento ha sido publicado en <a href="www.sihayparahacer.com">www.sihayparahacer.com</a> con éxito
	</p>

	<table>
		<tr>
			<td>Nombre Aliado</td>
			<td>{{ $evento->aliado->nombre }}</td>
		</tr>

		<tr>
			<td>Nombre del Evento</td>
			<td>{{ $evento->nombre }}</td>
		</tr>

		<tr>
			<td>Puede ver el evento en el siguiente enlace</td>
			<td>{{ route('front_evento', ['id' => $evento->id]) }}</td>
		</tr>
	</table>
@endsection