@extends('email.template.base')

@section('contenido')
	<p style="text-align: center">
		El siguiente usuario solicitó el beneficio en su evento
	</p>
	<table>
		<tr>
			<td><strong>Evento:</strong></td>
			<td>{{ $evento->nombre }}</td>
		</tr>

		<tr>
			<td><strong>Nombre del Usuario:</strong></td>
			<td>{{ $usuario->perfil->nombre_completo }}</td>
		</tr>

		<tr>
			<td><strong>Cédula:</strong></td>
			<td>{{ $usuario->perfil->cedula }}</td>
		</tr>
	</table>
@endsection