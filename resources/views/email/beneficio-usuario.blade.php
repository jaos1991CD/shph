@extends('email.template.base')

@section('contenido')
	<p style="text-align: center">
		 
	
			FELICITACIONES! {{ $usuario->perfil->nombre_completo }} Ya puedes hacer uso de tu beneficio por ser miembro del club Si hay para hacer. 
			A continuación  encontrarás la información del evento:<br><br>

			

			<strong>Evento:</strong> {{ $evento->nombre }}<br>
			<strong>Lugar :</strong> {{ $evento->lugar }}<br>
			<strong>Dirección:</strong> {{ $evento->direccion }}<br>
			@if(!is_null($horario))
			<strong>Fecha:</strong>{{ $horario->fecha}}<br>
			<strong>Hora:</strong> {{ $horario->hora}}<br>
			@endif
			<strong>Condiciones:</strong>
			para redimir este beneficio debes presentarte en la taquilla o entrada del evento con tu <strong>cédula</strong> o <strong>documento de identificación</strong>.
			<br><br>
	</p>

	<p style="text-align: center">
		Te esperamos y no dejes de estar actualizado de lo que está ocurriendo en tu ciudad.
	</p>




@endsection