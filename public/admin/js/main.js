$(document).on('ready', function() {

	//$('.datepicker').datetimepicker();
	//$("#modales").modal('show');

	$('.datepicker').datetimepicker({
		format: 'YYYY-MM-DD'
	});

	$('.datepicker-datetime').datetimepicker({
		format: 'YYYY-MM-DD H:m:s'
	});

	$('.datepicker-time').datetimepicker({
		format: 'H:m:s'
	});

	$(":file").filestyle({buttonName: "btn-success"});
});

function notificacion (mensaje, tiempo) {
	$.snackbar({
		content: mensaje,
		style: "toast", 
		timeout: tiempo
	});
}