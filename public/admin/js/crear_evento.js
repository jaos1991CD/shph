$(document).on('ready', function() {

	// mostrar los campos para generar codigos
	if ($("#generar_codigos").is(':checked')) {
		$(".generarCodigos").removeClass('sr-only');
	}else{
		$(".generarCodigos").addClass('sr-only');
	}

	$("#generar_codigos").on('change', function(event) {
		event.preventDefault();
		if ($(this).is(':checked')) {
			$(".generarCodigos").removeClass('sr-only');
		}else{
			$(".generarCodigos").addClass('sr-only');
		}
	});

	// mostrar el campo para cargar codigos
	if ($("#cargar_codigos").is(':checked')) {
		$(".cargarCodigos").removeClass('sr-only');
	}else{
		$(".cargarCodigos").addClass('sr-only');
	}

	$("#cargar_codigos").on('change', function(event) {
		event.preventDefault();
		if ($(this).is(':checked')) {
			$(".cargarCodigos").removeClass('sr-only');
		}else{
			$(".cargarCodigos").addClass('sr-only');
		}
	});

	// tipos de evento

	if ($("#tipo").val() == 'general') {
		$(".codigoGeneral").removeClass('sr-only');
		$(".codigoUnico").addClass('sr-only');
		$(".enlace").addClass('sr-only');
	}

	if ($("#tipo").val() == 'unico') {
		$(".codigoGeneral").addClass('sr-only');
		$(".codigoUnico").removeClass('sr-only');
		$(".enlace").addClass('sr-only');
	}

	if ($("#tipo").val() == 'enlace') {
		$(".codigoGeneral").addClass('sr-only');
		$(".codigoUnico").addClass('sr-only');
		$(".enlace").removeClass('sr-only');
	}

	$("#tipo").on('change', function(event) {
		event.preventDefault();
		if ($(this).val() == 'general') {
			$(".codigoGeneral").removeClass('sr-only');
			$(".codigoUnico").addClass('sr-only');
			$(".enlace").addClass('sr-only');
		}

		if ($(this).val() == 'unico') {
			$(".codigoGeneral").addClass('sr-only');
			$(".codigoUnico").removeClass('sr-only');
			$(".enlace").addClass('sr-only');
		}

		if ($(this).val() == 'enlace') {
			$(".codigoGeneral").addClass('sr-only');
			$(".codigoUnico").addClass('sr-only');
			$(".enlace").removeClass('sr-only');
		}
	});

	// horarios
	$(".btn-add-horario").on('click', function(event) {
		event.preventDefault();
		var boton = $(this);
		generarHorario(boton);
	});

	// Guardar imagen
	Dropzone.options.frmImagenes = {
		paramName: "imagen", 
		maxFilesize: 10,
		maxFiles: 1,
		method: 'post',
		createImageThumbnails: true,
		addRemoveLinks: true,
		dictDefaultMessage: "Arrastre la imagen del evento aquí.",
		dictFallbackMessage: "Su navegador no soporta el sistema de cargar de imagenes, le recomendamos usar Firefox, Google Chrome o IE > 9",
		init: function() {
			// Si todo salio bien
			this.on("success", function(file, response) {

				$("#btn-imagen").removeAttr('disabled');
				// id de la imagen guardada
				$('#id_imagen').val('');
				$('#id_imagen').val(response.imagen.id);

				var source   = $("#tmp-imagen").html();
				var template = Handlebars.compile(source);

				var context = {url: response.imagen.url, id: response.imagen.id};
				var html    = template(context);

				$(".homePublica-imagen").html('');
				$(".homePublica-imagen").append(html);
				$("#btn-imagen").removeClass('sr-only');
				// inicializar crop de la imagen
				var $imagen = $('#image').cropper({
					aspectRatio: 16 / 9,
					zoomable: false
				});
				// Guardar imagen
				$("#btn-imagen").off('click');
				$("#btn-imagen").on('click', function() {
					var boton = $(this);
					var data_img = $imagen.cropper('getData', true);
					var datos = {
						x: data_img.x,
						y: data_img.y,
						width: data_img.width,
						height: data_img.height,
						id: $("#image").data('id')
					}

					// Enviar datos para cortar la imagen
					postCropImagen(datos, function (data) {
						$(".homePublica-imagen").html('');
						$("#btn-imagen").addClass('sr-only');
						$(".eventoInfo").removeClass('sr-only');
					});
				});
			});

			this.on("maxfilesexceeded", function(file){
				this.removeFile(file);
			});

			this.on("removedfile", function(file) {
				$("#btn-imagen").addClass('sr-only');
				$(".homePublica-imagen").html('');
			});
		}
	};
});

function postCropImagen (datos, callback) {
	$.post('api/evento/imagen/cortar', datos, function(data, textStatus, xhr) {
		callback(data);
	});
}

function generarHorario (boton) {
	var source   = $("#tmp-horario").html();
	var template = Handlebars.compile(source);
	var html = template();

	$("#ctn-horario").append(html);

	boton.remove();

	$(".btn-add-horario").on('click', function(event) {
		event.preventDefault();
		var boton = $(this);
		generarHorario(boton);
	});

	$('.datepicker').datetimepicker({
		format: 'YYYY-MM-DD'
	});

	$('.datepicker-time').datetimepicker({
		format: 'H:m:s'
	});

}



/* Mapa */
/*var map;
var markers = [];

function initMap() {
	var haightAshbury = {lat: 4.7111, lng: -74.1577};

	map = new google.maps.Map(document.getElementById('mapa'), {
		zoom: 5,
		center: haightAshbury
	});

	map.addListener('dblclick', function(event) {
		addMarker(event.latLng);

		$("#longitud").val(event.latLng.lng());
		$("#latitud").val(event.latLng.lat());
	});

	addMarker(haightAshbury);
}

function addMarker(location) {
	deleteMarkers();
	var marker = new google.maps.Marker({
		position: location,
		map: map
	});
	markers.push(marker);
}

function setMapOnAll(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

function clearMarkers() {
	setMapOnAll(null);
}

function showMarkers() {
	setMapOnAll(map);
}

function deleteMarkers() {
	clearMarkers();
	markers = [];
}*/