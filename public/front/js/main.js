 $(document).on('ready', function() {

 	$('[data-toggle="tooltip"]').tooltip();

 	// Search menu
 	$('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
	
 	// menu estatico scroll
 	$("#menu").sticky({topSpacing:0});

 	if ($('.selectpicker').length > 0) {
 		$('.selectpicker').selectpicker();
 	}

 	if ($('.datepicker').length > 0) {
	 	$('.datepicker').datetimepicker({
			format: 'YYYY-MM-DD'
		});

		$('.datepicker-datetime').datetimepicker({
			format: 'YYYY-MM-DD H:m:s'
		});

		$('.datepicker-time').datetimepicker({
			format: 'H:m:s'
		});
 	}

 	if ($(":file").length > 0) {
		$(":file").filestyle({buttonName: "btn-success"});
 	}
 	
	// cuando se elige un departamento se cargan sus ciudades
	if ($("#departamento").length > 0) {
		if ($("#departamento").val() != 0) {
			getCiudades($("#departamento").val(), function (data) {

				var source   = $("#tmp-ciudades").html();
				var template = Handlebars.compile(source);

				var context = {ciudades: data};
				var html    = template(context);

				$("#ciudad").html(html);
			});
		}

		$("#departamento").on('change', function(event) {
			event.preventDefault();
			if ($(this).val() != 0) {
				getCiudades($(this).val(), function (data) {

					var source   = $("#tmp-ciudades").html();
					var template = Handlebars.compile(source);

					var context = {ciudades: data};
					var html    = template(context);

					$("#ciudad").html(html);
				});
			}
		});

	};

	if ($("#zona").length > 0) {
		$("#zona").on('change', function(event) {
			event.preventDefault();
			
			if ($(this).val() != 0) {
				changeZona($(this).val(), function (data) {
					if (data.status == 'success') {
						location.reload();
					};
				})
			}
		});
	};

});

// Obtener ciudades segun departamento
function getCiudades (id_departamento, callback) {
	$.get('/json/ciudades/'+id_departamento, function(data, textStatus, xhr) {
		callback(data);
	});
}

function changeZona (zona, callback) {
	$.get('/json/zona/'+zona, function(data) {
		callback(data);
	});
}