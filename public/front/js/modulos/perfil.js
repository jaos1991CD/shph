$(document).on('ready', function() {
	console.log('sisa');
	// Guardar imagen
	Dropzone.options.frmImagenes = {
		paramName: "imagen", 
		maxFilesize: 2,
		maxFiles: 1,
		method: 'post',
		createImageThumbnails: true,
		dictDefaultMessage: "Arrastre la imagen de perfil aquí. max (2mb)",
		dictFallbackMessage: "Su navegador no soporta el sistema de cargar de imagenes, le recomendamos usar Firefox, Google Chrome o IE > 9",
		init: function() {
			var drop = this;
			// Si todo salio bien
			this.on("success", function(file, response) {

				var source   = $("#tmp-imagen").html();
				var template = Handlebars.compile(source);

				var context = {url: response.imagen};
				var html    = template(context);

				$(".Perfil-imagen-upload").slideUp();
				$(".Perfil-imagen-crop").append(html);
				// inicializar crop de la imagen
				var $imagen = $('#image').cropper({
					aspectRatio: 1,
					zoomable: false,
					crop: function(e) {
						//console.log(e.x);
						//console.log(e.y);
					}
				});
				// Guardar imagen
				$(".Perfil-imagen-guardar").on('click', function() {
					var data_img = $imagen.cropper('getData', true);
					cortarImagen(data_img, drop, function (data) {
						location.reload(); 
					});
				});
				$('#modalPerfil').off('hide.bs.modal');
				$('#modalPerfil').on('hide.bs.modal', function () {
					var data_img = $imagen.cropper('getData', true);
					cortarImagen(data_img, drop, function (data) {
						location.reload(); 
					});
				});
			});

			this.on("maxfilesexceeded", function(file){
				this.removeFile(file);
			});
		}
	};
});

function postCropImagen (datos, callback) {
	$.post('/api/perfil/imagen/crop', datos, function(data, textStatus, xhr) {
		callback(data);
	});
}

function cortarImagen (data_img, drop, callback) {
	var datos = {
		x: data_img.x,
		y: data_img.y,
		width: data_img.width,
		height: data_img.height,
		id: $("#image").data('id')
	}

	// Enviar datos para cortar la imagen
	postCropImagen(datos, function (data) {
		$(".Perfil-imagen-upload").slideDown();
		$(".Perfil-imagen-crop").html('');
		drop.removeAllFiles();

		$('#modalPerfil').off('hide.bs.modal');
		$('#modalPerfil').modal('hide');

		callback(data);
	});
}