$(document).on('ready', function(){

	$('#guardar').attr("disabled", true);

	$("#id_suscripcion").on('change', function (e) {
		e.preventDefault();

		var plan = $("#id_suscripcion option:selected").html();
		var valor = $("#id_suscripcion option:selected").data('valor');
		updateTabla(plan, valor, valor, $("#codigo_actual").val(), $("#codigo_actual").data('valor'), function() {
			$('#guardar').attr("disabled", false);
		});
	});

	$("#aplicar").off('click');
	$("#aplicar").on('click', function(event) {
		event.preventDefault();
		$.blockUI({ message: '<h1>Procesando...</h1>' });

		var codigo= $("#codigo").val();
		var token = $('#token').val();

		$('#aplicar').attr("disabled", true);
		confirmCodigo(codigo, token, function(data, success){
			$.unblockUI();
			if (success) {
				$("#aplicar").attr('disabled', true);

				var descuento =  data.data.valor;

				var nombre = data.data.nombre;
				var id = data.data.id;
				var plan = $("#id_suscripcion option:selected").html();
				var valormes = $("#id_suscripcion option:selected").data('valor');
				var token = $('#token').val();

				// calculo de descuento
				valordescuento = (valormes * descuento) / 100;
				valortotal = valormes - valordescuento;
				
				updateTabla(plan, valormes, valortotal, nombre, valordescuento, function () {
					$("#codigo_actual").val(nombre);
					$("#codigo_actual").data('id', id);
					$("#codigo_actual").data('valor', valordescuento);
				});
			}else{
				$("#aplicar").removeAttr('disabled');
				$(".msj_error").html(data.message);
				$(".msj_error").removeClass('hide');

				setTimeout(function () {
					$(".msj_error").addClass('hide');
				}, 3000);
			}
		});

	});

	$('#guardar').on('click',function(e){
		e.preventDefault();
		$.blockUI({ message: '<h1>Redireccionando...</h1>' });

		var datos = {
			"suscripcion": $('#id_suscripcion').val(),
			"usuario": $('#id_usuario').val(),
			"codigo": $("#codigo_actual").data('id'),
			"token": $('#token').val()
		}
		
		var url = $("#id_suscripcion").data('url');
		
		postPagos(datos, url, function(data, success){
			

			if (success) {
				$("#frm-pago").submit();
				//alert('formulario enviado');
			}else{
				$("#aplicar").removeAttr('disabled');
				$(".msj_error").html(data.message);
				$(".msj_error").removeClass('hide');

				setTimeout(function () {
					$(".msj_error").addClass('hide');
				}, 3000);
			}
		});
	});
});

function postPagos(datos, url, callback) {
	$.ajax({
		method: 'post',
		url: url,
		data: datos,
		dataType: 'json',
		success: function(data){
			if (data.state == 'success' ) {

				callback(data, true);
			}else{
				callback(data, false);
			}
		},
		error: function(data){
			callback(data, false);
		}
	});
}

function confirmCodigo(codigo,token,callback){
	$.ajax({
		method: 'post',
		url: '/suscripcion/codigo',
		data:{'_token' : token, 'codigo': codigo},
		dataType: 'json',

		success: function(data){
			
			if (data.state === 'success' ) {
				callback(data, true);
			}
			else{
				callback(data, false);
			}
		},
		error: function(data){
			callback(data, false);
		}

	});
}

function updateTabla(plan, plan_valor, total_valor, descuento_nombre, descuento_valor, callback) {

	if (descuento_nombre != 0) {
		$(".txt-descuento").removeClass('sr-only');
		$(".txt-descuento-nombre").html("Descuento: "+descuento_nombre);
		$(".txt-descuento-valor").html("$ "+descuento_valor.toLocaleString()+" (COP)");
	}

	if ($("#codigo_actual").data('valor') != 0) {

		total_valor = plan_valor - $("#codigo_actual").data('valor');
		
		$(".txt-descuento").removeClass('sr-only');
		$(".txt-descuento-nombre").html($("#codigo_actual").val());
		$(".txt-descuento-valor").html($("#codigo_actual").data('valor'));
	}

	$(".txt-plan").html(plan);
	$(".txt-valor").html("$ "+plan_valor.toLocaleString()+" (COP)");
	$(".txt-total-valor").html("$ "+total_valor.toLocaleString()+" (COP)");

	$("#amount").val(total_valor);

	if(total_valor === 0){
			
		  var titulo = $("#frm-pago").attr('action');
          $("#frm-pago").attr('action', $("#url2").val());
          titulo = $("#frm-pago").attr('action');

          $("#guardar").html("Activar Membresía");
          console.log(titulo);
	}

	callback();
}