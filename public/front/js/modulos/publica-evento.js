//Dropzone.autoDiscover = false;
$(document).on('ready', function () {
	//Initialize tooltips
		$('.nav-tabs > li a[title]').tooltip();

	//Wizard
		$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
			var $target = $(e.target);
			if ($target.parent().hasClass('disabled')) {
				return false;
			}
		});

		$(".next-step").click(function (e) {
			var $active = $('.wizard .nav-tabs li.active');
			$active.next().removeClass('disabled');
			nextTab($active);
		});

		$(".prev-step").click(function (e) {
			var $active = $('.wizard .nav-tabs li.active');
			prevTab($active);
		});

		function nextTab(elem) {
			$(elem).next().find('a[data-toggle="tab"]').click();
		}

		function prevTab(elem) {
			$(elem).prev().find('a[data-toggle="tab"]').click();
		}

	// add horario
		$(".btn-add-horario").on('click', function(event) {
			event.preventDefault();
			var boton = $(this);
			generarHorario(boton);
		});

	// Guardar Imagen de evento (R)
		Dropzone.options.frmImagenes = {
	    	url: "api/evento/imagen",
	    	paramName: "imagen", 
			maxFilesize: 10,
			maxFiles: 1,
			method: 'post',
			addRemoveLinks: true,
			createImageThumbnails: true,
			acceptedFiles: "image/jpeg,image/png",
			dictDefaultMessage: "Arrastre la imagen del evento aquí.",
			dictInvalidFileType: "Este tipo de archivo no es válido",
			dictFallbackMessage: "Su navegador no soporta el sistema de cargar de imagenes, le recomendamos usar Firefox, Google Chrome o IE > 9",
			
			init: function () {
				this.on("addedfile", function(file) {
					if (file.type != 'image/jpeg' && file.type != 'image/png') {
						$("#error-imagen").removeClass('hide');
						$("#error-imagen").find('.alert').html('Este tipo de archivo no es permitido, por favor adjunte imagenes (JPG, PNG),');
						setTimeout(function () {
							$("#error-imagen").addClass('hide').find('.alert').html('');
						}, 5000);

						this.removeAllFiles();
					}
				});

				// Si todo salio bien
				this.on("success", function(file, response) {
					// id de la imagen guardada
					$('[name="id_imagen"]').val(response.imagen.id);

					var source   = $("#tmp-imagen").html();
					var template = Handlebars.compile(source);

					var context = {url: response.imagen.url, id: response.imagen.id};
					var html    = template(context);

					$(".homePublica-imagen").append(html);
					// inicializar crop de la imagen
					var $imagen = $('#image').cropper({
						aspectRatio: 16 / 9,
						zoomable: false
					});

					// Cortar y guardar imagen
					$("#btn-cortar-imagen").removeAttr('disabled');
					$("#btn-cortar-imagen").off('click');
					$("#btn-cortar-imagen").on('click', function() {
						$(".wizard").block({ 
							message: '<div class="alert alert-info">Cargando imagen...</div>',
							css: { border: 'none', background: 'none', color: 'white' } 
						});
						var data_img = $imagen.cropper('getData', true);

						var datos = {
							x: data_img.x,
							y: data_img.y,
							width: data_img.width,
							height: data_img.height,
							id: $("#image").data('id')
						}

						// Enviar datos para cortar la imagen
						postCropImagen(datos, function (data) {
							$(".homePublica-imagen").html('');
							$(".wizard").unblock();

							var $active = $('.wizard .nav-tabs li.active');
							$active.next().removeClass('disabled');
							nextTab($active);
						});
					});
				});

				this.on("removedfile", function(file) {
					//$("#btn-imagen").addClass('sr-only');
					$("#btn-cortar-imagen").attr('disabled', 'disabled');
					$(".homePublica-imagen").html('');
					$('[name="id_imagen"]').val(null);
				});
			}
	    };

	// Guardar Evento (R)
		$("#frm-evento").validator().on('submit', function(event) {
			if (event.isDefaultPrevented()) {
				$("#error-evento").removeClass('hide');
				$("#error-evento").find('.alert').html('Por favor rellene los campos requeridos.');
				setTimeout(function () {
					$("#error-evento").addClass('hide').find('.alert').html('');
				}, 5000);
			} else {
				event.preventDefault();
				var form = $(this);
				var id_aliado = $('[name="id_aliado"]').val();
				var id_imagen = $('[name="id_imagen"]').val();

				var fechas = [];
				$('.fecha').each(function(i, el) {
					fechas[i] = $(el).val();
				});

				var horas = [];
				$('.hora').each(function(i, el) {
					horas[i] = $(el).val();
				});

				datos = {
					"_token": $(this).find('[name="_token"]').val(),
					"nombre": $(this).find('[name="nombre"]').val(),
					"slug": $(this).find('[name="nombre"]').val(),
					"descripcion": $(this).find('[name="descripcion"]').val(),
					"direccion": $(this).find('[name="direccion"]').val(),
					"lugar": $(this).find('[name="lugar"]').val(),
					//"latitud": $(this).find('[name="latitud"]').val(),
					//"longitud": $(this).find('[name="longitud"]').val(),
					"web": $(this).find('[name="web"]').val(),
					"fecha_inicio": $(this).find('[name="fecha_inicio"]').val(),
					"fecha_fin": $(this).find('[name="fecha_fin"]').val(),
					"precio": $(this).find('[name="precio"]').val(),
					"descuento": $(this).find('[name="descuento"]').val(),
					"fechadesc": $(this).find('[name="fechadesc"]').val(),
					"id_categoria": $(this).find('[name="id_categoria"]').val(),
					"id_ciudad": $(this).find('[name="id_ciudad"]').val(),
					"terminos": $(this).find('[name="terminos"]').val(),
					"id_imagen": id_imagen,
					"id_aliado": id_aliado,
					"hora": horas,
					"fecha": fechas
				};

				$(".wizard").block({ 
					message: '<div class="alert alert-info">Cargando imagen...</div>',
					css: { border: 'none', background: 'none', color: 'white' } 
				});

				postEvento(datos, function (data, valido, validacion) {
					$(".wizard").unblock();

					if (valido) {
						var $active = $('.wizard .nav-tabs li.active');
						$active.next().removeClass('disabled');
						nextTab($active);

						$("html, body").animate({ scrollTop: 0 }, "slow");

						// desactivar las otras pestañas
						$('.wizard .nav-tabs li').addClass('disabled');
						$('.wizard .nav-tabs li.active').removeClass('disabled');

						$("#msj-success").removeClass('sr-only');

						$("#i_nombre").val(data.data.nombre);
						$("#i_descripcion").val(data.data.descripcion);
						$("#i_terminos").val(data.data.terminos);
						var id_e = data.data.id;

						$("#btn-actualizar").on('click', function(){
							var datos_actualizar = {
								"nombre":$("#i_nombre").val(),
								"descripcion":$("#i_descripcion").val(),
								"terminos":$("#i_terminos").val(),
								"_token": $('[name="_token"]').val(),
								"id": id_e
							}

							updateEvento(datos_actualizar, function (data) {
								alert(data.message);
								document.location.href = '/aliado/dashboard';
							})
						});
					}else{
						if (validacion) {
							// errores con la validacion
							$.each(data.responseJSON, function(index, val) {
								$('[name="'+index+'"]').parent().addClass('has-error');
								$.each(val, function(i, v) {
									$('[name="'+index+'"]').siblings('.help-block.with-errors').html(v);

									/*if (index == 'latitud') {
										$("#error-evento").removeClass('hide');
										$("#error-evento").find('.alert').html('Por favor seleccione una localización en el mapa.');
										setTimeout(function () {
											$("#error-evento").addClass('hide').find('.alert').html('');
										}, 5000);
									};*/
								});
							});
						}else{
							// error con el sistema
							alert(data.message);
						}
					}
				});
			}
		});
});

function generarHorario (boton) {
	var source   = $("#tmp-horario").html();
	var template = Handlebars.compile(source);
	var html = template();

	$("#ctn-horario").append(html);

	boton.remove();

	$(".btn-add-horario").on('click', function(event) {
		event.preventDefault();
		var boton = $(this);
		generarHorario(boton);
	});

	$('.datepicker').datetimepicker({
		format: 'YYYY-MM-DD'
	});

	$('.datepicker-time').datetimepicker({
		format: 'H:m:s'
	});

}

function postCropImagen (datos, callback) {
	$.post('api/evento/imagen/cortar', datos, function(data, textStatus, xhr) {
		callback(data);
	});
}

function postAliado (datos, callback) {
	$.ajax({
		type: 'post',
		url: '/api/aliado/create',
		data: datos,
		dataType: 'json',
		success: function(data){
			if (data.state == 'success') {
				callback(data, true, false);
			}else{
				callback(data, false, false);
			}
		},
		error: function(data){
			callback(data, false, true);
		}
	});

	// callback(data [datos], boolean [si ocurrio error], boolean [si es de validacion en el server])
}

function postEvento (datos, callback) {
	$.ajax({
		type: 'post',
		url: '/api/evento/create',
		data: datos,
		dataType: 'json',
		success: function(data){
			if (data.state == 'success') {
				callback(data, true, false);
			}else{
				callback(data, false, false);
			}
		},
		error: function(data){
			callback(data, false, true);
		}
	});
}

function updateEvento (datos, callback) {
	$.ajax({
		type: 'post',
		url: '/api/evento/actualizar',
		data: datos,
		dataType: 'json',
		success: function(data){
			callback(data);
			/*if (data.state == 'success') {
				callback(data, true, false);
			}else{
				callback(data, false, false);
			}*/
		},
		error: function(data){
			//callback(data, false, true);
		}
	});
}
