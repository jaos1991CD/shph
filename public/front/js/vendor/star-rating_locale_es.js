/*!
 * Star Rating <LANG> Translations
 *
 * This file must be loaded after 'fileinput.js'. Patterns in braces '{}', or
 * any HTML markup tags in the messages must not be converted or translated.
 *
 * @see http://github.com/kartik-v/bootstrap-star-rating
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
(function ($) {
    "use strict";
    $.fn.ratingLocales['es'] = {
        defaultCaption: '{rating} estrellas',
        starCaptions: {
            0.5: 'Half Star',
            1: 'Una estrella',
            1.5: 'One & Half Star',
            2: 'Dos estrellas',
            2.5: 'Two & Half Stars',
            3: 'Tres estrellas',
            3.5: 'Three & Half Stars',
            4: 'Cuatro estrellas',
            4.5: 'Four & Half Stars',
            5: 'Cinco estrellas'
        },
        clearButtonTitle: 'Borrar',
        clearCaption: 'No calificado'
    };
})(window.jQuery);
