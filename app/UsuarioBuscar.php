<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioBuscar extends Model
{
    protected $table = 'usuario';
    public $timestamps = false;
}
