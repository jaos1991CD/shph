<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuscarCodigo extends Model
{
    protected $table = 'suscripcion';
    public $timestamps = false;
}
