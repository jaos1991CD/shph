<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistroPago extends Model
{
    protected $table = 'usuario_suscripcion';
    public $timestamps = false;
}
