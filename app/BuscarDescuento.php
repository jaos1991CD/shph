<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuscarDescuento extends Model
{
    protected $table = 'aliado_codigo';
    public $timestamps = false;
}
