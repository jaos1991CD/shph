<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table = 'evento';
	protected $fillable = [
		'nombre',
		'slug',
		'descripcion',
		'terminos',
		'direccion',
		'lugar',
		'web',
		'fecha_inicio',
		'fecha_fin',
		'precio',
		'descuento',
		'fechadesc',
		'longitud',
		'latitud',
		'tipo',
		'codigo',
		'enlace',
		'id_aliado',
		'id_estado',
		'id_categoria',
		'id_imagen',
		'id_ciudad'
	];
	protected $perPage = 20;

	// Relaciones
	public function aliado()
	{
		return $this->belongsTo('Shph\Aliado', 'id_aliado');
	}

	public function estado()
	{
		return $this->hasOne('Shph\EventoEstado', 'id', 'id_estado');
	}

	public function categoria()
	{
		return $this->hasOne('Shph\EventoCategoria', 'id', 'id_categoria');
	}

	public function imagen()
	{
		return $this->belongsTo('Shph\EventoImagen', 'id_imagen');
	}

	public function ciudad()
	{
		return $this->belongsTo('Shph\DepartamentoCiudad', 'id_ciudad');
	}

	public function comentarios()
	{
		return $this->hasMany('Shph\EventoComentario', 'id_evento');
	}

	public function codigos()
	{
		return $this->hasMany('Shph\EventoCodigo', 'id_evento');
	}

	public function beneficios()
    {
        return $this->hasMany('Shph\Beneficio', 'id_evento');
    }

    public function horarios()
    {
        return $this->hasMany('Shph\EventoHorario', 'id_evento');
    }

    // metodos
    // Formato de fechas en español
    public function getFechaInicioFormatAttribute()
    {
    	$fecha = new \Date($this->attributes['fecha_inicio']);
    	return $fecha->format('l j F Y');
    }

    /*public function getHoraInicioFormatAttribute()
    {
    	$fecha = new \Date($this->attributes['hora_inicio']);
    	return $fecha->format('g:ia');
    }*/
}
