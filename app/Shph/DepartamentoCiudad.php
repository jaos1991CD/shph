<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class DepartamentoCiudad extends Model
{
    protected $table = 'departamento_ciudad';
    protected $fillable = ['nombre', 'descripcion', 'id_departamento'];

    // Relaciones
    public function departamento()
    {
        return $this->belongsTo('Shph\Departamento', 'id_departamento');
    }

    public function eventos()
    {
        return $this->hasMany('Shph\Evento', 'id_ciudad');
    }

    public function usuarios()
    {
        return $this->hasMany('Shph\UsuarioPerfil', 'id_ciudad');
    }

    public function getList()
    {
        $lista = $this->where('id', '!=', 1)->lists('nombre', 'id')->toArray();
        $lista = [0 => 'Seleccione uno'] + $lista;
        return $lista;
    }
}
