<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class EventoComentario extends Model
{
    protected $table = 'evento_comentario';
	protected $fillable = ['nombre', 'mensaje', 'calificacion', 'id_evento', 'id_usuario'];

	public function evento()
    {
        return $this->belongsTo('Shph\Evento', 'id_evento');
    }

    public function usuario()
    {
        return $this->belongsTo('Shph\Usuario', 'id_usuario');
    }
}
