<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class Aliado extends Model
{
	protected $table = 'aliado';
	protected $fillable = ['nombre', 'nit', 'direccion', 'web', 'estado', 'id_usuario', 'id_ciudad'];

    // Relaciones
	public function contacto()
	{
		return $this->hasOne('Shph\AliadoContacto', 'id_aliado');
	}
	public function codigos()
	{
		return $this->hasMany('Shph\Codigo', 'id_aliado');
	}

	public function eventos()
	{
		return $this->hasMany('Shph\Evento', 'id_aliado');
	}

	public function usuario()
	{
		return $this->belongsTo('Shph\Usuario', 'id_usuario');
	}

	public function getList()
    {
        $lista = $this->where('id', '!=', 1)->lists('nombre', 'id')->toArray();
        $lista = [0 => 'Seleccione uno'] + $lista;
        return $lista;
    }
}
