<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class ContenidoVideo extends Model
{
    protected $table = 'contenido_video';
    protected $fillable = ['identificador'];

}
