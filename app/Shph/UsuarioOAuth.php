<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class UsuarioOAuth extends Model
{
    protected $table = 'usuario_oauth';
    protected $fillable = ['provider', 'provider_id', 'id_usuario'];

    // Relaciones
    public function usuario()
    {
        return $this->belongsTo('Shph\Usuario', 'id_usuario');
    }
}
