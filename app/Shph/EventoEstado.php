<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class EventoEstado extends Model
{
    protected $table = 'evento_estado';
	protected $fillable = ['nombre', 'descripcion'];

	// Relaciones
    public function eventos()
    {
        return $this->hasMany('Shph\Evento', 'id');
    }

    public function getList()
    {
        $lista = $this->lists('nombre', 'id')->toArray();
        $lista = [0 => 'Seleccione uno'] + $lista;
        return $lista;
    }
}
