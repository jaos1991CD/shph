<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class Suscripcion extends Model
{
    protected $table = 'suscripcion';
	protected $fillable = ['nombre', 'descripcion','valor','tipo', 'meses'];
    protected $casts = [
        'valor' => 'integer',
    ];

	// Relaciones
    public function usuarios()
    {
        return $this->belongsToMany('Shph\Usuario', 'usuario_suscripcion', 'id_suscripcion', 'id_usuario')
            ->withPivot(
                'id_codigo',
                'fecha_inicio',
                'fecha_vencimiento',
                'estado',
                'token',
                'numeros',
                'fechaexp',
                'idTransaccion',
                'idEstado',
                'nombreEstado',
                'referencia',
                'codigoRespuesta',
                'codigoAutorizacion',
                'riesgo',
                'valor',
                'iva',
                'baseDevolucion',
                'fechaProcesamiento',
                'mensaje',
                'id_suscripcion'
            );
    }

    public function getList()
    {
        $lista = $this->lists('nombre', 'id')->toArray();
        $lista = ['' => 'Seleccione uno'] + $lista;
        return $lista;
    }
    
}
