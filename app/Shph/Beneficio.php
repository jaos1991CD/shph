<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class Beneficio extends Model
{
	protected $table = 'beneficio';
	protected $fillable = ['id_evento', 'id_usuario', 'estado', 'codigo', 'enlace', 'fecha_redimido'];

	// Relaciones
	public function evento()
	{
		return $this->belongsTo('Shph\Evento', 'id_evento');
	}

	public function usuario()
	{
		return $this->belongsTo('Shph\Usuario', 'id_usuario');
	}
}
