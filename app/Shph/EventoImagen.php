<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class EventoImagen extends Model
{
    protected $table = 'evento_imagen';
	protected $fillable = ['nombre', 'url'];

	// Relaciones
    public function evento()
    {
        return $this->hasMany('Shph\Evento', 'id_imagen');
    }
}
