<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class UsuarioPerfil extends Model
{
    protected $table = 'usuario_perfil';
    protected $fillable = ['nombres', 'apellidos', 'genero', 'fecha_nacimiento', 'id_ciudad', 'id_usuario'];

    // Relaciones
    public function usuario()
    {
        return $this->belongsTo('Shph\Usuario', 'id_usuario');
    }

    public function ciudad()
    {
        return $this->belongsTo('Shph\DepartamentoCiudad', 'id_ciudad');
    }

    // Atributos virtuales
    public function getNombreCompletoAttribute()
    {
        return $this->nombres." ".$this->apellidos;
    }

    public function getFechaNacimientoCarbonAttribute()
    {
        return \Carbon\Carbon::parse($this->fecha_nacimiento);
    }
}
