<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class AliadoContacto extends Model
{
	protected $table = 'aliado_contacto';
	protected $fillable = ['nombre', 'email', 'telefono', 'id_aliado'];

	// Relaciones
	public function aliado()
	{
		return $this->belongsTo('Shph\Aliado', 'id_aliado');
	}
}
