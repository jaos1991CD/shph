<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class EventoHorario extends Model
{
    protected $table = 'evento_horario';
    protected $fillable = ['fecha', 'hora', 'id_evento'];

    // Relaciones
    public function evento()
    {
        return $this->belongsTo('Shph\Evento', 'id_evento');
    }

    // metodos
    // Formato de fechas en español
    public function getFechaFormatAttribute()
    {
    	$fecha = new \Date($this->attributes['fecha']);
    	return $fecha->format('l j F Y');
    }

    public function getHoraFormatAttribute()
    {
    	$fecha = new \Date($this->attributes['hora']);
    	return $fecha->format('g:ia');
    }
}
