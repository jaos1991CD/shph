<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'departamento';
    protected $fillable = ['nombre', 'descripcion'];

    // Relaciones
    public function ciudades()
    {
        return $this->hasMany('Shph\DepartamentoCiudad', 'id_departamento');
    }

    public function region()
    {
        return $this->belongsTo('Shph\DepartamentoRegion', 'id_region');
    }

    public function getList()
    {
    	$lista = $this->lists('nombre', 'id')->toArray();
    	$lista = [0 => 'Seleccione uno'] + $lista;
    	return $lista;
    }
}
