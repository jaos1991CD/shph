<?php

namespace Shph;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Usuario extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    
    protected $table = 'usuario';
    protected $fillable = ['correo', 'password', 'tipo', 'confirmation_token', 'estado','acepto'];
    protected $hidden = ['password', 'remember_token'];

    // Relaciones
    public function perfil()
    {
        return $this->hasOne('Shph\UsuarioPerfil', 'id_usuario');
    }

    public function suscripcion()
    {
        return $this->belongsToMany('Shph\Suscripcion', 'usuario_suscripcion', 'id_usuario', 'id_suscripcion')
            ->withPivot(
                'id_codigo',
                'fecha_inicio',
                'fecha_vencimiento',
                'estado',
                'token',
                'numeros',
                'fechaexp',
                'idTransaccion',
                'idEstado',
                'nombreEstado',
                'referencia',
                'codigoRespuesta',
                'codigoAutorizacion',
                'riesgo',
                'valor',
                'iva',
                'baseDevolucion',
                'fechaProcesamiento',
                'mensaje',
                'id_suscripcion'
            );
    }

    public function oauth()
    {
        return $this->hasMany('Shph\UsuarioOAuth', 'id_usuario');
    }

    public function comentarios()
    {
        return $this->hasMany('Shph\EventoComentario', 'id_usuario');
    }

    public function beneficios()
    {
        return $this->hasMany('Shph\Beneficio', 'id_usuario');
    }

    public function aliado()
    {
        return $this->hasOne('Shph\Aliado', 'id_usuario');
    }

    // Set attributes
    public function setPasswordAttribute($value)
    {
        if (! empty($value)) {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function scopeFecha()
    {

    }
}
