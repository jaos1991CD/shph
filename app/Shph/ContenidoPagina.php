<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class ContenidoPagina extends Model
{
    protected $table = 'contenido_pagina';
    protected $fillable = ['nombre', 'titulo', 'lead', 'texto'];

}
