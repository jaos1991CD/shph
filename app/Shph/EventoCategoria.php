<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class EventoCategoria extends Model
{

	protected $table = 'evento_categoria';
	protected $fillable = ['nombre', 'color', 'slug', 'posicion', 'descripcion'];

    // Relaciones
    public function evento()
    {
        return $this->hasMany('Shph\Evento', 'id');
    }

    public function getList()
    {
        $lista = $this->lists('nombre', 'id')->toArray();
        $lista = [0 => 'Seleccione uno'] + $lista;
        return $lista;
    }
}
