<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class Codigo extends Model
{
    protected $table = 'aliado_codigo';
	protected $fillable = ['id_aliado', 'nombre', 'tipo','valor','estado'];

    // Relaciones


	public function aliado()
	{
		return $this->hasMany('Shph\Aliado', 'id_aliado');
	}
}
