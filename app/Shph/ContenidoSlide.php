<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class ContenidoSlide extends Model
{
    protected $table = 'contenido_slide';
    protected $fillable = ['imagen', 'titulo', 'enlace', 'descripcion', 'posicion', 'estado'];

}
