<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class EventoCodigo extends Model
{
	protected $table = 'evento_codigo';
	protected $fillable = ['nombre', 'estado', 'id_evento'];

	// Relaciones
	public function evento()
	{
		return $this->belongsTo('Shph\Evento', 'id_evento');
	}
}
