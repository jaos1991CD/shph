<?php

namespace Shph;

use Illuminate\Database\Eloquent\Model;

class DepartamentoRegion extends Model
{
    protected $table = 'departamento_region';
    protected $fillable = ['nombre'];

    // Relaciones
    public function departamento()
    {
        return $this->hasMany('Shph\Departamento', 'id_departamento');
    }

    public function getList()
    {
        $lista = $this->orderBy('nombre', 'asc')->where('nombre', 'cali')->lists('nombre', 'id')->toArray();
        $lista = [0 => 'Selecciona tu Región'] + $lista;
        return $lista;
    }
}
