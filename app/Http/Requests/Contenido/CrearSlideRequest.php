<?php

namespace App\Http\Requests\Contenido;

use App\Http\Requests\Request;

class CrearSlideRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imagen' => 'required|image_size:800,500|image',
            'titulo' => 'required',
            'enlace' => 'required',
        ];
    }
}
