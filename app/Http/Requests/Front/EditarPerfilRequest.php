<?php

namespace App\Http\Requests\Front;

use App\Http\Requests\Request;

class EditarPerfilRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres' => 'required|min:3',
            'apellidos' => 'required|min:3',
            'email' => 'required|email|unique:usuario,correo,'.\Auth::user()->id,
            'genero' => 'required',
            'id_departamento' => 'required',
            'id_ciudad' => 'required',
            'password' => 'confirmed'
        ];
    }
}
