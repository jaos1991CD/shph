<?php

namespace App\Http\Requests\Front;

use App\Http\Requests\Request;

class CrearAliadoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_aliado' => 'required',
            'direccion_aliado' => 'required',

            'nombre_contacto' => 'required',
            'email_contacto' => 'required|email|unique:aliado_contacto,email',
            'telefono_contacto' => 'required',
        ];
    }
}
