<?php

namespace App\Http\Requests\Front;

use App\Http\Requests\Request;

class RegisterAliadoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_aliado' => 'required',
            'direccion_aliado' => 'required',
            'id_ciudad_aliado' => 'required|numeric',

            'nombre_contacto' => 'required',
            'email_contacto' => 'required|email|unique:usuario,correo',
            'telefono_contacto' => 'required',
            'password' => 'required|confirmed|min:6',
            'terminos' => 'required'
        ];
    }
}
