<?php

namespace App\Http\Requests\Front;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres' => 'required',
            'apellidos' => 'required',
            'email' => 'required|email|unique:usuario,correo',
            'genero' => 'required|in:m,f',
            'fecha_nacimiento' => 'required|date',
            'id_departamento' => 'required|numeric',
            'id_ciudad' => 'required|numeric',
            'password' => 'required|confirmed|min:6',
            'terminos' => 'required'
        ];
    }
}
