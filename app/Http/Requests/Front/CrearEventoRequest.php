<?php

namespace App\Http\Requests\Front;

use App\Http\Requests\Request;

class CrearEventoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'descripcion' => 'required',
            'direccion' => 'required',
            'lugar' => 'required',
            //'latitud' => 'required',
            //'longitud' => 'required',
            'web' => 'required',
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date',
            'precio' => 'required',
            'id_categoria' => 'required',
            'id_ciudad' => 'required',
            'terminos' => 'required',
            'id_imagen' => 'required',
            'id_aliado' => 'required'
        ];
    }
}
