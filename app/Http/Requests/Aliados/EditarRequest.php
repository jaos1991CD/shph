<?php

namespace App\Http\Requests\Aliados;

use App\Http\Requests\Request;

class EditarRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'nombre_contacto' => 'required',
            'email_contacto' => 'required|email'
        ];
    }
}
