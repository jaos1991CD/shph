<?php

namespace App\Http\Requests\Eventos;

use App\Http\Requests\Request;

class CrearRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'descripcion' => 'required',
            'terminos' => 'required',
            'direccion' => 'required',
            //'latitud' => 'required',
            //'longitud' => 'required',
            'fechadesc' => 'required',
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date',
            'id_aliado' => 'required|numeric',
            'id_categoria' => 'required|numeric',
            'id_ciudad' => 'required|numeric',
            'id_estado' => 'required',
            'id_imagen' => 'required'
        ];
    }
}
