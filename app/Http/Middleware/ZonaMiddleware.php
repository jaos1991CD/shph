<?php

namespace App\Http\Middleware;

use Closure;

class ZonaMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ! session()->has('zona')) {
            session()->put('zona', 2);
        }
        
        return $next($request);
    }
}
