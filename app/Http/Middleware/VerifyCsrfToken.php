<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */

    protected $except = [
        'json/*',
        'api/evento/imagen',
        'api/evento/imagen/cortar',
        'api/perfil/imagen/crop',
        'respuesta/*',
        'pagos',
        'postman/*',
        'pagos/usuario/*',
        'contenido/slide/*'
    ];
}
