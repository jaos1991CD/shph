<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Shph\EventoCategoria;
use Shph\DepartamentoRegion;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(
    	EventoCategoria $categoria,
    	DepartamentoRegion $region
    	)
    {

		$categorias = $categoria->orderBy('posicion','asc')->get();
		$regiones = $region->getList();
        
		view()->share('categorias_menu', $categorias);
		view()->share('regiones', $regiones);
    }
}