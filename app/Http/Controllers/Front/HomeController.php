<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Session\SessionManager;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Carbon\Carbon;

use Shph\Departamento;
use Shph\DepartamentoRegion;
use Shph\DepartamentoCiudad;
use Shph\Evento;
use Shph\EventoComentario;
use Shph\EventoCategoria;
use Shph\EventoEstado;
use Shph\ContenidoVideo;
use Shph\ContenidoPagina;
use Shph\ContenidoSlide;
use Shph\Beneficio;
use Shph\Usuario;
use Shph\Suscripcion;

class HomeController extends Controller
{
	protected $departamento, $ciudad, $evento, $comentario, $video, $beneficio, $pagina, $region,$usuario,$suscripcion;

	public function __construct(Departamento $departamento,
		DepartamentoCiudad $ciudad,
		DepartamentoRegion $region,
		Evento $evento,
		EventoCategoria $categoria,
        EventoEstado $estado,
        ContenidoVideo $video,
        ContenidoPagina $pagina,
        ContenidoSlide $slide,
        Beneficio $beneficio,
        Usuario $usuario,
        Suscripcion $suscripcion,
		EventoComentario $comentario)
	{
		parent::__construct($categoria, $region);
		$this->departamento = $departamento;
		$this->ciudad = $ciudad;
		$this->evento = $evento;
		$this->comentario = $comentario;
		$this->categoria = $categoria;
        $this->estado = $estado;
        $this->video = $video;
        $this->beneficio = $beneficio;
        $this->pagina = $pagina;
        $this->slide = $slide;
        $this->suscripcion= $suscripcion;
        $this->usuario= $usuario;
	}

	public function index()
	{
		$video = $this->video->find(1);
		$slides = $this->slide->where('estado', 'activo')->orderBy('id', 'desc')->get();
		return view('front.home', compact('video', 'slides'));
	}

	// Eventos segun el slug
	public function getEventos(Request $request, $slug)
	{	

		$eventos = $this->evento->with(['horarios', 'imagen', 'ciudad.departamento.region'])
			/*->join('departamento_ciudad', 'departamento_ciudad.id', '=', 'evento.id_ciudad')
			->join('departamento', 'departamento.id', '=', 'departamento_ciudad.id_departamento')
			->join('departamento_region', 'departamento_region.id', '=', 'departamento.id_region')
			->join('evento_imagen', 'evento_imagen.id', '=', 'evento.id_imagen')
			->where('departamento_region.id', session()->get('zona'))*/
			->whereHas('ciudad.departamento.region', function ($q)
			{
				return $q->where('id', session()->get('zona'));
			})
			->where('id_estado', 1)
			->whereRaw("'".Carbon::now()->toDateString()."' BETWEEN fecha_inicio AND fecha_fin")
			->groupBy('evento.id')
			->orderBy('created_at','desc');

		if ($slug == 'hoy') {
			$eventos = $eventos->whereHas('horarios', function ($query){
				return $query->where('fecha', '=', Carbon::now()->toDateString());
			});
		}

		if ($slug == 'esta-semana') {
			$eventos = $eventos->whereHas('horarios', function ($query){
				return $query->whereRaw("WEEKOFYEAR(fecha)=WEEKOFYEAR(NOW())");
			});
		}

		if ($slug == 'este-mes') {
			/*$eventos = $eventos->whereRaw("YEAR(fecha_inicio) = YEAR(NOW())");
			$eventos = $eventos->whereRaw("MONTH(fecha_inicio) = MONTH(NOW())");*/

			$eventos = $eventos->whereHas('horarios', function ($query){
				return $query->whereRaw("YEAR(fecha) = YEAR(NOW())")->whereRaw("MONTH(fecha) = MONTH(NOW())");
			});
		}

		if ($slug == 'fin-de-semana') {
			$viernes = Carbon::now()->endOfWeek()->subDays(2)->toDateString();
			$domingo = Carbon::now()->endOfWeek()->toDateString();

			$eventos = $eventos->whereHas('horarios', function ($query) use ($viernes, $domingo){
				return $query->whereRaw("fecha BETWEEN '".$viernes."' AND '".$domingo."'");
			});
		}

		if ($slug == 'buscar') {
			$eventos = $eventos->where(function ($query) use ($request)
			{
				$query->where('nombre', 'like', '%'.$request->get('q').'%');
				$query->orWhere('descripcion', 'like', '%'.$request->get('q').'%');
			});
		}

		$eventos = $eventos->paginate(12);
		return view('front.eventos', compact('eventos', 'slug'));
	}

	// Eventos por categoria
	public function getEventosCategoria($slug)
	{
		$categoria = $this->categoria->where('slug', $slug)->first();

		if ( ! is_null($categoria)) {
			$eventos = $this->evento->with(['horarios', 'imagen', 'ciudad.departamento.region'])
				->where('id_estado', 1)
				->whereRaw("'".Carbon::now()->toDateString()."' BETWEEN fecha_inicio AND fecha_fin")
				->where('id_categoria', $categoria->id)
				->whereHas('ciudad.departamento.region', function ($q)

				{
					return $q->where('id', session()->get('zona'));
				})
				->orderBy('created_at','desc')
                 ->Paginate(12);

			return view('front.eventos', compact('eventos', 'categoria'));
		}

		abort(404, 'No existe esa categoria');
	}
	// Evento
	public function getEvento($ciudad, $slug)
	{
		$evento = $this->evento->with('aliado', 'aliado.contacto', 'imagen', 'categoria')->where('slug', $slug)->first();

		if (is_null($evento)) {
			abort(404, 'No existe este evento.');
		}
		
		// verificar si el usuario ya ha solicitado este evento
		$beneficio = false;
		if (auth()->check()) {
			$beneficio = auth()->user()->beneficios()->where('id_evento', $evento->id)->exists();
			$suscripciones = \Auth::user()->suscripcion()->whereRaw("'".Carbon::now()->toDateString()."' BETWEEN fecha_inicio AND fecha_vencimiento")->where('usuario_suscripcion.estado','activo')->orderBy('id', 'desc')->first();
			$usuario= \Auth::user();
		}

		$proximo_horario = $evento->horarios()->whereRaw('concat(fecha, " ", hora) >= \''.Carbon::now().'\'')->orderBy('fecha')->first();
		$horarios = $evento->horarios()->whereRaw('concat(fecha, " ", hora) >= \''.Carbon::now().'\'')->orderBy('fecha')->get();

		$comentarios = $evento->comentarios()->where('estado', 'activo')->get();

		/* Calendario */
		$data = [];
		foreach ($horarios as $i => $horario) {
			$data[Carbon::parse($horario->fecha)->day] = "#";
		}

		$config = array(
			'start_day' => 'monday',
			'month_type' => 'long'
		);

			
		\Calendar::initialize($config);
		$calendar = \Calendar::generate(Carbon::now()->year, Carbon::now()->month, $data);

		return view('front.evento', compact('usuario','evento', 'beneficio', 'proximo_horario', 'horarios', 'comentarios', 'calendar','suscripciones'));
	}

	public function getCiudades(Request $request, $id_departamento)
	{
		if($request->ajax()){

			$departamento = $this->departamento->findOrFail($id_departamento);
			$ciudades = $this->ciudad->where('id_departamento', $departamento->id)->get();
			//$ciudades = $this->ciudad->where('slug', $slug)->first();
					
			return $ciudades;
		}

		abort(401, 'No autorizado');
	}
	// validar y sacar de aqui
	public function postComentario(Request $request)
	{
		$comentario = new $this->comentario;

		$comentario->fill([
			'nombre' => '', //quitar nombre
			'mensaje' => $request->get('comentario'),
			'calificacion' => $request->get('calificacion'),
			'id_evento' => $request->get('id'),
			'id_usuario' => \Auth::user()->id
		]);

		if ($comentario->save()) {
			return redirect()->route('front_evento', ['id' => $comentario->id_evento])->with('msj_success_comentario', 'Tu comentario esta en estado de aprobación por parte del Administrador. Una vez sea aprobado podrás verlo debajo de la descripción del evento.');
		}
	}

	// Formulario para enviar evento
	public function getPublicaTuEvento()
	{
		$ciudades = $this->ciudad->getList();
        $categorias = $this->categoria->getList();

		return view('front.formulario', compact('ciudades', 'categorias'));
	}

	
	public function getNosotros()
	{
		$pagina = $this->pagina->where('nombre', 'nosotros')->first();

		return view('front.pagina', compact('pagina'));
	}

	public function getTerminos()
	{
		$pagina = $this->pagina->where('nombre', 'terminos')->first();
		
		return view('front.pagina', compact('pagina'));
		return redirect()->back()->withInput(Input::all());

	}

	// Cambiar zona
	public function changeZona(Request $request, $zona)
	{
		if ($zona != 0) {
			session()->put('zona', $zona);

			if ($request->ajax()) {
				return response()->json(['status' => 'success', 'message' => 'Zona actualizada correctamente']);
			}

			return redirect()->route('home');
		}
	}
}
