<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Shph\Aliado;
use Shph\AliadoContacto;
use Shph\Beneficio;
use Shph\DepartamentoCiudad;
use Shph\DepartamentoRegion;
use Shph\Evento;
use Shph\EventoCategoria;
use Shph\EventoHorario;
use Shph\EventoImagen;
use Shph\Usuario;
use Shph\EventoEstado;
use Shph\Suscripcion;
use Carbon\Carbon;

use App\Http\Requests\Front\CrearAliadoRequest;
use App\Http\Requests\Front\CrearContactoRequest;
use App\Http\Requests\Front\CrearEventoRequest;

class EventoController extends Controller
{
    protected $aliado, $beneficio, $ciudad, $contacto, $evento, $horario, $imagen, $estado, $usuario,$suscripcion;
    protected $path_imagenes = '/front/img/eventos/';

    public function __construct(
        Aliado $aliado,
        EventoEstado $estado,
        AliadoContacto $contacto,
        Beneficio $beneficio,
        DepartamentoCiudad $ciudad,
        DepartamentoRegion $region,
        Evento $evento,
        EventoCategoria $categoria,
        EventoHorario $horario,
        EventoImagen $imagen,
        Usuario $usuario,
        Suscripcion $suscripcion
        )
    {
        parent::__construct($categoria, $region);
        $this->aliado = $aliado;
        $this->beneficio = $beneficio;
        $this->ciudad = $ciudad;
        $this->contacto = $contacto;
        $this->evento = $evento;
         $this->estado = $estado;
        $this->horario = $horario;
        $this->imagen = $imagen;
        $this->usuario = $usuario;
        $this->suscripcion =$suscripcion;
    }

    // Solicitar codigo por parte del usuario
    public function getSolicitar(Request $request, $id)
    {

        try {
            \DB::beginTransaction();
            $evento = $this->evento->findOrFail($id);
            $codigo = null;
            $enlace = null;
            $usuario = \Auth::user();
                
            // buscar si el usuario ya tiene este beneficio
            $beneficio = $this->beneficio->where('id_usuario', \Auth::user()->id)->where('id_evento', $evento->id);

            if ($beneficio->exists()) {
                return redirect()->route('front_evento', ['id' => $evento->id, 'slug' => $evento->slug, 'ciudad' => $evento->ciudad->slug])->with('msj_error', 'Usted ya ha solicitado este beneficio anteriormente, Gracias por su interés.');
            }

            if ($evento->tipo == 'general') {
                $codigo = $evento->codigo;
            }

            //evento horario hoy 
            $horario = $evento->horarios()->where('fecha', '>=', Carbon::now()->toDateString())->orderBy('fecha', 'DESC')->first();

                
            /*if ($evento->tipo == 'enlace') {
                $enlace = $evento->enlace;
            }*/

            /*if ($evento->tipo == 'unico') {
                $evento_codigo = $evento->codigos()->where('estado', 'activo')->first();

                if (is_null($evento_codigo)) {
                    return redirect()->route('front_evento', ['id' => $evento->id])->with('msj_error', 'Este evento ya no tiene beneficios disponibles');
                }

                $evento_codigo->estado = 'inactivo';
                $evento_codigo->save();

                $codigo = $evento_codigo->nombre;
            }*/

            $beneficio = new $this->beneficio;
            $beneficio->fill([
                'id_evento' => $evento->id,
                'id_usuario' => \Auth::user()->id,
                'estado' => 'solicitado',
                'codigo' => $codigo,
                'enlace' => $enlace
            ]);

            if ($beneficio->save()) {
                \DB::commit();

                // Correo que se le envia al usuario
                \Mail::send('email.beneficio-usuario', compact('usuario','evento','horario'), function ($m)
                {
                    $m->to(\Auth::user()->correo, \Auth::user()->perfil->nombre_completo)->subject('Solicitud de beneficio');
                });

                // correo que se le envia al aliado
                \Mail::send('email.beneficio-aliado', compact('evento', 'usuario'), function ($m) use ($evento)
                {
                    $m->to($evento->aliado->contacto->email, $evento->aliado->contacto->nombre)->subject('Solicitud de beneficio');
                });


                return redirect()->route('front_evento', ['id' => $evento->id , 'slug' => $evento->slug, 'ciudad' => $evento->ciudad->slug])->with('msj_success', 'A su correo debe llegar la información para redimir este beneficio.');
            }

        } catch (ModelNotFoundException $e) {
            abort(404, 'La página que usted busca no está disponible.');
        }
    }
    // guardar imagen del evento
    public function postSaveImagen(Request $request)
    {
        $nombre = str_random(30);

        $archivo = $request->file('imagen');
        $nombre_imagen = $nombre.'.'.$archivo->getClientOriginalExtension();
        $destino = public_path().$this->path_imagenes;
        $archivo->move($destino, $nombre_imagen);
        $url = url($this->path_imagenes.$nombre_imagen);

        $imagen = new $this->imagen;

        $imagen->fill([
            'nombre' => $nombre_imagen,
            'url' => $url,
        ]);

        if ($imagen->save()) {
            return response()->json(['state' => 'success', 'message' => 'Archivo Cargado', 'imagen' => $imagen], 200);
        }

        return response()->json(['state' => 'error', 'message' => 'Ocurrió un error.', 'data' => null], 200);
    }
    // cortar imagen
    public function postCropImagen(Request $request)
    {
        //return [];
        $imagen = $this->imagen->find($request->get('id'));

        $img = \Image::make(url($this->path_imagenes.$imagen->nombre))
            ->crop((int) $request->get('width'), (int) $request->get('height'), (int) $request->get('x'), (int) $request->get('y'))
            ->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save(public_path($this->path_imagenes.$imagen->nombre));

        return response()->json(['status' => 'success'], 200);
    }
    // guardar evento (AJAX)
    public function saveEvento(CrearEventoRequest $request)
    {
        \DB::beginTransaction();

        $evento = new $this->evento;
        $evento->fill([
            'nombre' => $request->get('nombre'),
            'slug' => $request->get('nombre'),
            'descripcion' => $request->get('descripcion'),
            'terminos' => $request->get('terminos'),
            'direccion' => $request->get('direccion'),
            'lugar' => $request->get('lugar'),
            'web' => $request->get('web'),
            'fecha_inicio' => $request->get('fecha_inicio'),
            'fecha_fin' => $request->get('fecha_fin'),
            'precio' => $request->get('precio'),
            'descuento' => $request->get('descuento'),
            'fechadesc' => $request->get('fechadesc'),
            //'longitud' => $request->get('longitud'),
            //'latitud' => $request->get('latitud'),
            'id_aliado' => $request->get('id_aliado'),
            'id_estado' => 2,
            'id_categoria' => $request->get('id_categoria'),
            'id_imagen' => $request->get('id_imagen'),
            'id_ciudad' => $request->get('id_ciudad')
        ]);

        if ($evento->save()) {

            if (count($request->get('fecha')) > 0) {
                foreach ($request->get('fecha') as $i => $fecha) {
                    $horario = new $this->horario;

                    $horario = $horario->fill([
                        'fecha' => $fecha,
                        'hora' => $request->get('hora')[$i],
                        'id_evento' => $evento->id
                    ]);

                    $horario->save();
                }
            }

            \DB::commit();

            // enviar correo
            \Mail::send('email.evento_nuevo', compact('evento'), function ($message)
            {
                $message->to('contacto@sihayparahacer.com', 'Admistrador');
                $message->cc('sihayparahacer@gmail.com', 'Admistrador');
                $message->subject('Nuevo Evento');
            });


            return response()->json(['state' => 'success', 'data' => $evento, 'message' => 'El evento ha sido enviado con exito.']);
        }
    }

       

     public function update(Request $request)
    {

        
        $evento = $this->evento->findOrFail($request->get('id'));
        $evento->fill([
            'nombre' => $request->get('nombre'),
            'slug' => $request->get('nombre'),
            'descripcion' => $request->get('descripcion'),
            'terminos' => $request->get('terminos'),
           
        ]);
            
        $evento->save();

        return response()->json(['state' => 'success', 'data' => $evento, 'message' => 'el evento fue creado']);
    }
    
}
