<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use Shph\Codigo;
use Shph\Aliado;
use Shph\DepartamentoRegion;
use Shph\EventoCategoria;
use App\Http\Controllers\Controller;

class CodigosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $codigo,$region,$aliado;
    
    public function __construct(Codigo $codigo,
       EventoCategoria $categoria,
       DepartamentoRegion $region,
       Aliado $aliado
       )
    {   
       parent::__construct($categoria, $region);             
        $this->codigo =$codigo;
        $this->categoria = $categoria;
    }
    public function getCodigo()
    {
        $codigos= $this->codigo->paginate();

        return view('front.aliado.codigo', compact('codigos'));
    }

    public function getVista()
    {
        return view('front.aliado.create');
    }

    public function postCodigos(Request $request)
    {   
        
        $codigo = new $this->codigo;

        $codigo->fill([
            'nombre'=> $request->get('nombre'),
            'valor'=> $request->get('valor'),
            'tipo'=> 'descuento',
            'estado'=>'activo',
            'id_aliado'=>\Auth::user()->aliado->id
        ]);
        $codigo->save();
        
        return redirect()->route('aliado_dashboard')->with('msj_success', 'Tu Codigo fue Creado extiosamente!');

    }
    public function getEditar($id)
    {
        $codigo= $this->codigo->findOrFail($id);
        return view ('front.aliado.editar',compact('codigo'));
    }
    public function postActualizar(Request $request, $id)
    {
        $codigo= $this->codigo->findOrFail($id);

        $codigo->fill([
            'nombre'=> $request->get('nombre'),
            'valor'=> $request->get('valor'),
            'tipo'=> 'descuento',
            'estado'=>'activo',
            'id_aliado'=>\Auth::user()->aliado->id
        ]);
        
        $codigo->save();
        return redirect()->route('aliado_dashboard')->with('msj_success', 'Tu Codigo fue Editado!');
    }

    
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
