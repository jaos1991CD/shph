<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Shph\Aliado;
use Shph\AliadoContacto;
use Shph\DepartamentoCiudad;
use Shph\DepartamentoRegion;
use Shph\Evento;
use Shph\EventoCategoria;
use Shph\EventoHorario;
use Shph\EventoImagen;
use Shph\Usuario;
use Shph\EventoEstado;
use Shph\Codigo;
use Illuminate\Support\Str;

use App\Http\Requests\Front\RegisterAliadoRequest;
use App\Http\Requests\Front\EditAliadoRequest;


class AliadoController extends Controller
{

	protected $aliado, $contacto, $ciudad, $imagen, $evento, $usuario, $horario, $estados, $categoria, $codigo;
	protected $path_imagenes = '/front/img/eventos/';

	public function __construct(
		Aliado $aliado,
		AliadoContacto $contacto,
		DepartamentoCiudad $ciudad,
		Evento $evento,
		EventoEstado $estados,
		DepartamentoRegion $region,
		EventoCategoria $categoria,
		EventoHorario $horario,
		EventoImagen $imagen,
		Usuario $usuario,
		Codigo $codigo
		)
	{
		parent::__construct($categoria, $region);
		$this->aliado = $aliado;
		$this->ciudad = $ciudad;
		$this->contacto = $contacto;
		$this->evento = $evento;
		$this->horario = $horario;
		$this->imagen = $imagen;
		$this->usuario = $usuario;
		$this->estado = $estados;
		$this->categoria = $categoria;
		$this->codigo = $codigo;
	}

	public function dashboard()
	{

		$eventos = \Auth::user()->aliado->eventos()->paginate();
		return view('front.aliado.dashboard', compact('eventos'));
	}


	public function getEdit($id)
	{
		try {
            $evento = $this->evento->findOrFail($id);
            $aliados = $this->aliado->getList();
            $categorias = $this->categoria->getList();
            $ciudades = $this->ciudad->getList();
            $estados = $this->estado->getList();

            return view('front.aliado.edit', compact('evento', 'aliados', 'ciudades', 'categorias', 'estados'));

        } catch (ModelNotFoundException $e) {
            return abort(404, 'Registro no encontrado');
        } 
	}

	public function actualizarEvento(Request $request, $id)
	{
			// return $request->all();
        try {
            //\DB::beginTransaction();

            $evento = $this->evento->findOrFail($id);
            $estado = $evento->id_estado;

            $evento->fill([
                'nombre' => $request->get('nombre'),
                //tomar nombre para el slug
                'slug'=> Str::slug($request->get('nombre')),
                'descripcion' => $request->get('descripcion'),
                'terminos' => $request->get('terminos'),
                'direccion' => $request->get('direccion'),
                'lugar' => $request->get('lugar'),
                'web' => $request->get('web'),
                'fecha_inicio' => $request->get('fecha_inicio'),
                'fecha_fin' => $request->get('fecha_fin'),
                'precio' => $request->get('precio'),
                'descuento' => $request->get('descuento'),
                'fechadesc' => $request->get('fechadesc'),
                //'tipo' => $request->get('tipo'),
                //'codigo' => $request->get('codigo'),
                //'enlace' => $request->get('enlace'),
                
            ]);
            
            
            
            if ($evento->save()) {


                if ($evento->horarios()->delete()) {
                    // Horario
                    if (count($request->get('fecha')) > 0) {
                        foreach ($request->get('fecha') as $i => $fecha) {
                            $horario = new $this->horario;

                            $horario = $horario->fill([
                                'fecha' => $fecha,
                                'hora' => $request->get('hora')[$i],
                                'id_evento' => $evento->id
                            ]);

                            $horario->save();
                        }
                    }
                }

                ;
                return redirect()->route('aliado_dashboard');
            }

        } catch (ModelNotFoundException $e) {
            return abort(404, 'Registro no encontrado');
        }
	}
	

	public function perfil()
	{
		$ciudades = $this->ciudad->getList();
		$aliado = \Auth::user()->aliado;
		return view('front.aliado.perfil', compact('ciudades', 'aliado'));
	}

	public function patchPerfil(EditAliadoRequest $request, $id)
	{
		try {
			$aliado = $this->aliado->findOrFail($id);
			$aliado->fill([
				'nombre' => $request->get('nombre_aliado'),
				'direccion' => $request->get('direccion_aliado'),
				'web' => $request->get('web_aliado'),
				'id_ciudad' => $request->get('id_ciudad_aliado')
			]);

			if ($aliado->save()) {
				$aliado->contacto->fill([
					'nombre' => $request->get('nombre_contacto'),
					'telefono' => $request->get('telefono_contacto')
				]);

				if ($aliado->contacto->save()) {
					$aliado->usuario->fill([
						'password' => $request->get('password'),
						'tipo' => 'aliado',
						'estado' => 'inactivo'
					]);

					if ($request->has('password')) {
						$aliado->usuario->password = $request->get('password');
					}

					if ($aliado->usuario->save()) {
						return redirect()->route('aliado_perfil')->with('msj_success', 'Tu perfil ha sido actualizado exitosamente!');
					}
				}
			}
		} catch (ModelNotFoundException $e) {
			abort(404, 'Página no encontrada');
		}
	}

	public function getRegister()
	{
		$ciudades = $this->ciudad->getList();

		return view('front.auth.register-aliado', compact('ciudades'));
	}

	public function postRegister(RegisterAliadoRequest $request)
	{
		$aliado = new $this->aliado;

		$aliado->fill([
			'nombre' => $request->get('nombre_aliado'),
			'direccion' => $request->get('direccion_aliado'),
			'web' => $request->get('web_aliado'),
			'estado' => 'activo',
			'id_ciudad' => $request->get('id_ciudad_aliado')
		]);

		if ($aliado->save()) {
			$contacto = new $this->contacto;

			$contacto->fill([
				'nombre' => $request->get('nombre_contacto'),
				'email' => $request->get('email_contacto'),
				'telefono' => $request->get('telefono_contacto'),
				'id_aliado' => $aliado->id
			]);

			if ($contacto->save()) {

                // crear usuario para este aliado
				$usuario = new $this->usuario;

				$usuario->fill([
					'correo' => $request->get('email_contacto'),
					'password' => $request->get('password'),
					'confirmation_token' => str_random(40),
					'tipo' => 'aliado',
					'estado' => 'inactivo'
				]);

				$url = route('confirmation', ['token' => $usuario->confirmation_token]);
				$nombre = $contacto->nombre;

				if ($usuario->save()) {
					$aliado->id_usuario = $usuario->id;
					$aliado->save();

					// enviar correo de confirmacion
					\Mail::send('email.registration', compact('nombre', 'url'), function ($m) use ($contacto) {
						$m->to($contacto->email, $contacto->nombre)->subject('Activa tu cuenta');
					});

					return redirect()->route('login')->with('msj_success', 'Al tu correo enviamos un mensaje de confirmación.');
				}
			}
		}
		return response()->json(['state' => 'error', 'message' => 'Ocurrió un error, por favor comuniquese con el administrador del sitio.', 'data' => null], 200);
	}

	

}
