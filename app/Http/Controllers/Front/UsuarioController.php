<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

use Shph\Suscripcion;
use Shph\Usuario;
use Shph\Departamento;
use Shph\EventoCategoria;
use Shph\DepartamentoRegion;
use Carbon\Carbon;

use App\Http\Requests\Front\EditarPerfilRequest;

class UsuarioController extends Controller
{

	protected $usuario, $departamento, $categoria, $region,$suscripcion;
	protected $path_imagenes = '/front/img/usuarios/';

	public function __construct(Usuario $usuario,
        DepartamentoRegion $region,
		Departamento $departamento,
		EventoCategoria $categoria,
        Suscripcion $suscripcion)
	{
		parent::__construct($categoria, $region);
        $this->usuario = $usuario;  
        $this->departamento = $departamento;
        $this->suscripcion=$suscripcion;
    }

    public function getPerfil()
    {   
        $usuario = \Auth::user();

        $departamentos = $this->departamento->getList();
        $generos = ['' => 'Seleccione Uno', 'm' => 'Masculino', 'f' => 'Femenino'];

        return view('front.perfil', compact('usuario', 'departamentos', 'generos'));
    }

    public function postPerfil(EditarPerfilRequest $request)
    {
        $usuario = \Auth::user();
        $perfil = $usuario->perfil;

        \DB::beginTransaction();

        $usuario->correo = $request->get('email');

        if ($request->has('password')) {
        	$usuario->password = $request->get('password');
        }

        if ($usuario->save()) {
        	$perfil->fill([
        		'nombres' => $request->get('nombres'),
        		'apellidos' => $request->get('apellidos'),
        		'genero' => $request->get('genero'),
        		'fecha_nacimiento' => Carbon::create($request->get('year', $request->get('month'), $request->get('day')))->toDateTimeString(),
        		'id_ciudad' => $request->get('id_ciudad')
        	]);

        	if ($perfil->save()) {
        		return redirect()->route('front_perfil')->with('msj_success', 'Su perfil ha sido actualizado');
        	}
        }
        return redirect()->route('front_perfil')->with('msj_error', 'Ocurrió un error al tratar de actualizar su perfil, por favor intentelo más tarde.');
    }

    public function postSaveImagen(Request $request)
    {
        $usuario = \Auth::user();
        $perfil = $usuario->perfil;

    	$nombre = Str::slug($perfil->nombres." ".$perfil->apellidos." ".str_random(5));

        $archivo = $request->file('imagen');

        $nombre_imagen = $nombre.'.'.$archivo->getClientOriginalExtension();
        $destino = public_path().$this->path_imagenes;
        $archivo->move($destino, $nombre_imagen);
        $url = url($this->path_imagenes.$nombre_imagen);


        $perfil->imagen = $nombre_imagen;

        if ($perfil->save()) {
            return response()->json(['state' => 'success', 'message' => 'Archivo Cargado', 'imagen' => url('front/img/usuarios/'.$perfil->imagen)], 200);
        }

        return response()->json(['state' => 'error', 'message' => 'Ocurrió un error.', 'data' => null], 200);
    }

    public function postCropImagen(Request $request)
    {
    	$usuario = \Auth::user();
        $perfil = $usuario->perfil;

        $img = \Image::make(url($this->path_imagenes.$perfil->imagen))
            ->crop((int) $request->get('width'), (int) $request->get('height'), (int) $request->get('x'), (int) $request->get('y'))
            ->save(public_path($this->path_imagenes.$perfil->imagen));

        return response()->json(['status' => 'success', 'imagen' => url('front/img/usuarios/'.$perfil->imagen)], 200);
    }
}
