<?php

namespace App\Http\Controllers\Auth;



use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests\Front\RegisterRequest;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Shph\Departamento;
use Shph\DepartamentoRegion;
use Shph\EventoCategoria;
use Shph\Usuario;
use Shph\UsuarioOAuth;
use Shph\UsuarioPerfil;

class PasswordController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */


    use ResetsPasswords;
    protected $usuario;
    public function __construct(
        Usuario $usuario,
        Guard $auth, 
        Departamento $departamento,
        PasswordBroker $passwords, 
        DepartamentoRegion $region,
        EventoCategoria $categoria
       
        )
    {
        parent::__construct($categoria, $region);
        $this->auth = $auth;
        $this->passwords = $passwords;
        $this->usuario = $usuario;

        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEmail()
    {
        return view('front.auth.password');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {   
       

        $rules = [
            'correo' => 'required|email|exists:usuario,correo',
        ];

        $validator = \Validator::make($request->all(), $rules);

        // Si falla la validacion
        if ($validator->fails()) {
            return redirect()->route('password.email')->withErrors($validator->errors())->withInput();
        }

        $usuario = $this->usuario->where('correo', $request->get('correo'))->first();
        $usuario->token = str_random(80);
        $usuario->save();
           

            $data = [
            'link' => route('password.reset', ['token' => $usuario->token, 'correo' => $usuario->correo]),
            'usuario' => $usuario
        ];

        \Mail::send('email.password', $data, function ($message) use ($usuario) {
            $message->to($usuario->correo)->subject('Reestablecer contraseña');
        });
        return redirect()->route('password.email')->with('msj_success', 'Hemos enviado a tu correo un enlace para que puedas restablecer tu contraseña.');
        
    }

  
    public function getReset(Request $request)
    {
        if ($request->has('token')) {
            return view('front.auth.reset', ['token' => $request->get('token'), 'correo' => $request->get('correo')]);
        }
        return 'El token para cambiar el password de su cuenta no es correcto, por favor vuelva a intentar abrir desde la aplicación.';
        
   }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {

        $rules = [
            'correo' => 'required',
            'password' => 'required|confirmed|min:6',
        ];
         $validator = \Validator::make($request->all(), $rules);

        // Si falla la validacion
        if ($validator->fails()) {
            return redirect()->route('password.email', ['token' => $request->get('token'), 'correo' => $request->get('correo')])->withErrors($validator->errors())->withInput();
        }

      $datos = $request->all();
      
        $usuario = $this->usuario
            ->where('token', 'LIKE', $datos['token'])
            ->first();

        if (!is_null($usuario)) {
            $usuario->password = $request->get('password');
            $usuario->token = null;

            if ($usuario->save()) {
                return redirect()->route('password.email.post')->with('msj_success', '¡Hemos restablecido su contraseña! Gracias por usar Si Hay Para Hacer.');
            }
        }

        return redirect()->route('password.reset', ['token' => $request->get('token'), 'correo' => $request->get('correo')])->with('msj_error', 'Su token ha expirado, por favol intente recuperar su contraseña de nuevo.');
    
    }
    
}
