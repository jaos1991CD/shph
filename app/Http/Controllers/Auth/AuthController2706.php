<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Front\RegisterRequest;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Contracts\Factory as Socialite;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Shph\Departamento;
use Shph\DepartamentoRegion;
use Shph\EventoCategoria;
use Shph\Usuario;
use Shph\Suscripcion;
use Shph\UsuarioOAuth;
use Shph\UsuarioPerfil;
use Shph\Transaccion;
use Shph\Codigo;
use App\RegistroPago;
use App\UsuarioBuscar;
use App\BuscarCodigo;
use App\BuscarDescuento;

class AuthController extends Controller
{
	protected $usuario, $session, $perfil;
	protected $departamento, $categoria, $region, $suscripcion,$transaccion,$codigo;


	public function __construct(
		Departamento $departamento,
		DepartamentoRegion $region,
		EventoCategoria $categoria,
		SessionManager $session, 
		Socialite $socialite,
		Usuario $usuario,
		Suscripcion $suscripcion,
		UsuarioOAuth $oauth,
		UsuarioPerfil $perfil,
		Transaccion $transaccion,
		Codigo $codigo
		)
	{
		parent::__construct($categoria, $region);
		$this->departamento = $departamento;
		$this->oauth = $oauth;
		$this->perfil = $perfil;
		$this->session = $session;
		$this->socialite = $socialite;
		$this->usuario = $usuario;
		$this->suscripcion= $suscripcion;
		$this->transaccion=$transaccion;
		$this->codigo= $codigo;

		$this->middleware('guest', ['except' => ['getLogout', 'getPagos', 'postPagos', 'postPagosUsuarioSuscripcion','confirmCodigo', 'postMembresia']]);
	}

	public function getLogin()
	{
		
		\Session::put('url.intended',\URL::previous());
		return view('front.auth.login');
	}

	public function postLogin(Request $request)
	{
		
		$this->validate($request, [
			'correo' => 'email|required',
			'password' => 'required'
		]);

		$credentials = $request->only('correo', 'password');

		if (Auth::attempt($credentials, $request->has('rememberme'))) {
			// verificar si el usuario esta activo
			if ( ! is_null(Auth::user()->confirmation_token)) {
				Auth::logout();
				return redirect()->route('login')->with('msj_error', 'Por favor confirme su correo electrónico.');
			}

			if (Auth::user()->estado == 'inactivo') {
				Auth::logout();
				return redirect()->route('login')->with('msj_error', 'Su usuario se encuentra inactivo, por favor comuniquese con el administrador.');
			}

			// Redirecciona administrador
			if (Auth::user()->tipo == 'admin') {
				return redirect()->intended('administrador/');
			}else{
				//return redirect()->intended('/');
				return \Redirect::to(\Session::get('url.intended'));

			}
		
		}
		return redirect()->route('login')
			->withInput($request->only('correo', 'rememberme'))
			->withErrors([
				'correo' => 'Estas credenciales no coinciden con nuestros registros.',
			]);
		
	}

	public function getLogout()
	{
		Auth::logout();
		return redirect('/');
	}

	public function getSocialLogin($provider = null)
	{
		if(!config("services.$provider")) abort('404');
		return $this->socialite->with($provider)->redirect();
	}

	public function getSocialCallback(Request $request, $provider)
	{
		if($user = $this->socialite->with($provider)->user()){

			$usuario = $this->usuario->where('correo', $user->email)->first();

			if (is_null($usuario)) {
				//Crear registro
				$usuario = new $this->usuario;
				$usuario->fill([
					'correo' => $user->email,
					'password' => '123456',
					'tipo' => 'user',
					'estado' => 'activo'
				]);

				if ($usuario->save()) {
					// obtener imagen y guardarla en el servidor
					$nombre = Str::slug($user->user['first_name']." ".$user->user['last_name']." ".str_random(5)).'.jpg';
					copy($user->avatar_original, public_path('front/img/usuarios/'.$nombre));

					$perfil = new $this->perfil;
					$perfil->fill([
						'nombres' => $user->user['first_name'],
						'apellidos' => $user->user['last_name'],
						'genero' => ($user->user['gender'] == 'male') ? 'm' : 'f',
						'fecha_nacimiento' => '1990-01-01',
						'imagen' => $nombre,
						'id_usuario' => $usuario->id,
						'id_ciudad' => 1
						]);

					if ($perfil->save()) {
						$oauth = new $this->oauth;
						$oauth->fill([
							'provider' => $provider,
							'provider_id' => $user->id,
							'id_usuario' => $usuario->id
						]);
						$oauth->save();
					}
				}
			}
			// Iniciar Sesion
			if (Auth::loginUsingId($usuario->id, true)) {
				// verificar si el usuario tiene imagen
				$perfil = $usuario->perfil;
				if (is_null($perfil->imagen)) {
					// obtener imagen y guardarla en el servidor
					$nombre = Str::slug($user->user['first_name']." ".$user->user['last_name']." ".str_random(5)).'.jpg';
					copy($user->avatar_original, public_path('/front/img/usuarios/'.$nombre));

					$perfil->imagen = $nombre;
					$perfil->save();
				}

				return redirect()->intended('/')->with('success', true)->with('mensaje', 'Se ha registrado exitosamente, disfrute de su suscripción gratis.');
			}

			return redirect('auth/login')->withErrors(['msj_error' => 'No pudimos procesar tu información.']);
		}else{
			return 'something went wrong';
		}
	}

	public function getSuscribirse()
	{
		return view('front.auth.suscribirse');
	}

	public function getRegister()
	{
		$generos = ['' => 'Seleccione Uno', 'm' => 'Masculino', 'f' => 'Femenino'];
		$departamentos = $this->departamento->getList();
		$meses = [1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre'];
		
		return view('front.auth.register', compact('meses', 'departamentos', 'generos'));
	}

	public function postRegister(RegisterRequest $request)
	{
		$usuario = new $this->usuario;
			 
	   $acepto= \Input::has('acepto');
	   
		$usuario->fill([
			'correo' => $request->get('email'),
			'password' => $request->get('password'),
			'tipo' => 'user',
			'confirmation_token' => str_random(40),
			'estado' => 'inactivo',
			'acepto' => $acepto
		]);
		
		$url = route('confirmation', ['token' => $usuario->confirmation_token]);

		if ($usuario->save()) {

			$perfil = new $this->perfil;

			$perfil->fill([
				'nombres' => $request->get('nombres'),
				'apellidos' => $request->get('apellidos'),
				'genero' => $request->get('genero'),
				'fecha_nacimiento' => $request->get('fecha_nacimiento'),
				'id_usuario' => $usuario->id,
				'id_ciudad' => $request->get('id_ciudad')
			]);

			if ($perfil->save()) {
				$nombre = $perfil->nombre_completo;

				// enviar correo de confirmacion
				\Mail::send('email.registration', compact('nombre', 'url'), function ($m) use ($usuario)
				{
					$m->to($usuario->correo, $usuario->perfil->nombre_completo)->subject('Activa tu cuenta');
				});

				// enviar al login con el mensaje
				return redirect()->route('vista_pagos', ['id' => $usuario->id])->with('msj_success', 'A tu correo enviamos un mensaje de confirmación. Suscribite para disfrutar de los beneficios, recuerda usar la misma contraseña con la que te registraste ');
			}
		}
	}

	public function getPagos(Request $request, $id)
	{ 	
		$profileid	= env('PROFILE_ID');
		$acceskey	=	env('ACCESS_KEY');
		$secretkey	=	env('SECRET_KEY');

		$referpay = rand(1,9999999999999);
		$referpay = rand(1,9999999999999);

		$consultaref = RegistroPago::where('referencia',$referpay)->orderBy('id','desc')->first();

		if($consultaref['id'] != null){

			$referpay = rand(1,9999999999999);
			$referpay = rand(1,9999999999999);

			$consultaid = RegistroPago::where('id_usuario',$id)->orderBy('id','desc')->first();

			$reference = new RegistroPago;
										
			DB::table('usuario_suscripcion') ->where('id',$consultaid['id']) ->update(array('referencia'=> $referpay)); //Diligencia la informacion de referencia de pago para el formulario

			$reference->save();
		}else{

			$consultaid = RegistroPago::where('id_usuario',$id)->orderBy('id','desc')->first();

			$reference = new RegistroPago;

														
			DB::table('usuario_suscripcion') ->where('id',$consultaid['id']) ->update(
				array('referencia'=> $referpay)); //Diligencia la informacion de referencia de pago para el formulario

			$reference->save();

		}


		$suscripciones = $this->suscripcion->select('id', 'descripcion', 'valor')->where('id', '!=', 4)->get();
		return view('front.auth.pagos',compact('suscripciones', 'id','profileid', 'acceskey', 'secretkey', 'referpay'));
	}

	public function postMembresia(Request $request)
	{ 
		//modulo de fechas
		$inicio = Carbon::now();
		$inicio = $inicio->format('Y-m-d');

		$fin= Carbon::now()->addmonth(1);

		//fin fechas

		$consulta = RegistroPago::where('id_usuario',$request->get('id_usuario'))->orderBy('id','desc')->first();

		$codigo = BuscarDescuento::where('id',$consulta['id_codigo'])->orderBy('id','desc')->first();//informacion de los codigos de descuento-Consulta tabla aliado_codigo

		$usuario = new RegistroPago;

														
			DB::table('usuario_suscripcion') ->where('id',$consulta['id']) ->update(array(
				'fecha_inicio'=> $inicio,
				'fecha_vencimiento'=>$fin,
				'estado'=>'activo',
				'fechaexp'=>$fin,
				'valor'=>$request->get('amount'),
				'created_at'=>'0000-00-00 00:00:00',
				'updated_at'=>'0000-00-00 00:00:00')); //Diligencia la informacion necesaria para el ALTA en la plataforma.

			DB::table('aliado_codigo') ->where('id',$codigo['id']) ->update(array('estado'=>'inactivo')); //Inactiva el codigo de descuento ya utilizado

			$usuario->save();

			//return response()->json(['state' => 'success', 'data' => $usuario, 'message' => 'Su pago se realizó satisfactoriamente, ahora puede disfrutar de nuestros beneficios']);

			return redirect()->route('login')->with('msj_success', 'Su pago se realizó satisfactoriamente');

	}

	public function postPagos(Request $request)
	{ 
		//modulo de fechas
		$inicio = Carbon::now();
		$inicio = $inicio->format('Y-m-d');

		$fin= Carbon::now()->addyear(1);

		//fin fechas

		try{

													//$request->get(req_bill_to_email) 
			//$results = UsuarioBuscar::where('correo',$request->get('req_bill_to_email'))->first();//informacion del usuario-Consulta tabla Usuario

			//$suscripcion = $results->suscripcion()->where('usuario_suscripcion.estado', 'en_proceso')->orderBy('id', 'desc')->first();
													 //$consulta['id']
			$consulta2 = RegistroPago::where('referencia',$request->get('req_reference_number'))->orderBy('id','desc')->first();//informacion de pago de suscripcion del usuario-Consulta tabla usuario_suscripcion
		
			$estado=$request->get('message');//$request->get('message')

			//Revision de Codigo y Valor esperado a pagar
			$suscripcion = BuscarCodigo::where('id',$consulta2['id_suscripcion'])->orderBy('id','desc')->first();//informacion del plan de suscripcion- Consulta tabla suscripcion
			$codigo = BuscarDescuento::where('id',$consulta2['id_codigo'])->orderBy('id','desc')->first();//informacion de los codigos de descuento-Consulta tabla aliado_codigo
			
			if(! is_null($consulta2['id_codigo'])){

				$total = $suscripcion['valor']-(($suscripcion['valor']*$codigo['valor'])/100);

			}else{

				$total = $suscripcion['valor'];

			}
												
			if ($request->get('req_amount') == $total) {

				if($estado =='Aprobada'){

					$usuario = new RegistroPago;
																
					DB::table('usuario_suscripcion') ->where('id',$consulta2['id']) ->update(array('fecha_inicio'=>$inicio,
					'fecha_vencimiento'=>$fin,
					'estado'=>'activo',
					'token'=>$request->get('request_token'),//$request->get('request_token'),
					'idTransaccion'=>$request->get('transaction_id'),//$request->get('transaction_id'),
					'numeros'=>$request->get('req_card_number'),//$request->get('req_card_number'),
					'fechaexp'=>$fin,
					'idTransaccion'=>$request->get('transaction_id'),//$request->get('transaction_id'),
					'idEstado'=>$request->get('reason_code'),//$request->get('reason_code'),
					'nombreEstado'=>$request->get('decision'),//$request->get('decision'),
					'referencia'=>$request->get('req_reference_number'),//$request->get('req_reference_number'),
					'codigoRespuesta'=>$request->get('reason_code'),//$request->get('reason_code'),
					'codigoAutorizacion'=>$request->get('auth_code'),//$request->get('auth_code'),
					'riesgo'=>'0',
					'valor'=>$request->get('req_amount'),//$request->get('req_amount'),
					'iva'=>$request->get('req_merchant_defined_data5'),//$request->get('req_merchant_defined_data5'),
					'baseDevolucion'=>$request->get('req_merchant_defined_data6'),//$request->get('req_merchant_defined_data6'),
					'fechaProcesamiento'=>$request->get('signed_date_time'),//$request->get('signed_date_time'),
					'mensaje'=>$request->get('message'),//$request->get('message'),
					'created_at'=>'0000-00-00 00:00:00',
					'updated_at'=>'0000-00-00 00:00:00'));

					DB::table('aliado_codigo') ->where('id',$codigo['id']) ->update(array('estado'=>'inactivo'));

					$usuario->save();

					//$usuario = RegistroPago::get();
			    	//dd($usuario);
					
					//return response()->json(['state' => 'success', 'data' => $usuario, 'message' => 'Su pago se realizó satisfactoriamente, ahora puede disfrutar de nuestros beneficios']);

			    	return redirect('perfil')->with('msj_success', 'Su pago se realizó satisfactoriamente'); 
			    }
			}else{
				// devolver pago y mostrar el mensaje de error al usuario.
				// consultar con jimmy
				return redirect('perfil')->with('msj_error', 'Ocurrió un error con su transacción.');
			}

			if(Auth::loginUsingId($usuario->id, true)){

							return redirect('login');
						}

		}catch (ModelNotFoundException $e) {
				return ['No se encontro el usuario'];
		}   

	} 
		//hASTA AQUI TODO SIRVE Y ES LA LOGICA QUE GUARDA EN LA BD, SE PONE ASI PARA PROBAR LA OBTENCION DE LAS VARIABLES
			
//De aqui para abajo nada sirve, solo para apoyo educativo
			/*
			$consulta = UsuarioSuscripcion::where('correo','jaos1991@gmail.com')->first();


			if($consulta == '366'){

					$prueba = new UsuarioSuscripcion;

					$prueba->fecha_inicio='2016-06-02';//$inicio ,
					$prueba->fecha_vencimiento= '2016-12-02';//$fin,
					$prueba->estado='activo';//$estado,
					$prueba->token='AhjzbwSR9/IKF4w2SeaYNgKXBCnGX+VRW3GwMBMMmkmWLr4G1TgJgAAAnhR9';//$request->get('request_token'),
					$prueba->numeros='xxxxxxxxxxxx1111';//$request->get('req_card_number'),
					$prueba->fechaexp= '2017-06-02';//$fechaexp,
					$prueba->idTransaccion='113070';//$request->get('transaction_id'),
					$prueba->idEstado= '100';//$request->get('reason_code'),
					$prueba->nombreEstado = 'ACCEPT';//$request->get('decision'),
					$prueba->referencia='1458325873677';//$request->get('req_reference_number'),
					$prueba->codigoRespuesta ='100';//$request->get('reason_code'),
					$prueba->codigoAutorizacion  = '831000';//$request->get('auth_code'),
					$prueba->valor= '15000.0';//$request->get('req_amount'), 
					$prueba->iva= '0.0';//$request->get('req_merchant_defined_data5'), 
					$prueba->baseDevolucion= '0.0';//$request->get('req_merchant_defined_data6'),
					$prueba->fechaProcesamiento= '2016-06-01T19:00:52Z';//$request->get('signed_date_time'),
					$prueba->mensaje= 'Aprobada';//$request->get('message')

					$prueba->save();

				return redirect()->route('login')->with('msj_success', 'Su pago se realizó satisfactoriamente');
			}else{
				// devolver pago y mostrar el mensaje de error al usuario.
				// consultar con jimmy
				return redirect()->route('login')->with('msj_error', 'Ocurrió un error con su transacción.');
			}*/

/*
		\DB::beginTransaction();

		try {											//$request->get(req_bill_to_email)
			$usuario 	= $this->usuario->where('correo', 'jaos1991@gmail.com')->firstOrFail();
			$suscripcion = $usuario->suscripcion()->where('usuario_suscripcion.estado', 'en_proceso')->orderBy('id', 'desc')->first();

				if ($usuario['id']=='366') {
					
					dd([$usuario, $suscripcion]);
				
			
					$codigo = $this->codigo->find($suscripcion->pivot->id_codigo);
					$total = ($suscripcion->valor * $codigo->valor) / 100;
					
								//$request->get('req_amount')
					if ($total == '15000') {
						if ( ! is_null($usuario['id'])) {
							if ( ! is_null($suscripcion['id'])) {

								$inicio = Carbon::now();
								$fin = Carbon::now()->addmonth($suscripcion->meses);
								$fechaexp = $inicio->diffInDays($fin);
								
								//$estado = "en_proceso";
								//
								//if ($request->input('message') == 'Aprobada') {
								$estado = 'activo';
								//}
								
								$codigo->estado='inactivo';
								$codigo->save();

								$usuario['id']->suscripcion()->updateExistingPivot($suscripcion->id,[
									'fecha_inicio' =>'2016-06-02',//$inicio ,
									'fecha_vencimiento' => '2016-12-02',//$fin,
									'estado'=>$estado,
									'token'=>'AhjzbwSR9/IKF4w2SeaYNgKXBCnGX+VRW3GwMBMMmkmWLr4G1TgJgAAAnhR9',//$request->get('request_token'),
									'numeros'=>'xxxxxxxxxxxx1111',//$request->get('req_card_number'),
									'fechaexp'=> $fechaexp,
									'idTransaccion'=>'113070',//$request->get('transaction_id'),
									'idEstado'=> '100',//$request->get('reason_code'),
									'nombreEstado' => 'ACCEPT',//$request->get('decision'),
									'referencia'=> '1458325873677',//$request->get('req_reference_number'),
									'codigoRespuesta' => '100',//$request->get('reason_code'),
									'codigoAutorizacion'  => '831000',//$request->get('auth_code'),
									'valor'=> '15000.0',//$request->get('req_amount'), 
									'iva'=> '0.0',//$request->get('req_merchant_defined_data5'), 
									'baseDevolucion'=> '0.0',//$request->get('req_merchant_defined_data6'),
									'fechaProcesamiento'=> '2016-06-01T19:00:52Z',//$request->get('signed_date_time'),
									'mensaje'=> 'Aprobada',//$request->get('message')
								], true);

								return redirect()->route('login')->with('msj_success', 'Su pago se realizó satisfactoriamente');
							}

						}
						\DB::commit();
					}else{
						// devolver pago y mostrar el mensaje de error al usuario.
						// consultar con jimmy
						return redirect()->route('login')->with('msj_error', 'Ocurrió un error con su transacción.');
					}
						
					if(Auth::loginUsingId($usuario->id, true)){
						return redirect()->route('login');
					}
				}else{
					return ['No paso nada'];
				}
			}
			

		catch (ModelNotFoundException $e) {
			return ['No se encontro el usuario'];
		}
		
		

	
		//return redirect()->route('login')->with('msj_success', 'Su pago se realizó satisfactoriamente');
		
		/* $usuario->suscripcion->save();*/
		/*return view('front.auth.pagos',compact('meses', 'departamentos', 'generos','suscripciones','usuario','perfil'));*/

		/* return view('front.auth.pagos',compact('meses', 'departamentos', 'generos','suscripciones','usuario','perfil'));
		  "req_card_number" => "xxxxxxxxxxxx1111"
		  "signed_field_names" => "signed_field_names,req_amount,req_merchant_defined_data3,req_merchant_defined_data5,req_merchant_defined_data6,transaction_id,decision,req_profile_id,req_transaction_uuid,req_transaction_type,req_reference_number,req_currency,reason_code,message,req_bill_to_forename,req_bill_to_surname,req_bill_to_email,req_bill_to_phone,req_bill_to_address_line1,req_bill_to_address_city,req_bill_to_address_country,decision"
		  "req_transaction_type" => "create_payment_token"
		  "req_payment_method" => "card"
		  "signature" => "v48Q4fGxPDI+FegZqrkvoTryS8OGWCOskGZbUHpg0zU="
		  "request_token" => "AhjzbwSR8yX5INGCnkfQNgKXC0t18AVRa3GwF9aGTSTLF18DapwEwAAAgQOV"
		  "req_device_fingerprint_id" => "59c45b2909fbbdee541690741856"
		  "req_reference_number" => "1458325873677"
		   "req_merchant_secure_data1" => "bKjKs2ekzDDnKAiorjOTaA=="
		  "req_merchant_defined_data4" => "1"
		   "req_profile_id" => "E7D0F4D0-D1FE-4013-91F0-85DBB0D0FB16"
		  "req_transaction_uuid" => "5fa53d18-73dd-408d-8a3c-db721e1d137a"
		  "payment_token" => "4603830773556941104104"
		  "req_bill_to_email" => "roberto.arenas@creardigital.com"
		  "req_merchant_defined_data100" => "9623"
	}*/
	
	// verificar codigo de descuento
	public function confirmCodigo(Request $request)
	{
		$codigo = $this->codigo->where('nombre', $request->get('codigo'))->where('estado', 'activo')->first();
		
		if (is_null($codigo)) {
			return response()->json(['state' => 'error', 'data' => null, 'message' => 'Código no válido o ya utilizado, intente con otro']);
		}
		
		return response()->json(['state' => 'success', 'data' => $codigo, 'message' => 'Código es válido']);

	}
	public function postPagosUsuarioSuscripcion(Request $request)
	{
		try {
			$usuario = $this->usuario->find($request->get('usuario'));
			if($request->get('codigo') == 0){
				$id_codigo=null;
				$usuario->suscripcion()->attach($request->get('suscripcion'));
			}else{

			$usuario->suscripcion()->attach($request->get('suscripcion'), ['id_codigo' => $request->get('codigo')]);
			}
			return response()->json(['state' => 'success']);
		} catch (Exception $e) {
			return response()->json(['state' => 'error', 'message' => 'Ocurrió un error en el sistema, intentelo más tarde.']);
		}
	}


	 public function getRespuesta()
	{   

		return view('front.auth.pagos');
	}

	public function postRespuesta(Request $request)

	{
		if($request->isMethod('post'))
		{
	   
		}
		
	}

	public function getConfirmation($token)
	{
		$usuario = $this->usuario->where('confirmation_token', $token)->firstOrFail();
		$usuario->confirmation_token = null;
		$usuario->estado = 'activo';
		if ($usuario->save()) {
			return redirect()->route('login')->with('msj_success', 'Has confirmado tu correo electrónico exitosamente, por favor inicia sesión.');
		}
	}
}
