<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Eventos\CrearRequest;
use App\Http\Requests\Eventos\EditarRequest;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Shph\Evento;
use Shph\EventoCategoria;
use Shph\EventoImagen;
use Shph\EventoHorario;
use Shph\Aliado;
use Shph\Beneficio;
use Shph\EventoEstado;
use Shph\EventoComentario;
use Shph\EventoCodigo;
use Shph\DepartamentoCiudad;

class EventosController extends Controller
{

    protected $evento, $aliado, $ciudad, $categoria, $estado, $imagen, $codigo, $comentario;
    protected $path_imagenes = '/front/img/eventos/';
    protected $temporal_path = '/temporal/';

    public function __construct(Evento $evento,
        Aliado $aliado,
        DepartamentoCiudad $ciudad,
        
        EventoCategoria $categoria,
        EventoEstado $estado,
        EventoCodigo $codigo,
        EventoComentario $comentario,
        EventoHorario $horario,
        EventoImagen $imagen)
    {
        $this->evento = $evento;
        $this->aliado = $aliado;
        $this->ciudad = $ciudad;
        $this->categoria = $categoria;
        $this->estado = $estado;
        $this->imagen = $imagen;
        $this->codigo = $codigo;
        $this->horario = $horario;
        $this->comentario = $comentario;
        

    }

    public function index(Request $request)
    {

        $eventos = $this->evento->with('estado', 'categoria', 'aliado');

        if($request->has('nombre')){
            $eventos= $eventos->where('nombre', 'like', '%'.$request->get('nombre').'%');
        }

         if ($request->has('estado')) {
            $eventos = $eventos->where('id_estado', $request->get('estado'));
        }


        if($request->has('exportar')){
            // exportar excel
             $eventos = $eventos->get();
                
             \Excel::create('Reporte eventos', function($excel) use ($eventos) {
                $excel->setDescription('Información de los eventos de la aplicación');

                $excel->sheet('eventos', function($sheet) use ($eventos) {
                    $sheet->loadView('admin.reportes.eventos', compact('eventos'));
                });
            })->download('xlsx');
        }

        $eventos = $eventos->paginate();
        
        $estados = $this->estado->lists('nombre', 'id');
        return view('admin.eventos.index', compact('eventos', 'estados'));
    }

    public function create()
    {


        $aliados = $this->aliado->getList();
        $categorias = $this->categoria->getList();
        $ciudades = $this->ciudad->getList();
        $estados = $this->estado->getList();

        return view('admin.eventos.create', compact('aliados', 'ciudades', 'categorias', 'estados'));
    }

    public function store(CrearRequest $request)
    {
        //\DB::beginTransaction();
        $evento = new $this->evento;

        $evento->fill([
			'nombre' => $request->get('nombre'),
            'slug'=> Str::slug($request->get('nombre')),
			'descripcion' => $request->get('descripcion'),
			'terminos' => $request->get('terminos'),
			'direccion' => $request->get('direccion'),
			'lugar' => $request->get('lugar'),
			'web' => $request->get('web'),
			'fecha_inicio' => $request->get('fecha_inicio'),
			'fecha_fin' => $request->get('fecha_fin'),
			'precio' => $request->get('precio'),
            'descuento' => $request->get('descuento'),
            'fechadesc' => $request->get('fechadesc'),
			//'longitud' => $request->get('longitud'),
            //'latitud' => $request->get('latitud'),

            //'longitud' => null,
            //'latitud' => null,

            //'tipo' => $request->get('tipo'),
            'tipo' => 'general',
            'codigo' => 'SHPH',
			'enlace' => null,
			'id_aliado' => $request->get('id_aliado'),
			'id_estado' => $request->get('id_estado'),
			'id_categoria' => $request->get('id_categoria'),
			'id_imagen' => $request->get('id_imagen'),
            'id_ciudad' => $request->get('id_ciudad')
        ]);
            
        if ($evento->save()) {
            // Codigos Unicos
            /*if ($evento->tipo == 'unico') {
                // Generar codigos si se pidio
                if ($request->has('generar_codigos')) {
                    for ($i=0; $i <= $request->get('codigos_numero'); $i++) { 
                        $codigo = new $this->codigo;

                        $codigo->fill([
                            'nombre' => str_random($request->get('codigos_cantidad')),
                            'estado' => 'activo',
                            'id_evento' => $evento->id
                        ]);

                        $codigo->save();
                    }
                }
                // cargar codigos desde un excel
                if ($request->has('cargar_codigos')) {
                    $codigos = $request->file('codigos');
                    // subir el archivo
                    $nombre_archivo = str_random(20).'.'.$codigos->getClientOriginalExtension();
                    $destino = public_path().$this->temporal_path;
                    $codigos->move($destino, $nombre_archivo);
                    $archivo = $destino.$nombre_archivo;

                    // recorrer el excel
                    \Excel::load($archivo, function($reader) use ($evento) {
                        $results = $reader->get();
                        foreach ($results as $i => $row) {
                            $codigos = new $this->codigo;

                            $codigos->fill([
                                'nombre' => $row->codigos,
                                'estado' => 'activo',
                                'id_evento' => $evento->id
                            ]);

                            $codigos->save();
                        }
                    });
                    // eliminar archivo
                    if (\File::exists($archivo)) {
                        \File::delete($archivo);
                    }
                }
            }*/

            if (count($request->get('fecha')) > 0) {
                foreach ($request->get('fecha') as $i => $fecha) {
                    $horario = new $this->horario;

                    $horario = $horario->fill([
                        'fecha' => $fecha,
                        'hora' => $request->get('hora')[$i],
                        'id_evento' => $evento->id
                    ]);

                    $horario->save();
                }
            }

        	\DB::commit();
        	return redirect()->route('administrador.eventos.index')->with('msj_success', 'Evento creado con éxito.');
        }
        
    }

    public function postChangeEstado(Request $request, $id)
    {
        $evento = $this->evento->find($id);
        $estado = $evento->id_estado;
        $evento->id_estado = $request->get('id_estado');

        if ($evento->save()) {
            // enviar correo
            // Si se cambío el estado de inactivo a activo se le envia un correo al aliado
            if ($estado == 2 && $estado != $evento->id_estado) {
                \Mail::send('email.evento_estado', compact('evento'), function ($message) use ($evento)
                {
                    $message->to($evento->aliado->contacto->email, $evento->aliado->nombre);
                    $message->subject('Evento Activo');
                });
            }

            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'error', 'message' => 'Ocurrió un error, por favor comuniquese con el administrador']);
    }

    public function edit($id)
    {
        try {
            $evento = $this->evento->findOrFail($id);
            $aliados = $this->aliado->getList();
            $categorias = $this->categoria->getList();
            $ciudades = $this->ciudad->getList();
            $estados = $this->estado->getList();

            return view('admin.eventos.edit', compact('evento', 'aliados', 'ciudades', 'categorias', 'estados'));

        } catch (ModelNotFoundException $e) {
            return abort(404, 'Registro no encontrado');
        } 
    }

    public function update(EditarRequest $request, $id)
    {
        // return $request->all();
        try {
            //\DB::beginTransaction();

            $evento = $this->evento->findOrFail($id);
            $estado = $evento->id_estado;

            $evento->fill([
                'nombre' => $request->get('nombre'),
                //tomar nombre para el slug
                'slug'=> Str::slug($request->get('nombre')),
                'descripcion' => $request->get('descripcion'),
                'terminos' => $request->get('terminos'),
                'direccion' => $request->get('direccion'),
                'lugar' => $request->get('lugar'),
                'web' => $request->get('web'),
                'fecha_inicio' => $request->get('fecha_inicio'),
                'fecha_fin' => $request->get('fecha_fin'),
                'precio' => $request->get('precio'),
                'descuento' => $request->get('descuento'),
                'fechadesc' => $request->get('fechadesc'),
                //'tipo' => $request->get('tipo'),
                //'codigo' => $request->get('codigo'),
                //'enlace' => $request->get('enlace'),
                'id_aliado' => $request->get('id_aliado'),
                'id_estado' => $request->get('id_estado'),
                'id_categoria' => $request->get('id_categoria'),
                'id_ciudad' => $request->get('id_ciudad'),
                'id_imagen' => $request->get('id_imagen')
            ]);
            
            if ( $request->get('longitud') != '' &&  $request->get('latitud') != '') {
                $evento->longitud = $request->get('longitud');
                $evento->latitud = $request->get('latitud');
            }
            
            if ($evento->save()) {

                /*if ($evento->tipo == 'unico') {
                    // Generar codigos si se pidio
                    if ($request->has('generar_codigos')) {
                        for ($i=0; $i <= $request->get('codigos_numero'); $i++) { 
                            $codigo = new $this->codigo;

                            $codigo->fill([
                                'nombre' => str_random($request->get('codigos_cantidad')),
                                'estado' => 'activo',
                                'id_evento' => $evento->id
                            ]);

                            $codigo->save();
                        }
                    }

                    if ($request->has('cargar_codigos')) {
                        $codigos = $request->file('codigos');
                        // subir el archivo
                        $nombre_archivo = str_random(20).'.'.$codigos->getClientOriginalExtension();
                        $destino = public_path().$this->temporal_path;
                        $codigos->move($destino, $nombre_archivo);
                        $archivo = $destino.$nombre_archivo;

                        // recorrer el excel
                        \Excel::load($archivo, function($reader) use ($evento) {
                            $results = $reader->get();
                            foreach ($results as $i => $row) {
                                $codigos = new $this->codigo;

                                $codigos->fill([
                                    'nombre' => $row->codigos,
                                    'estado' => 'activo',
                                    'id_evento' => $evento->id
                                ]);

                                $codigos->save();
                            }
                        });
                        // eliminar archivo
                        if (\File::exists($archivo)) {
                            \File::delete($archivo);
                        }
                    }
                }*/

                if ($evento->horarios()->delete()) {
                    // Horario
                    if (count($request->get('fecha')) > 0) {
                        foreach ($request->get('fecha') as $i => $fecha) {
                            $horario = new $this->horario;

                            $horario = $horario->fill([
                                'fecha' => $fecha,
                                'hora' => $request->get('hora')[$i],
                                'id_evento' => $evento->id
                            ]);

                            $horario->save();
                        }
                    }
                }

                // enviar correo
                // Si se cambío el estado de inactivo a activo se le envia un correo al aliado
                if ($estado == 2 && $estado != $evento->id_estado) {
                    \Mail::send('email.evento_nuevo', compact('evento'), function ($message) use ($evento)
                    {
                        $message->to($evento->aliado->contacto->email, $evento->aliado->nombre);
                        $message->subject('Evento Activo');
                    });
                }

                //\DB::commit();
                return redirect()->route('administrador.eventos.index')->with('msj_success', 'El evento ha sido modificado con éxito.');
            }

        } catch (ModelNotFoundException $e) {
            return abort(404, 'Registro no encontrado');
        }
    }

    public function destroy($id)
    {
        try {
            // eliminar comentarios
            $comentarios = $this->comentario->where('id_evento', $id)->get();

            if ($comentarios->delete()) {
                $evento = $this->evento->findOrFail($id);

                if ($evento->delete()) {
                    return redirect()->route('administrador.eventos.index')->with('msj_success', 'Se ha eliminado toda la información del evento');
                }
            }
        } catch (ModelNotFoundException $e) {
            abort(404, 'Usuario no autorizado');
        }
    }
}
