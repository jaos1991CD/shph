<?php
//45645645645
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function getDashboard()
    {
        return view('admin.dashboard');
    }
}
