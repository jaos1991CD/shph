<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Shph\Usuario;
use Shph\Beneficio;

class UsuarioController extends Controller
{

    protected $usuario, $beneficio;

    public function __construct(Usuario $usuario, Beneficio $beneficio)
    {
        $this->usuario = $usuario;
        $this->beneficio =$beneficio;
    }

    public function index(Request $request)
    {
       

        $usuarios = $this->usuario->whereNotIn('tipo', ['aliado', 'admin']);

        if ($request->has('fecha_ini')) {
            $fecha_ini = $request->get('fecha_ini')." 00:00:00";
            $fecha_fin = $request->get('fecha_fin')." 23:59:59";

            if ( ! $request->has('fecha_fin')) {
                $fecha_fin = Carbon::now()->toDateTimeString();
            }

            $usuarios = $usuarios->whereBetween('created_at', [$fecha_ini, $fecha_fin])->whereNotIn('tipo', ['aliado', 'admin']);

        }

        if ($request->has('correo')) {
            $usuarios= $usuarios->where('correo', $request->get('correo'));
        }

        if ($request->has('buscar')) {
            
            $usuarios= $usuarios->paginate();

            return view('admin.usuarios.index', compact('usuarios'));
        }
       

        if($request->has('exportar')){
            // exportar excel

             $usuarios = $usuarios->get();

             $consulta = \DB::table('beneficio','usuario')
            ->join('usuario', 'beneficio.id_usuario', '=', 'usuario.id')
            ->join('evento', 'beneficio.id_evento', '=', 'evento.id')
            ->join('usuario_perfil', 'usuario.id','=','usuario_perfil.id_usuario')
            ->select(
            'usuario_perfil.nombres', 
            'usuario_perfil.apellidos',
            'usuario.correo',
            'usuario.tipo',
            'usuario_perfil.fecha_nacimiento',
            'usuario.id',
            'usuario_perfil.genero',
            'beneficio.estado',
            'evento.nombre'
            )
            ->get();
            
             \Excel::create('Reporte usuarios', function($excel) use ($usuarios, $consulta) {
                $excel->setDescription('Información de los usuarios de la aplicación');

                $excel->sheet('Usuarios', function($sheet) use ($usuarios,$consulta) {
                    $sheet->loadView('admin.reportes.usuarios', compact('usuarios','consulta'));
                   
                });
                $excel->sheet('Redimidos', function($sheet) use ($consulta) {
                    $sheet->loadView('admin.reportes.beneficios', compact('consulta'));
                   
                });
                 
            })->download('xlsx');
        }
        $usuarios=$usuarios->paginate();
        return view('admin.usuarios.index', compact('usuarios'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function edit($id)
    {
        try {
            $usuario = $this->usuario->findOrFail($id);

            return view('admin.usuarios.edit', compact('usuario'));

        } catch (ModelNotFoundException $e) {
            abort(404, 'No se encontró este registro.');
        }
    }

    public function update(Request $request, $id)
    {
        // pendiente de pasar a un request
        $validator = \Validator::make($request->all(), [
            'password' => 'min:6|confirmed'
        ]);

        // Si falla la validacion
        if ($validator->fails()) {
            return redirect()->route('administrador.usuarios.edit', compact('id'))->withErrors($validator->errors());
        }

        try {
            $usuario = $this->usuario->findOrFail($id);

            $usuario->tipo = $request->get('tipo');
            $usuario->estado = $request->get('estado');

            if ($usuario->save()) {
                return redirect()->route('administrador.usuarios.index')->with('msj_success', 'Usuario actualizado correctamente.');
            }

            return redirect()->route('administrador.usuarios.index')->with('msj_error', 'Ocurrió un error');
        } catch (ModelNotFoundException $e) {
            abort(404, 'No se encontró este registro.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $usuario = $this->usuario->findOrFail($id);
            if ($usuario->delete()) {
                return redirect()->route('administrador.usuarios.index')->with('msj_success', 'Usuario eliminado correctamente.');
            }
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
    }
}
