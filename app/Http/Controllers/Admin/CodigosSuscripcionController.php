<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Shph\Codigo;
use Shph\Aliado;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CodigosSuscripcionController extends Controller
{
    
    protected $codigo,$region,$aliado;

    public function __construct(Codigo $codigo,Aliado $aliado )
    {   
                  
       $this->codigo =$codigo;
       $this->aliado=$aliado;
   }

    public function index()
    {
        $codigos=$this->codigo->paginate();
        return view('admin.codigosuscripcion.index',compact('codigos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $aliados = $this->aliado->getList();
        return view('admin.codigosuscripcion.create',compact('aliados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

     if(\Input::hasFile('importar')){

            $path = \Input::file('importar')->getRealPath();
            $data = \Excel::selectSheetsByIndex(0)->load($path, function($reader) { 
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {

                    $insert[]=[
                    'nombre' => $value->nombre, 
                    'valor' => $value->valor, 
                    'tipo'=> $value->tipo, 
                    'estado'=>$value->estado,
                    'id_aliado' => $value->id_aliado
                    ];

                }
                if(!empty($insert)){
                    \DB::table('aliado_codigo')->insert($insert);
                    
                }
            }
        return back();
    }

        $codigo = new $this->codigo;
        
        $codigo->fill([
            'nombre'=> $request->get('nombre'),
            'valor'=> $request->get('valor'),
            'tipo'=> 'descuento',
            'estado'=> 'activo',
            'id_aliado'=>$request->get('id_aliado')
        ]);

        $codigo->save();

      
        
        return redirect()->route('administrador.codigosuscripcion.index')->with('msj_success', 'Tu Codigo fue Creado extiosamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $codigo= $this->codigo->findOrFail($id);
        return view('admin.codigosuscripcion.edit',compact('aliados','codigo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $codigo= $this->codigo->findOrFail($id);
        $aliado= $this->aliado->findOrFail($id);
        $codigo->fill([
            'nombre'=> $request->get('nombre'),
            'valor'=> $request->get('valor'),
            'tipo'=> 'descuento',
            'estado'=>'activo',
            'id_aliado'=>$aliado
        ]);
        
        $codigo->save();
        return redirect()->route('administrador.codigosuscripcion.index')->with('msj_success', 'Tu Codigo fue Editado!');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $codigo= $this->codigo->findOrFail($id);

        if ($codigo->delete()) {
                return redirect()->route('administrador.codigosuscripcion.index')->with('message', 'Se ha eliminado toda la información del permiso');
            }
    }
}
