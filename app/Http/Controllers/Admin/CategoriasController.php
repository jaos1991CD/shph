<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Shph\EventoCategoria;
use App\Http\Requests\Categorias\CrearRequest;
use App\Http\Requests\Categorias\EditarRequest;

class CategoriasController extends Controller
{
    protected $categoria;

    public function __construct(
        EventoCategoria $categoria)
    {
        $this->categoria = $categoria;
    }

    public function index()
    {
        $categorias = $this->categoria->paginate();
        return view('admin.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('admin.categorias.create');
    }

    public function store(CrearRequest $request)
    {
        $categoria = new $this->categoria;
        $categoria->fill([
            'nombre' => $request->get('nombre'),
            'slug' => Str::slug($request->get('nombre')),
            'posicion' => 9,
            'descripcion' => $request->get('descripcion'),
            'color' => $request->get('color'),
        ]);

        if ($categoria->save()) {
            return redirect()->route('administrador.categorias.index')->with('msj_success', 'Categoría creada éxitosamente.');
        }
        return redirect()->route('administrador.categorias.create')->withInputs()->with('msj_error', 'Ocurrió un error, por favor comuniquese con el administrador');
    }

    public function edit($id)
    {
        try {
            $categoria = $this->categoria->findOrFail($id);

            return view('admin.categorias.edit', compact('categoria'));

        } catch (ModelNotFoundException $e) {
            abort(404, 'Categoria no encontrada');
        }
    }

    public function update(EditarRequest $request, $id)
    {
        try {
            $categoria = $this->categoria->findOrFail($id);

            $categoria->fill([
                'nombre' => $request->get('nombre'),
                'slug' => Str::slug($request->get('nombre')),
                'descripcion' => $request->get('descripcion'),
                'color' => $request->get('color')
            ]);

            if ($categoria->save()) {
                return redirect()->route('administrador.categorias.index')->with('msj_success', 'Categoría editada éxitosamente.');
            }
            return redirect()->route('administrador.categorias.edit', ['id' => $id])->withInputs()->with('msj_error', 'Ocurrió un error, por favor comuniquese con el administrador');
        
        } catch (ModelNotFoundException $e) {
            abort(404, 'Categoria no encontrada');
        }
    }

    public function destroy($id)
    {
        //
    }
}
