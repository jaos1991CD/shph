<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Shph\Suscripcion;

class SuscripcionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $suscripcion;

    public function __construct(Suscripcion $suscripcion)
    {
        $this->suscripcion= $suscripcion;
    }
    public function index()
    {   

        $suscripciones = $this->suscripcion->paginate();
        return view('admin.suscripcion.index', compact('suscripciones'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.suscripcion.create');    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $suscripcion= new $this->suscripcion;

        $suscripcion->fill([
                'nombre'=> $request->get('nombre'),
                'descripcion'=> $request->get('descripcion'),
                'valor'=> $request->get('valor'),
                'tipo'=> $request->get('tipo'),
                'meses'=> $request->get('meses')



            ]);
          $suscripcion->save();

        return redirect()->route('administrador.suscripcion.index')->with('msj_success', 'La suscripcion a sido creada con exitosamente');
    }

          
           
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suscripcion= $this->suscripcion->findOrFail($id); 

        return view('admin.suscripcion.edit',compact('suscripcion'));       }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $suscripcion= $this->suscripcion->findOrFail($id); 

        $suscripcion->fill([

           'nombre'=> $request->get('nombre'),
           'descripcion'=> $request->get('descripcion'),
           'valor'=> $request->get('valor'),
           'tipo'=> $request->get('tipo'),
           'meses'=> $request->get('meses')


           ]);    

        $suscripcion->save();
         return redirect()->route('administrador.suscripcion.index')->with('msj_success', 'La suscripción a sido editada con exitosamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $suscripcion= $this->suscripcion->findOrFail($id);
        if ($suscripcion->delete()) {
                return redirect()->route('administrador.suscripcion.index')->with('message', 'Se ha eliminado toda la información del la Suscripcion');
            }
    }
}
