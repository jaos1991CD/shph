<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\Contenido\CrearSlideRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

use Shph\ContenidoVideo;
use Shph\ContenidoPagina;
use Shph\ContenidoSlide;

class ContenidoController extends Controller
{
    
    protected $video, $slide, $pagina;
    protected $path_slide = '/front/img/slide/';

    public function __construct(ContenidoVideo $video,
        ContenidoPagina $pagina,
        ContenidoSlide $slide)
    {
        $this->video = $video;
        $this->pagina = $pagina;
        $this->slide = $slide;
    }

    public function getVideo()
    {
        $video = $this->video->find(1);
        return view('admin.contenido.video', compact('video'));
    }

    public function postVideo(Request $request)
    {
        if ($request->get('identificador') == '') {
            return redirect()->route('admin_contenido_video')->with('msj_error', 'El campo identificador no puede estar vacío.');
        }
        $video = $this->video->find($request->get('id'));
        $video->identificador = $request->get('identificador');
        if ($video->save()) {
            return redirect()->route('admin_contenido_video')->with('msj_success', 'El video fue actualizado.');
        }
    }

    public function getNosotros()
    {
        $pagina = $this->pagina->where('nombre', 'nosotros')->first();
        return view('admin.contenido.nosotros', compact('pagina'));
    }

    public function postNosotros(Request $request)
    {
        $pagina = $this->pagina->where('nombre', 'nosotros')->first();

        $pagina->fill([
            'titulo' => $request->get('titulo'),
            'texto' => $request->get('texto')
        ]);

        if ($pagina->save()) {
            return redirect()->route('admin_contenido_nosotros')->with('msj_success', 'Contenido actualizado correctamente.');
        }
        return redirect()->route('admin_contenido_nosotros')->with('msj_error', 'Ocurrió un error, por favor comuniquese con el administrador.');
    }

    public function getTerminos()
    {
        $pagina = $this->pagina->where('nombre', 'terminos')->first();
        return view('admin.contenido.terminos', compact('pagina'));
    }

    public function postTerminos(Request $request)
    {
        $pagina = $this->pagina->where('nombre', 'terminos')->first();

        $pagina->fill([
            'titulo' => $request->get('titulo'),
            'texto' => $request->get('texto')
        ]);

        if ($pagina->save()) {
            return redirect()->route('admin_contenido_terminos')->with('msj_success', 'Contenido actualizado correctamente.');
        }
        return redirect()->route('admin_contenido_terminos')->with('msj_error', 'Ocurrió un error, por favor comuniquese con el administrador.');
    }

    public function getSlide()
    {
        $slides = $this->slide->where('estado', 'activo')->paginate();
        return view('admin.contenido.slide', compact('slides'));
    }

    public function postSlide(CrearSlideRequest $request)
    {
        \DB::beginTransaction();

        $nombre = str_random(20);
        $archivo = $request->file('imagen');

        $nombre_imagen = $nombre.'.'.$archivo->getClientOriginalExtension();
        $destino = public_path().$this->path_slide;
        $archivo->move($destino, $nombre_imagen);
        $url = url($this->path_slide.$nombre_imagen);

        $slide = new $this->slide;

        $slide->fill([
            'imagen' => $nombre_imagen,
            'titulo' => $request->get('titulo'),
            'enlace' => $request->get('enlace'),
            'descripcion' => $request->get('descripcion'),
            'posicion' => 1,
            'estado' => 'activo'
        ]);

        if ($slide->save()) {
            \DB::commit();
            return redirect()->route('admin_contenido_slide')->with('msj_success', 'Imagen añadida al slide correctamente.');
        }
        return redirect()->route('admin_contenido_slide')->with('msj_error', 'Ocurrió un error, por favor comuniquese con el administrador.');
    }

    public function editSlide(Request $request, $id){

        $slide = $this->slide->findOrFail($id);
        return view('admin.contenido.editslide', compact('slide'));

    }
    public function updateSlie(Request $request, $id){    
        \DB::beginTransaction();

         $slide = $this->slide->findOrFail($id);

        $nombre = str_random(20);
        $archivo = $request->file('imagen');

        $nombre_imagen = $nombre.'.'.$archivo->getClientOriginalExtension();
        $destino = public_path().$this->path_slide;
        $archivo->move($destino, $nombre_imagen);
        $url = url($this->path_slide.$nombre_imagen);
       
        $slide->fill([
            'imagen' => $nombre_imagen,
            'titulo' => $request->get('titulo'),
            'enlace' => $request->get('enlace'),
            'descripcion' => $request->get('descripcion'),
            'posicion' => 1,
            'estado' => 'activo'
        ]);

        if ($slide->save()) {
            \DB::commit();
            return redirect()->route('admin_contenido_slide')->with('msj_success', 'Imagen añadida al slide correctamente.');
        }
        return redirect()->route('admin_contenido_slide')->with('msj_error', 'Ocurrió un error, por favor comuniquese con el administrador.');

    }

    public function deleteSlide(Request $request, $id)
    {
        try {
            $slide = $this->slide->findOrFail($id);

            if ($slide->delete()) {
                return redirect()->route('admin_contenido_slide')->with('msj_success', 'Imagen eliminada correctamente.');
            }
        } catch (ModelNotFoundException $e) {
            abort(404, 'Imagen no encontrada');
        }
    }
}








