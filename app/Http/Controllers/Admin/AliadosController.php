<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Shph\Aliado;
use Shph\AliadoContacto;
use Shph\DepartamentoCiudad;
use Shph\Usuario;

use App\Http\Requests\Aliados\CrearRequest;
use App\Http\Requests\Aliados\EditarRequest;

class AliadosController extends Controller
{
    protected $aliado, $contacto, $ciudad, $usuario;

    public function __construct(Aliado $aliado,
        AliadoContacto $contacto,
        Usuario $usuario,
        DepartamentoCiudad $ciudad)
    {
        $this->aliado = $aliado;
        $this->contacto = $contacto;
        $this->ciudad = $ciudad;
        $this->usuario = $usuario;
    }

    public function index(Request $request)
    {   

            $aliados = $this->aliado->paginate();

           if ($request->has('fecha_ini')) {
            $fecha_ini = $request->get('fecha_ini')." 00:00:00";
            $fecha_fin = $request->get('fecha_fin')." 23:59:59";

            if ( ! $request->has('fecha_fin')) {
                $fecha_fin = Carbon::now()->toDateTimeString();
            }

            $aliados = $this->aliado->whereBetween('created_at', [$fecha_ini, $fecha_fin]);

        }

        if ($request->has('buscar')) {
            
            $aliados= $aliados->paginate();

            return view('admin.aliados.index', compact('aliados'));
        }
        if($request->has('exportar')){
            // exportar excel

             $aliados  = $aliados->get();
            
             \Excel::create('Reporte aliados', function($excel) use ($aliados) {
                $excel->setDescription('Información de los Aliados de la aplicación');

                $excel->sheet('Aliados', function($sheet) use ($aliados) {
                    $sheet->loadView('admin.reportes.aliados', compact('aliados'));
                });
            })->download('xlsx');
        }

     
        $aliados = $this->aliado->paginate();
        return view('admin.aliados.index', compact('aliados'));
    }

    public function create()
    {
        $ciudades = $this->ciudad->getList();
        return view('admin.aliados.create', compact('ciudades'));
    }

    public function store(CrearRequest $request)
    {
        $aliado = new $this->aliado;

        $aliado->fill([
            'nombre' => $request->get('nombre'),
            'nit' => $request->get('nit'),
            'descripcion' => $request->get('descripcion'),
            'web' => $request->get('web'),
            'estado' => $request->get('estado'),
            'id_ciudad' => $request->get('id_ciudad')

        ]);

        if ($aliado->save()) {
            $contacto = new $this->contacto;
            $contacto->fill([
                'nombre' => $request->get('nombre_contacto'),
                'email' => $request->get('email_contacto'),
                'telefono' => $request->get('telefono_contacto'),
                'id_aliado' => $aliado->id
            ]);

            if ($contacto->save()) {
                $usuario = new $this->usuario;

                $usuario->fill([
                    'correo' => $request->get('email_contacto'),
                    'password' => '123456',
                    'tipo' => 'aliado',
                    'estado' => 'activo'
                ]);

                if ($usuario->save()) {
                    return redirect()->route('administrador.aliados.index')->with('msj_success', 'El aliado ha sido guardado éxitosamente');
                }
            }
        }

        return redirect()->route('administrador.aliados.create')->withInputs()->with('msj_error', 'Ocurrió un error, por favor comuniquese con el administrador');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try {
            $aliado = $this->aliado->findOrFail($id);
            $ciudades = $this->ciudad->getList();

            return view('admin.aliados.edit', compact('ciudades', 'aliado'));
        } catch (ModelNotFoundException $e) {
            abort(404, 'No se encontró el registro');
        }
    }

    public function update(EditarRequest $request, $id)
    {
        try {
            $aliado = $this->aliado->findOrFail($id);

            $aliado->fill([
                'nombre' => $request->get('nombre'),
                'nit' => $request->get('nit'),
                'descripcion' => $request->get('descripcion'),
                'web' => $request->get('web'),
                'estado' => $request->get('estado'),
                'id_ciudad' => $request->get('id_ciudad')

            ]);

            if ($aliado->save()) {
                // editar contacto
                if (!is_null($aliado->contacto)) {
                    $contacto = $aliado->contacto;

                    $contacto->fill([
                        'nombre' => $request->get('nombre_contacto'),
                        'email' => $request->get('email_contacto'),
                        'telefono' => $request->get('telefono_contacto')
                    ]);
                }else{
                    // crear contacto
                    $contacto = new $this->contacto;

                    $contacto->fill([
                        'nombre' => $request->get('nombre_contacto'),
                        'email' => $request->get('email_contacto'),
                        'telefono' => $request->get('telefono_contacto'),
                        'id_aliado' => $aliado->id
                    ]);
                }

                if ($contacto->save()) {
                    return redirect()->route('administrador.aliados.index')->with('msj_success', 'El aliado ha sido editado éxitosamente');
                }
            }

            return redirect()->route('administrador.aliados.create')->withInputs()->with('msj_error', 'Ocurrió un error, por favor comuniquese con el administrador');
        } catch (ModelNotFoundException $e) {
            abort(404, 'No se encontró el registro');
        }  
    }

    public function destroy($id)
    {
        try {
            $aliado = $this->aliado->findOrFail($id);

            if ($aliado->delete()) {
                return redirect()->route('administrador.aliados.index')->with('msj_success', 'Se ha eliminado toda la información del aliado');
            }
        } catch (ModelNotFoundException $e) {
            abort(404, 'Usuario no autorizado');
        }
    }
}
