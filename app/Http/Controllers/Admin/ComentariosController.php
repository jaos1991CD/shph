<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Shph\EventoComentario;

class ComentariosController extends Controller
{
    public function __construct(EventoComentario $comentario)
    {
        $this->comentario = $comentario;
    }

    public function index()
    {
        $comentarios = $this->comentario->orderBy('estado', 'desc')->paginate();

        return view('admin.comentarios.index', compact('comentarios'));
    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {
        try {
            $comentario = $this->comentario->findOrFail($id);
            $mensaje = '';

            if ($request->has('aprobar')) {
                $comentario->estado = 'activo';

                if ($comentario->save()) {
                    return redirect()->route('administrador.comentarios.index')->with('msj_success', 'El comentario fue aprobado correctamente.');
                }

            }else{

                if ($comentario->delete()) {
                    return redirect()->route('administrador.comentarios.index')->with('msj_success', 'El comentario fue eliminado correctamente.');
                }
            }


            return redirect()->route('administrador.comentarios.index')->with('msj_error', 'Ocurrió un error al tratar de cambiar el estado del comentario.');

        } catch (ModelNotFoundException $e) {
            abort(404, 'Comentario no encontrado');
        }
    }

    public function destroy($id)
    {
        //
    }
}
