<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Shph\Aliado;
use Shph\Usuario;
use Shph\UsuarioPerfil;
use Shph\AliadoContacto;
use Shph\DepartamentoRegion;
use Shph\EventoCategoria;

class ReporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $aliado;

    public function __construct(       
        DepartamentoRegion $region,
        EventoCategoria $categoria,
        Aliado $aliado
        )
    {
        parent::__construct($categoria, $region);
        
        $this->aliado= $aliado;
    }


    public function getAliado()
    {
         $aliados = Aliado::all();
         \Excel::create('Reporte Aliados', function($excel) use ($aliados) {
            $excel->setDescription('Información de los usuarios de la aplicación');

            $excel->sheet('Aliados', function($sheet) use ($aliados) {
                $sheet->loadView('admin.reportes.aliados', compact('aliados'));
            });
        })->download('xlsx');
 
        /*\Excel::create('Reporte Aliado', function($excel) {
        
            $excel->setTitle('LIsta de Aliados');

            $excel->sheet('Aliado', function($sheet) {
 
                //$aliado = Aliado::all();
                $aliado = $this->aliado->with('contacto')->get();
                
                $sheet->fromModel($aliado);

               
 
            });

        })->export('xls');*/
         
    }
    public function getUsuario()
    {
         $usuarios = Usuario::all();
         \Excel::create('Reporte usuarios', function($excel) use ($usuarios) {
            $excel->setDescription('Información de los usuarios de la aplicación');

            $excel->sheet('Usuarios', function($sheet) use ($usuarios) {
                $sheet->loadView('admin.reportes.usuarios', compact('usuarios'));
            });
        })->download('xlsx');
 
        
    }


}
