<?php

/* API */
Route::group(['prefix' => 'api', 'namespace' => 'Front'], function ()
{
	Route::post('evento/create', ['as' => 'front_evento_create', 'uses' => 'EventoController@saveEvento']);
	Route::post('aliado/create', ['as' => 'front_aliado_save', 'uses' => 'AliadoController@postStoreAliado']);
	Route::post('evento/actualizar', ['as' => 'front_evento_actualizar', 'uses' => 'EventoController@update']);

	//evento actualiza
	Route::get('evento/edit/{id}', ['as' => 'front_edit', 'uses' => 'AliadoController@getEdit']);
	Route::patch('evento/update/{id}', ['as' => 'front_aliado_actualizar', 'uses' => 'AliadoController@actualizarEvento']);
	

	Route::post('evento/imagen', ['as' => 'front_evento_imagen', 'uses' => 'EventoController@postSaveImagen']);
	Route::post('evento/imagen/cortar', ['as' => 'front_evento_imagen_crop', 'uses' => 'EventoController@postCropImagen']);

	/* Imagen usuario */
	Route::post('perfil/imagen/upload', ['as' => 'front_perfil_imagen_upload', 'uses' => 'UsuarioController@postSaveImagen']);
	Route::post('perfil/imagen/crop', ['as' => 'front_perfil_imagen_crop', 'uses' => 'UsuarioController@postCropImagen']);

	/* Guardar todo el evento */
	Route::post('evento/save', ['as' => 'front_evento_save', 'uses' => 'EventoController@postSave']);

});