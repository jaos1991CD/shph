<?php
/* Admin Auth - Register*/


// Login
Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('login', ['as' => 'post_login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

// Suscripcion
Route::get('suscribirse', ['as' => 'suscribirse', 'uses' => 'Auth\AuthController@getSuscribirse']);

//Register
Route::get('register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('register', ['as' => 'post_register', 'uses' => 'Auth\AuthController@postRegister']);

Route::get('aliado/register', ['as' => 'aliado.register', 'uses' => 'Front\AliadoController@getRegister']);
Route::post('aliado/register', ['as' => 'aliado.register.post', 'uses' => 'Front\AliadoController@postRegister']);


// Confirm
Route::get('confirmation/{token}', ['as' => 'confirmation', 'uses' => 'Auth\AuthController@getConfirmation']);

// Social Login
Route::get('auth/social/{provider}', ['as' => 'social.login', 'uses' => 'Auth\AuthController@getSocialLogin']);
Route::get('auth/social/callback/{provider}', ['as' => 'social.callback', 'uses' => 'Auth\AuthController@getSocialCallback']);

// reset password 

Route::get('password/email', ['as' => 'password.email', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('password/email', ['as' => 'password.email.post','uses' => 'Auth\PasswordController@postEmail']);

// Password reset routes...
Route::get('/password/reset/', ['as' =>'password.reset', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('/password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\PasswordController@postReset']);

//pagos ['as'=> 'boton_pagos','uses'=>'Auth\AuthController@postPagos']
Route::post('pagos/Dios/prueba', ['as' => 'pagos_dios_prueba', 'uses' => "Auth\AuthController@postMembresia"]);
Route::get('pagos/{id}', ['as'=> 'vista_pagos','uses'=> 'Auth\AuthController@getPagos']);
Route::post("pagos",['uses' => "Auth\AuthController@postPagos"]);
Route::post('pagos/usuario/suscripcion', ['as' => 'ajax_suscripcion_usuario', 'uses'=> 'Auth\AuthController@postPagosUsuarioSuscripcion']);
//codigo
Route::post('suscripcion/codigo', ['as' => 'busqueda', 'uses'=> 'Auth\AuthController@confirmCodigo']);
//respuestas suscripcion o algo mas	
Route::get('respuesta', ['as'=> 'vista_respuesta','uses'=> 'Auth\AuthController@getRespuesta']);
Route::post('respuesta', ['as'=> 'boton_respuesta','uses'=> 'Auth\AuthController@postRespuesta']);
/*Route::post('respuesta', ['as'=> 'respuesta_pago','uses'=> 'Auth\AuthController@Respuesta']);*/

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');