<?php 

/* Administrador */
Route::group(['prefix' => 'administrador', 'middleware' => ['auth', 'is_admin'], 'namespace' => 'Admin'], function ()
{
	Route::get('/', ['as' => 'dashboard', 'uses' => 'HomeController@getDashboard']);
	Route::get('contenido/video', ['as' => 'admin_contenido_video', 'uses' => 'ContenidoController@getVideo']);
	Route::post('contenido/video', ['as' => 'admin_contenido_video_post', 'uses' => 'ContenidoController@postVideo']);

	Route::get('contenido/nosotros', ['as' => 'admin_contenido_nosotros', 'uses' => 'ContenidoController@getNosotros']);
	Route::post('contenido/nosotros', ['as' => 'admin_contenido_nosotros_post', 'uses' => 'ContenidoController@postNosotros']);

	Route::get('contenido/terminos', ['as' => 'admin_contenido_terminos', 'uses' => 'ContenidoController@getTerminos']);
	Route::post('contenido/terminos', ['as' => 'admin_contenido_terminos_post', 'uses' => 'ContenidoController@postTerminos']);

	Route::get('contenido/slide', ['as' => 'admin_contenido_slide', 'uses' => 'ContenidoController@getSlide']);
	Route::get('contenido/slide/{id}/edit', ['as' => 'admin_contenido_slide_edit', 'uses' => 'ContenidoController@editSlide']);
	Route::patch('contenido/slide/{id}/update', ['as' => 'admin_contenido_slide_update', 'uses' => 'ContenidoController@updateSlie']);
	Route::post('contenido/slide', ['as' => 'admin_contenido_slide_post', 'uses' => 'ContenidoController@postSlide']);
	Route::delete('contenido/slide/{id}/delete', ['as' => 'admin_contenido_slide_delete', 'uses' => 'ContenidoController@deleteSlide']);

	Route::post('eventos/{id}/estado', ['as' => 'admin_cambiar_estado', 'uses' => 'EventosController@postChangeEstado']);

	Route::get('excel',['as'=> 'excel_aliado','uses'=> 'ReporteController@getAliado']);
	Route::get('excel/usuario',['as'=> 'excel_usuario','uses'=> 'ReporteController@getUsuario']);

	// ruta para pagos 


	Route::resource('eventos', 'EventosController');
	Route::resource('aliados', 'AliadosController');
	Route::resource('categorias', 'CategoriasController');
	Route::resource('comentarios', 'ComentariosController');
	Route::resource('usuarios', 'UsuarioController');
	Route::resource('suscripcion', 'SuscripcionController');
	Route::resource('codigosuscripcion','CodigosSuscripcionController');
	
});