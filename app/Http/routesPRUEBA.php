<?php

/* Verificar Zona */
Route::group(['middleware' => ['zona']], function ()
{
	Route::get('/', ['as' => 'home', 'uses' => 'Front\HomeController@index']);

	include 'Routes/AuthRoute.php';

	/* Front */
	Route::get('eventos/{slug}', ['as' => 'front_eventos', 'uses' => 'Front\HomeController@getEventos']);
	Route::get('evento/{ciudad}/{slug}', ['as' => 'front_evento', 'uses' => 'Front\HomeController@getEvento']);
	Route::get('categoria/{slug}', ['as' => 'front_eventos_categoria', 'uses' => 'Front\HomeController@getEventosCategoria']);
	Route::get('nosotros', ['as' => 'front_nosotros', 'uses' => 'Front\HomeController@getNosotros']);
	Route::get('terminos-y-condiciones', ['as' => 'front_terminos', 'uses' => 'Front\HomeController@getTerminos']);

	//JSON
	Route::get('json/ciudades/{id_departamento}', ['as' => 'json_ciudades', 'uses' => 'Front\HomeController@getCiudades']);
	Route::get('json/zona/{zona}', ['as' => 'json_change_zona', 'uses' => 'Front\HomeController@changeZona']);

	/* Usuario autenticado */
	Route::group(['middleware' => ['auth'], 'namespace' => 'Front'], function ()
	{	// Ver evento
		Route::post('evento/{id}/comentario', ['as' => 'front_evento_comentario', 'uses' => 'HomeController@postComentario']);
		// solicitar el beneficio
		Route::post('solicitar-beneficio/{id}', ['as' => 'front_solicitar_beneficio', 'uses' => 'EventoController@getSolicitar']);
		Route::get('perfil', ['as' => 'front_perfil', 'uses' => 'UsuarioController@getPerfil']);
		Route::post('perfil', ['as' => 'front_perfil_post', 'uses' => 'UsuarioController@postPerfil']);
		// formulario para publicar el evento
		Route::get('publica-tu-evento', ['as' => 'front_publica', 'uses' => 'HomeController@getPublicaTuEvento']);
		
		//codigos editar,eliminar,crear
		Route::get('codigo',['as'=> 'codigo_suscripcion', 'uses'=>'CodigosController@getCodigo']);
		Route::get('codigo/vista',['as'=>'codigo_vista', 'uses'=> 'CodigosController@getvista']);
		Route::get('codigo/editar/{id}',['as'=>'codigo_editar','uses'=>'CodigosController@getEditar']);
		Route::post('codigo/create',['as'=>'codigo_crear', 'uses'=>'CodigosController@postCodigos']);
		Route::patch('codigo/actualizar/{id}',['as'=>'codigo_actualizar','uses'=>'CodigosController@postActualizar']);


		Route::group(['prefix' => 'aliado', 'middleware' => 'is_aliado'], function ()
		{
			Route::get('perfil', ['as' => 'aliado_perfil', 'uses' => 'AliadoController@perfil']);
			Route::patch('perfil/{id}', ['as' => 'aliado.perfil.patch', 'uses' => 'AliadoController@patchPerfil']);
			Route::get('dashboard', ['as' => 'aliado_dashboard', 'uses' => 'AliadoController@dashboard']);

			
		});
	});
});


include 'Routes/ApiRoute.php';
include 'Routes/AdminRoute.php';


Route::get('pruebas', function ()
{




/*$usuario = \Shph\Usuario::where('correo','roberto.arenas@creardigital.com')->firstOrFail();

if(\Auth::loginUsingId($usuario->id, true)){

return redirect()->route('login')->with('msj_success', 'Su pago se realizó satisfactoriamente');
}*/

/*$usuario = \DB::table('beneficio','usuario')
            ->join('usuario', 'beneficio.id_usuario', '=', 'usuario.id')
            ->join('evento', 'beneficio.id_evento', '=', 'evento.id')
            ->join('usuario_perfil', 'usuario.id','=','usuario_perfil.id_usuario')
            ->select(
            'usuario_perfil.nombres', 
            'usuario_perfil.apellidos',
            'usuario.correo',
            'usuario.tipo',
            'usuario_perfil.fecha_nacimiento',
            'usuario.id',
            'usuario_perfil.genero',
            'beneficio.estado',
            'evento.nombre',
            'usuario.acepto'
            )
            ->get();
            dd($usuario);
*/
/*
	$usuario = Shph\Usuario::where('correo', 'roberto.arenas@creardigital.com')->first();
    $suscripcion = $usuario->suscripcion()->where('estado', 'en_proceso')->orderBy('id', 'desc')->first();

		$inicio = \Carbon::now();
    	$fin = \Carbon::now()->addmonth($suscripcion->meses);
    	'algo' = $inicio->diffInDays($fin);
    	
      


    	$usuario->suscripcion()->updateExistingPivot($suscripcion->id,[
       		'fecha_inicio' => 'algo' ,
    		'fecha_vencimiento' => 'algo',
    		'estado'=>'en_proceso',
    		'token'=>'request_token',
    		'numeros'=>'req_card_number',
    		'fechaexp'=> 'se hicieron ',
    		'idTransaccion'=> 'transaction_id',
    		'idEstado'=> 'reason_code',
    		'nombreEstado' => 'decision',
    		'referencia'=> 'req_reference_number',
    		'codigoRespuesta' => 'reason_code',
    		'codigoAutorizacion'  => 'auth_code',
    		'valor'=> 'req_amount', 
    		'iva'=> 'req_merchant_defined_data5', 
    		'baseDevolucion'=> 'req_merchant_defined_data6',
    		'fechaProcesamiento'=> 'signed_date_time',
    		'mensaje'=> 'Aprobada'
    	],false);
    	 return "todo good";*/

	/*$client = new \GuzzleHttp\Client(['base_uri' => 'https://test.secureacceptance.allegraplatform.com']);
		
	$response = $client->post('CI_Secure_Acceptance/Payment',   [
	 'form_data' => [
                    
                    'Acces key'=>'69d0292ac8bc322c987da8d9d21cd44f',
                    'profile_id'=>'8F64BCAE-25CE-4A35-8598-1EC35EEF0B7E',
                    'reference_number'=>'1458325873677',
                    'amount'=>'10000',
                    'currency'=>'COP',
                    'locale'=>'es-CO',
                    'transaction_uuid'=>'20160318T183228Z',
                    'signed_date_time'=>'2013-01-17T10:46:39Z',
                    'signed_field_names'=>'comma separated list of signed fields',
                    'unsigned_field_names'=>'comma separated list of unsigned fields',
                    'signature'=>'FApodS4JbvhiioaT8x3qqUWvVP1XYfq+tUSwLCvKcRo='
                    ]
                    ]);


	$response = json_decode($response->getBody());
		
	dd($response);*/

	}); 
