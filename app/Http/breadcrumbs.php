<?php 

Breadcrumbs::register('inicio', function($breadcrumbs)
{
	$breadcrumbs->push('Dashboard', route('dashboard'));
});

Breadcrumbs::register('perfil', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Perfil', route('perfil'));
});

Breadcrumbs::register('eventos', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Eventos', route('administrador.eventos.index'));
});

Breadcrumbs::register('aliados', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Aliados', route('administrador.aliados.index'));
});

	Breadcrumbs::register('aliados_create', function($breadcrumbs)
	{
		$breadcrumbs->parent('aliados');
	    $breadcrumbs->push('Crear Aliado', route('administrador.aliados.create'));
	});

	Breadcrumbs::register('aliados_edit', function($breadcrumbs)
	{
		$breadcrumbs->parent('aliados');
	    $breadcrumbs->push('Editar Aliado', route('administrador.aliados.create'));
	});

Breadcrumbs::register('eventos', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Eventos', route('administrador.eventos.index'));
});

	Breadcrumbs::register('eventos_create', function($breadcrumbs)
	{
		$breadcrumbs->parent('eventos');
	    $breadcrumbs->push('Crear Evento', route('administrador.eventos.create'));
	});

	Breadcrumbs::register('eventos_edit', function($breadcrumbs)
	{
		$breadcrumbs->parent('eventos');
	    $breadcrumbs->push('Editar Evento', route('administrador.eventos.create'));
	});

Breadcrumbs::register('comentarios', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Comentarios', route('administrador.comentarios.index'));
});

	Breadcrumbs::register('comentarios_edit', function($breadcrumbs)
	{
		$breadcrumbs->parent('comentarios');
	    $breadcrumbs->push('Ver Comentario', route('administrador.comentarios.create'));
	});

Breadcrumbs::register('categorias', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Categorías', route('administrador.categorias.index'));
});

	Breadcrumbs::register('categorias_create', function($breadcrumbs)
	{
		$breadcrumbs->parent('categorias');
	    $breadcrumbs->push('Crear Categoría', route('administrador.categorias.create'));
	});

	Breadcrumbs::register('categorias_edit', function($breadcrumbs)
	{
		$breadcrumbs->parent('categorias');
		$breadcrumbs->push('Editar Categoría', route('administrador.categorias.create'));
	});

Breadcrumbs::register('video', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Gestionar Video', route('admin_contenido_video'));
});

Breadcrumbs::register('nosotros', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Editar Sección Nosotros', route('admin_contenido_nosotros'));
});

Breadcrumbs::register('terminos', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Editar Sección Terminos y Condiciones', route('admin_contenido_terminos'));
});

Breadcrumbs::register('slide', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Gestionar Imágenes Slide', route('admin_contenido_slide'));
});

Breadcrumbs::register('usuarios', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
	$breadcrumbs->push('Usuarios', route('administrador.usuarios.index'));
});

	Breadcrumbs::register('usuarios_edit', function($breadcrumbs)
	{
		$breadcrumbs->parent('usuarios');
		$breadcrumbs->push('Editar Usuario', route('administrador.usuarios.create'));
	});

	Breadcrumbs::register('usuarios_create', function($breadcrumbs)
	{
		$breadcrumbs->parent('usuarios');
		$breadcrumbs->push('Nuevo Usuario', route('administrador.usuarios.create'));
	});




/*Breadcrumbs::register('aliados', function($breadcrumbs)
{
	$breadcrumbs->parent('inicio');
    $breadcrumbs->push('Aliados', route('admin.aliados.index'));
});

	Breadcrumbs::register('aliados_show', function($breadcrumbs, $aliado)
	{
		$breadcrumbs->parent('aliados');
	    $breadcrumbs->push($aliado->nombre, route('admin.aliados.show', ['aliados' => $aliado->id]));
	});

	Breadcrumbs::register('aliados_edit', function($breadcrumbs, $aliado)
	{
		$breadcrumbs->parent('aliados_show', $aliado);
	    $breadcrumbs->push('Editar', route('admin.aliados.edit', ['aliados' => $aliado->id]));
	});

	Breadcrumbs::register('aliados_tiendas_import', function($breadcrumbs, $aliado)
	{
		$breadcrumbs->parent('aliados_show', $aliado);
	    $breadcrumbs->push('Cargar Tiendas', route('aliados_cargar_tiendas'));
	});

	Breadcrumbs::register('aliados_tiendas_edit', function($breadcrumbs, $aliado, $tienda)
	{
		$breadcrumbs->parent('aliados_show', $aliado);
	    $breadcrumbs->push('Editar Tienda', route('admin.tiendas.edit', ['aliados' => $tienda->id]));
	});*/