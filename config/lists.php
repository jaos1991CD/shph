<?php 

return [
	'estados' => [
		'activo' => 'Activo',
		'inactivo' => 'Inactivo'
	],
	'tipos_evento' => [
		'' => 'Seleccionar Uno',
		'general' => 'Código General',
		'unico' => 'Código Único',
		'enlace' => 'Enlace'
	],
	'generos' => [
		'' => 'Seleccionar Uno',
		'm' => 'Masculino',
		'f' => 'Femenino'
	],
	'tipos_usuarios' => [
		'' => 'Seleccionar Uno',
		'admin' => 'Administrador',
		'aliado' => 'Aliado',
		'user' => 'Usuario'
	]
	
];