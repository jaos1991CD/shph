<?php

use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('usuario')->insert(['correo' => 'stiven.castillo@creardigital.com', 'password' => \Hash::make('123456'), 'tipo' => 'admin', 'estado' => 'activo']);
        \DB::table('usuario')->insert(['correo' => 'stivencastillo.90@gmail.com', 'password' => \Hash::make('123456'), 'tipo' => 'user', 'estado' => 'activo']);
    
        \DB::table('usuario_perfil')->insert(['nombres' => 'Stiven', 'apellidos' => 'Castillo Montero', 'genero' => 'm', 'fecha_nacimiento' => '1990-10-30', 'id_ciudad' => 1, 'id_usuario' => 1]);
        \DB::table('usuario_perfil')->insert(['nombres' => 'Stiven C', 'apellidos' => 'Castillo', 'genero' => 'm', 'fecha_nacimiento' => '1990-10-30', 'id_ciudad' => 1, 'id_usuario' => 2]);

    }
}