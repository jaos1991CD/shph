<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Shph\Evento;
use Shph\DepartamentoCiudad;

class EventoslugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eventos= Evento::get();
        
        foreach($eventos as $evento)
        {
            if (is_null($evento->slug)){

                $evento->slug= Str::slug($evento->nombre);
                $evento->save();
            }
        }

        $ciudades= DepartamentoCiudad::get();
        
        foreach($ciudades as $ciudad)
        {
            if (is_null($ciudad->slug)){

                $ciudad->slug= Str::slug($ciudad->nombre);
                $ciudad->save();
            }
        }
    }
}