<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration
{
    
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('correo');
            $table->string('password');
            $table->enum('tipo', ['user', 'admin']);
            $table->enum('estado', ['activo', 'inactivo']);
            $table->rememberToken();
            $table->timestamps();

            $table->unique('correo');
        });

        Schema::create('usuario_perfil', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->enum('genero', ['m', 'f']);
            $table->date('fecha_nacimiento');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_ciudad')->unsigned();
            $table->timestamps();

            $table->foreign('id_ciudad')->references('id')->on('departamento_ciudad')->onDelete('cascade');
            $table->foreign('id_usuario')->references('id')->on('usuario')->onDelete('cascade');
        });

        Schema::create('usuario_oauth', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('provider', ['facebook', 'twitter']);
            $table->string('provider_id');
            $table->integer('id_usuario')->unsigned();
            $table->timestamps();

            $table->foreign('id_usuario')->references('id')->on('usuario')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('usuario_oauth');
        Schema::drop('usuario_perfil');
        Schema::drop('usuario');
    }
}
