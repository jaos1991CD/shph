<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoEventoField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evento', function ($table) {
            $table->enum('tipo', ['general', 'unico', 'enlace'])->default('general')->after('latitud');
            $table->string('codigo')->nullable()->after('tipo');
            $table->string('enlace')->nullable()->after('codigo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evento', function ($table) {
            $table->dropColumn('tipo');
            $table->dropColumn('enlace');
            $table->dropColumn('codigo');
        });
    }
}
