<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodigoSuscripcionField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuario_suscripcion', function ($table) {
            $table->integer('id_codigo')->unsigned()->nullable()->after('id_usuario');

            $table->foreign('id_codigo')->references('id')->on('aliado_codigo')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuario_suscripcion', function ($table) {
            $table->dropColumn('id_codigo');
        });
    }
}
