<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResetTable extends Migration
{
    

    public function up()
    {
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('correo')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });
    }

    
    public function down()
    {
        Schema::drop('password_resets');
    }
}
    