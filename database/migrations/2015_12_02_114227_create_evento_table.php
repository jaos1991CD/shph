<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventoTable extends Migration
{
	public function up()
	{
		Schema::create('evento_estado', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nombre');
			$table->string('descripcion');
			$table->timestamps();
		});

		Schema::create('evento_imagen', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nombre');
			$table->string('url');
			$table->timestamps();
		});

		Schema::create('evento_categoria_imagen', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nombre');
			$table->string('url');
			$table->timestamps();
		});

		Schema::create('evento_categoria', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nombre');
			$table->string('color');
			$table->integer('posicion');
			$table->text('descripcion');
			$table->integer('id_imagen')->unsigned();

            $table->foreign('id_imagen')->references('id')->on('evento_categoria_imagen');
			$table->timestamps();
		});

		Schema::create('evento', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nombre');
			$table->text('descripcion');
			$table->string('descripcion_corta');
			$table->text('terminos');
			$table->string('direccion')->nullable();
			$table->string('lugar')->nullable();
			$table->string('web')->nullable();
			$table->date('fecha_inicio');
			$table->date('fecha_fin');
			$table->string('hora_inicio', 40);
			$table->string('hora_fin', 40);

			$table->text('precio')->nullable();

			$table->string('longitud');
			$table->string('latitud');

			$table->integer('id_aliado')->unsigned();
			$table->integer('id_estado')->unsigned();
			$table->integer('id_categoria')->unsigned();
			$table->integer('id_imagen')->unsigned();
			$table->integer('id_ciudad')->unsigned();

            $table->foreign('id_aliado')->references('id')->on('aliado')->onDelete('cascade');
            $table->foreign('id_estado')->references('id')->on('evento_estado');
            $table->foreign('id_categoria')->references('id')->on('evento_categoria');
            $table->foreign('id_imagen')->references('id')->on('evento_imagen');
            $table->foreign('id_ciudad')->references('id')->on('departamento_ciudad');

			$table->timestamps();
		});

		Schema::create('evento_comentario', function (Blueprint $table) {
			$table->increments('id');
			$table->string('nombre');
			$table->text('mensaje');
			$table->integer('calificacion');

			$table->integer('id_evento')->unsigned();
			$table->integer('id_usuario')->unsigned();

            $table->foreign('id_evento')->references('id')->on('evento');
            $table->foreign('id_usuario')->references('id')->on('usuario')->onDelete('cascade');
			$table->timestamps();
		});
	}

	public function down()
	{
        Schema::drop('evento_comentario');
        Schema::drop('evento');
        Schema::drop('evento_categoria');
        Schema::drop('evento_categoria_imagen');
        Schema::drop('evento_imagen');
        Schema::drop('evento_estado');
	}
}
