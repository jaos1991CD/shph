<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkSlideField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contenido_slide', function ($table) {
            $table->text('descripcion')->nullable()->after('titulo');
            $table->string('enlace')->nullable()->after('descripcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contenido_slide', function ($table) {
            $table->dropColumn('descripcion');
            $table->dropColumn('enlace');
        });
    }
}
