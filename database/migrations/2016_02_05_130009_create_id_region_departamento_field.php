<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdRegionDepartamentoField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('departamento', function ($table) {
            $table->integer('id_region')->unisgned()->default(0)->after('descripcion');
            $table->foreign('id_region')->references('id')->on('departamento_region')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('departamento', function ($table) {
            $table->dropColumn('id_region');
        });
    }
}
