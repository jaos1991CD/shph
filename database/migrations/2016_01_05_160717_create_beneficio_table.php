<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeneficioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_evento')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->enum('estado', ['solicitado', 'redimido']);
            $table->string('codigo')->nullable();
            $table->timestamp('fecha_redimido');
            $table->timestamps();

            $table->foreign('id_evento')->references('id')->on('evento')->onDelete('cascade');
            $table->foreign('id_usuario')->references('id')->on('usuario')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('beneficio');
    }
}
