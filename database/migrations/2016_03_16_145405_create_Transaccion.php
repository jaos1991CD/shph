<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaccion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idTransaccion');
            $table->integer('idEstado');
            $table->string('nombreEstado');
            $table->string('referencia');
            $table->string('codigoRespuesta');
            $table->string('codigoAutorizacion');
            $table->string('riesgo');
            $table->float('valor',9);
            $table->float('iva',9);
            $table->float('baseDevolucion',9);
            $table->timestamp('fechaProcesamiento');
            $table->string('mensaje');
            $table->integer('id_suscripcion')->unsigned();

            $table->foreign('id_suscripcion')->references('id')->on('usuario_suscripcion')->onDelete('cascade');
            $table->timestamps();
        });
      /*  Schema::create('Estados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('id_transaccion')->unsigned();
            
            $table->foreign('id_transaccion')->references('id')->on('transaccion')->onDelete('cascade');
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaccion');
    }
}
