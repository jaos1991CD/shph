<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAliadoCodigoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('aliado_codigo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_aliado')->unsigned();
            $table->string('nombre');
            $table->enum('tipo', ['descuento', 'total']);
            $table->string('valor');
          
            $table->timestamps();

            $table->foreign('id_aliado')->references('id')->on('aliado')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aliado_codigo');
    }
}
