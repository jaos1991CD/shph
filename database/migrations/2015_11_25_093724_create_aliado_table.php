<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAliadoTable extends Migration
{
    public function up()
    {
        Schema::create('aliado', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('nit');
            $table->string('direccion');
            $table->string('web')->nullable();
            $table->enum('estado', ['activo', 'inactivo']);
            $table->integer('id_ciudad')->unsigned();
            $table->timestamps();

            $table->foreign('id_ciudad')->references('id')->on('departamento_ciudad');
        });

        Schema::create('aliado_contacto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('email');
            $table->string('telefono');
            $table->integer('id_aliado')->unsigned();
            $table->timestamps();

            $table->foreign('id_aliado')->references('id')->on('aliado')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('aliado_contacto');
        Schema::drop('aliado');
    }
}
