<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadoCodigoAliadoField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('aliado_codigo', function ($table) {
            $table->enum('estado', ['general', 'activo', 'inactivo'])->default('activo')->after('valor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('aliado_codigo', function ($table) {
            $table->dropColumn('estado');
        });
    }
}
