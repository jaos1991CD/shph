<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteImagenCategoria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evento_categoria', function ($table) {
            $table->dropForeign('evento_categoria_id_imagen_foreign');
            $table->dropColumn('id_imagen');
        });

        Schema::drop('evento_categoria_imagen');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
