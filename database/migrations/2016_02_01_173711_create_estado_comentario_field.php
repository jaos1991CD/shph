<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadoComentarioField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evento_comentario', function ($table) {
            $table->enum('estado', ['activo', 'inactivo'])->default('inactivo')->after('calificacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evento_comentario', function ($table) {
            $table->dropColumn('estado');
        });
    }
}
