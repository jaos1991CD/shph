<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComboTable extends Migration
{
    public function up()
    {
        Schema::create('combo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion');
            $table->text('terminos');
            $table->integer('precio');
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->string('hora_inicio')->nullable();
            $table->string('hora_fin')->nullable();
            $table->enum('estado', ['activo', 'inactivo']);

            $table->timestamps();
        });

        Schema::create('combo_evento_pivot', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_combo')->unsigned();
            $table->integer('id_evento')->unsigned();

            $table->foreign('id_combo')->references('id')->on('combo')->onDelete('cascade');
            $table->foreign('id_evento')->references('id')->on('evento');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('combo_evento_pivot');
        Schema::drop('combo');
    }
}
