<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadoUsuarioSuscripcionField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuario_suscripcion', function ($table) {
            $table->dropColumn('estado');
        });

        Schema::table('usuario_suscripcion', function ($table) {
            $table->enum('estado', ['en_proceso', 'activo', 'inactivo'])->default('en_proceso')->after('id_usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
