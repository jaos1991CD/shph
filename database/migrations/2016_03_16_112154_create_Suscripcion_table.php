<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuscripcionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('suscripcion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('descripcion')->nullable();
            $table->float('valor');
            $table->enum('tipo',['mensual','semestral','anual','gratis'])->default('mensual');;
           

            $table->timestamps();
        });

         Schema::create('usuario_suscripcion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_suscripcion')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->date('fecha_inicio');
            $table->date('fecha_vencimiento');
            $table->string('estado');
            $table->string('token');
            $table->integer('numeros');
            $table->string('fechaexp');

            

            $table->foreign('id_suscripcion')->references('id')->on('suscripcion')->onDelete('cascade');
             $table->foreign('id_usuario')->references('id')->on('usuario')->onDelete('cascade');

             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suscripcion');
        Schema::drop('usuario_suscripcion');
    }
}
