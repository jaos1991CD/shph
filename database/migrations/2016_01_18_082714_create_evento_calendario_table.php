<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventoCalendarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento_horario', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->time('hora');
            $table->integer('id_evento')->unsigned();
            $table->foreign('id_evento')->references('id')->on('evento')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::table('evento', function ($table) {
            $table->dropColumn('hora_inicio');
            $table->dropColumn('hora_fin');
            $table->dropColumn('descripcion_corta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evento', function ($table) {
            $table->time('hora_inicio')->nullable();
            $table->time('hora_fin')->nullable();
        });

        Schema::drop('evento_horario');
    }
}
