<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Samsung\Usuario::class, function (Faker\Generator $faker) {
    return [
        'correo' => $faker->email,
        'password' => '123456',
        'tipo' => $faker->randomElement(['user', 'admin']),
        'estado' => $faker->randomElement(['activo', 'inactivo']),
        'remember_token' => str_random(10)
    ];
});


$factory->define(Samsung\UsuarioPerfil::class, function (Faker\Generator $faker) {
    return [
        'nombres' => $faker->name,
        'apellidos' => $faker->lastName,
        'empresa' => $faker->catchPhrase,
        'cargo' => 'Cargo de prueba',
        'id_usuario' => $faker->numberBetween(3, 23)
    ];
});